#pragma once

#include "definitions.hpp"
#include "render/res/resbuffers.hpp"
#include <render/rendercommon.hpp>
#include <vulkan/vulkan_core.h>

namespace ox
{
    struct GPUMesh
    {
        ox::GPUBuffer buffer = {};
        size_t vertcount = 0;
    };
    struct RenderCreateInfo
    {
        VkSampleCountFlagBits samplesCount;
    };
    class Render
    {        
    public:
        CTORS_REMOVE_ALL(Render);
        explicit Render(const RenderCreateInfo& info, bool& result) noexcept;
        ~Render() noexcept;
        
        auto wait() -> void;

        auto getInstance() const noexcept -> VkInstance { return mInstance; }
        auto getDevice() const noexcept -> VkDevice { return mDevice; }
        auto getAllocator() const noexcept -> VmaAllocator { return mAllocator; }
        auto getPhysicalDevice() const noexcept -> VkPhysicalDevice { return mPhysicalDevice; }
        auto getGfxCompQueue() const noexcept -> const RenderQueue& { return mGfxCompQueue; }
        auto getPresentQueue() const noexcept -> const RenderQueue& { return mPresentQueue; }
        auto getComputeQueue() const noexcept -> const RenderQueue& { return mComputeQueue; }
        auto getSamplesCount() const noexcept -> VkSampleCountFlagBits { return mSamplesCount; }
        auto get(ShaderResource shaderType) const noexcept -> VkShaderModule;
        auto get(FontResource fontType) const noexcept -> const BitmapFont*;
        auto getCubeMesh() const noexcept -> const ox::GPUMesh* { return &mCubeMesh; }

        auto setTargetColorFormat(VkFormat colorFormat) noexcept -> void 
        {
            mTargetColorFormat = colorFormat; 
        }
        auto setTargetColorSpace(VkColorSpaceKHR colorSpace) noexcept -> void 
        {
            mTargetColorSpace = colorSpace; 
        }
        auto getTargetColorFormat() const noexcept -> VkFormat { return mTargetColorFormat; }
        auto getTargetDepthFormat() const noexcept -> VkFormat { return mTargetDepthFormat; }
        auto getTargetColorSpace() const noexcept -> VkColorSpaceKHR { return mTargetColorSpace; }
        
        auto hasTargetColorFormat() const noexcept -> bool { return mTargetColorFormat != VK_FORMAT_UNDEFINED; }
        auto hasTargetDepthFormat() const noexcept -> bool { return mTargetDepthFormat != VK_FORMAT_UNDEFINED; }
        auto hasTargetColorSpace() const noexcept -> bool { return mTargetColorSpace != VK_COLOR_SPACE_MAX_ENUM_KHR; }

        constexpr static auto getFrameCount() noexcept -> uint32_t { return MAX_CONCURRENT_FRAMES; }
        
    private:
        auto createInstance() -> bool;
        auto createDebugger() -> bool;
        auto choosePhysicalDevice() -> bool;
        auto chooseQueueFamilies() -> bool;
        auto createDevice() -> bool;
        auto createAllocator() -> bool;
        auto getQueues() -> void;
        auto printDeviceInfo() -> void;
        auto loadResources() -> bool;
   
    private:
        //bool mInitialized = false;
        VkInstance mInstance = VK_NULL_HANDLE;
        VkDebugUtilsMessengerEXT mDebugger = VK_NULL_HANDLE;
        VkDevice mDevice = VK_NULL_HANDLE;
        VmaAllocator mAllocator = VK_NULL_HANDLE;
        VkPhysicalDevice mPhysicalDevice = VK_NULL_HANDLE;
        RenderQueue mGfxCompQueue = {.initialized = false};
        RenderQueue mPresentQueue = {.initialized = false};
        RenderQueue mComputeQueue = {.initialized = false};

        VkShaderModule mShaders[static_cast<size_t>(ShaderResource::SIZE)] = {VK_NULL_HANDLE};
        BitmapFont mFonts[static_cast<size_t>(FontResource::SIZE)] = {};
        GPUMesh mCubeMesh = {};
        VkSampleCountFlagBits mSamplesCount = VkSampleCountFlagBits::VK_SAMPLE_COUNT_1_BIT;
        VkFormat mTargetColorFormat = VK_FORMAT_UNDEFINED;
        VkFormat mTargetDepthFormat = VK_FORMAT_UNDEFINED;
        VkColorSpaceKHR mTargetColorSpace = VK_COLOR_SPACE_MAX_ENUM_KHR;
    };
}
