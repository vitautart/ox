#pragma once

#include "render/render.hpp"
#include <cstdint>
#include <definitions.hpp>
#include <render/rendercommon.hpp>
#include <ringindexer.hpp>

#include <vector>
#include <span>
#include <functional>
#include <vulkan/vulkan_core.h>
#include <GLFW/glfw3.h>

namespace ox
{
    struct SwapchainConfig
    {
        VkPresentModeKHR mode;
        VkColorSpaceKHR colorSpace;
        VkFormat colorFormat;
        VkFormat depthFormat;
    };

    struct RenderWindowCreateInfo
    {
        /*VkInstance instance;
        VkPhysicalDevice physicalDevice;
        VkDevice device;
        VmaAllocator allocator;
        VkSampleCountFlagBits samples;*/
        const Render* render;
        GLFWwindow* window;
        uint32_t gfxCompQueueFamily;
        uint32_t presentQueueFamily;
    };

    class RenderWindow
    {
    public:
        auto create(const RenderWindowCreateInfo& iCreateInfo, 
                const std::function<void(VkFormat, VkColorSpaceKHR)>& setColorFormatAndSpaceFn) -> bool;
        auto destroy() -> void;
        // TODO: maybe we should add here device wait as in renderer, to decrease future dependencies
        // also waiting for presentation fences introduces additional; synchronization in renderwait,
        // and sadly crash in NVIDIA drivers.
        //auto wait() -> void;
        auto acquire() -> RenderFrame;
        template <typename Func>
        auto addCommandBuffer(const RenderFrame& iFrame, 
                const Func& iFunc) -> void
        {
            const auto cb = mStandardCommands[iFrame.id];
            VkCommandBufferBeginInfo cbBeginInfo = { .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO };
            vkResetCommandBuffer(cb, VK_COMMAND_BUFFER_RESET_RELEASE_RESOURCES_BIT);
            vkBeginCommandBuffer(cb, &cbBeginInfo);
            iFunc(cb, iFrame);
            vkEndCommandBuffer(cb);
            mCachedCommandBuffers.push_back(cb);
        }
        auto addCommandBuffer(VkCommandBuffer iCommandBuffer) -> void;
        auto submit(VkQueue iGfxCompQueue, VkQueue iPresentQueue) -> bool;
        auto cleanupSizedData() -> void;
        auto resize() -> bool;
        auto getScreenTarget() const noexcept -> const RenderTarget* { return &mScreenTarget; }
        auto getCommandPoolGraphic() const noexcept -> VkCommandPool { return mCommandPoolGraphic; }
    
    private:
        auto createSurface() -> bool;
        auto configureSwapchain() -> bool;
        auto createSwapchain() -> bool;
        auto createScreenTarget() -> bool;
        auto createSyncData() -> bool;
        auto createCommandPool() -> bool;
        auto createStandardCommandBuffers() -> bool;
        auto createTransitionalCommandBuffers() -> bool;
        auto recordTransitionalCommandBuffers() -> void;
        auto destroyScreenTarget() -> void;
    
    private: // owned resources
        bool mInitialized = false;
        ox::ringindexer<uint32_t, MAX_CONCURRENT_FRAMES> mFrameIndexer = {}; 
        uint32_t mImageid = 0;
        VkSurfaceKHR mSurface = VK_NULL_HANDLE;
        VkSwapchainKHR mSwapchain = VK_NULL_HANDLE;
        VkExtent2D mExtent = {};
        SwapchainConfig mConfig = {};
        RenderTarget mScreenTarget = {};
        std::vector<VkSemaphore> mSemaphoresCommandDone = {};
        std::vector<VkSemaphore> mSemaphoresPresentDone = {};
        std::vector<VkFence> mFencesCommandDone = {};
        VkCommandPool mCommandPoolGraphic = VK_NULL_HANDLE;
        std::vector<VkCommandBuffer> mStandardCommands;
        std::vector<VkCommandBuffer> mToColorCommands;
        std::vector<VkCommandBuffer> mToPresentCommands;
        std::vector<VkCommandBuffer> mCachedCommandBuffers;

    private: // borrowed resources
        RenderWindowCreateInfo mCreateInfo;
    };
}
