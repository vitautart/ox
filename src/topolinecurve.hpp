#pragma once

#include <topobase.hpp>

namespace ox
{

struct TopoLineCurve : public ITopoCurve 
{ 
    TopoLineCurve (const oxm::Line3D<double>& iLine) : line(iLine) {}
    virtual auto coincide(TopoPoint iPoint) const -> bool { return oxm::coincide(line, iPoint); }
    //virtual auto tangentAt(TopoPoint iPoint) const -> TopoDir { return line.dir; }
    virtual auto plane() const -> std::optional<oxm::Plane<double>> { return std::nullopt; };
    virtual auto planar() const -> bool { return true; }
    virtual auto transform(const oxm::md4& iTransform) -> void
    {
        line.point = (iTransform * oxm::grow(line.point, 1.0)).shrink<3>();
        line.dir = (iTransform * oxm::grow(line.dir, 0.0)).shrink<3>();
    }
    virtual auto transformed(const oxm::md4& iTransform) const -> std::unique_ptr<ITopoCurve>
    {
        return std::make_unique<TopoLineCurve>(TopoLineCurve
                {{
                    .point = (iTransform * oxm::grow(line.point, 1.0)).shrink<3>(),
                    .dir = (iTransform * oxm::grow(line.dir, 0.0)).shrink<3>()
                }});
    }
    virtual auto discretize(
            TopoPoint iStart, 
            TopoPoint iEnd, 
            std::vector<TopoPoint>& oPointList) const -> void
    {
        oPointList.push_back(iStart);
        oPointList.push_back(iEnd);
    }
    virtual auto clone() const -> std::unique_ptr<ITopoCurve>
    {
        return std::make_unique<TopoLineCurve>(line);
    }
    oxm::Line3D<double> line;
};

}
