#pragma once
#include <memory>

namespace ox {

namespace tree {
template <typename TREE>
auto height(const TREE& t, typename TREE::node_t* n) noexcept -> size_t
{
    if (n == t.end())
        return 0;
    auto lsize = ox::tree::height(t, n->left);
    auto rsize = ox::tree::height(t, n->right);
    return 1 + ((lsize > rsize) ? lsize : rsize);
}

template <typename TREE, typename F>
auto inorderwalk(const TREE& t, typename TREE::node_t* n, const F& func) noexcept -> void
{
    if (n != t.end())
    {
        inorderwalk(t, n->left, func);
        func(n->data);
        inorderwalk(t, n->right, func);
    }
}
template <typename TREE>
auto height(const TREE& t) noexcept -> size_t { return ox::tree::height(t, t.root()); }

template <typename TREE, typename F>
auto inorderwalk(const TREE& t, const F& func) noexcept -> void { ox::tree::inorderwalk(t, t.root(), func); }

template<typename TREE, typename K, typename GreaterFunc>
auto lower(const TREE& t, const K& elem, const GreaterFunc& greaterFunc) 
    noexcept -> typename TREE::node_t*
{
    auto minimum = t.end();
    auto current = t.root();
    while(current != t.end())
    {
        if (greaterFunc(elem, current->data))
        {
            minimum = current;
            current = current->right;
        }
        else
        {
            current = current->left;
        }
    }
    return minimum;
}

template<typename TREE, typename K, typename LessFunc2>
auto upper(const TREE& t, const K& elem, const LessFunc2& lessFunc) 
    noexcept -> typename TREE::node_t*
{
    auto minimum = t.end();
    auto current = t.root();
    while(current != t.end())
    {
        if (lessFunc(elem, current->data))
        {
            minimum = current;
            current = current->left;
        }
        else
        {
            current = current->right;
        }
    }
    return minimum;
}
}

template<typename T>
struct node
{
    node* left;
    node* right;
    node* parent;
    T data;
};

template<typename T>
struct rbnode
{
    rbnode* left;
    rbnode* right;
    rbnode* parent;
    bool isred;
    T data;
};

template<typename T, typename Alloc = std::allocator<node<T>>, 
    typename LessFunc = std::less<T>, typename  EqualFunc = std::equal_to<T>>
class bst
{
public: using node_t = node<T>;
public:
    explicit bst(): mRoot(nullptr), mAllocator(), mLess(), mEqual() {}
    explicit bst(const LessFunc& comp, const EqualFunc& equal): 
        mRoot(nullptr), mAllocator(), mLess(comp), mEqual(equal){}
    explicit bst(const Alloc& alloc, const LessFunc& comp, const EqualFunc& equal): 
        mRoot(nullptr), mAllocator(alloc), mLess(comp), mEqual(equal){}
    bst(const bst&) = delete;
    ~bst() { if(mRoot) destroy(mRoot); } 

    auto root() const noexcept -> node_t* { return mRoot; }
    auto end() const noexcept -> node_t* { return nullptr; }

    auto search(const T& elem) const noexcept -> node_t*
    {
        auto current = mRoot;
        while(current && !mEqual(elem, current->data))
        {
            current = mLess(elem, current->data) ? current->left : current->right;  
        }
        return current;
    }

    template<typename K, typename EqualFuncCustom>
    auto search(const K& elem, const EqualFuncCustom& equalFunc) const noexcept -> node_t*
    {
        auto current = mRoot;
        while(current && !equalFunc(elem, current->data))
        {
            current = mLess(elem, current->data) ? current->left : current->right;  
        }
        return current;
    }

    auto insert(const T& elem) noexcept -> node_t*
    {
        auto n = mAllocator.allocate(1);
        *n = node_t
        {
            .left = nullptr,
            .right = nullptr,
            .parent = nullptr,
            .data = elem,
        };
        
        auto current = mRoot;
        while (current)
        {
            n->parent = current;
            if (mLess(elem, current->data))
                current = current->left;
            else
                current = current->right;
        }

        auto parent = n->parent;
        if (!parent)
            mRoot = n;
        else if (mLess(n->data, parent->data))
            parent->left = n;
        else
            parent->right = n;
        return n;
    }

    auto remove(node_t* n) noexcept -> void
    {
        if (n->left == nullptr)
            transplant(n, n->right);
        else if (n->right == nullptr)
            transplant(n, n->left);
        else
        {
            auto minimum = min(n->right);
            if(minimum != n)
            {
                transplant(minimum, minimum->right);
                minimum->right = n->right;
                minimum->right->parent = minimum;
            }
            transplant(n, minimum);
            minimum->left = n->left;
            minimum->left->parent = minimum;
        }

        mAllocator.deallocate(n, 1);
    }

    auto min() const noexcept -> node_t*
    {
        auto current = mRoot;
        if (!current)
            return current;
        return min(current);
    }

    auto max() const noexcept -> node_t*
    {
        auto current = mRoot;
        if (!current)
            return current;
        return max(current);
    }

    auto next(const node_t* n) const noexcept -> node_t*
    {
        if (n->right != nullptr)
            return min(n->right);
        auto nextNode = n->parent;
        auto current = n;
        while(nextNode && current == nextNode->right)
        {
            current = nextNode;
            nextNode = nextNode->parent;
        }
        return nextNode;
    }

    auto prev(const node_t* n) const noexcept -> node_t*
    {
        if (n->left != nullptr)
            return max(n->left);
        auto nextNode = n->parent;
        auto current = n;
        while(nextNode && current == nextNode->left)
        {
            current = nextNode;
            nextNode = nextNode->parent;
        }
        return nextNode;
    }

private:

    auto transplant(node_t* nodeToReplace, node_t* replaceWithNode) noexcept -> void
    {
        if (nodeToReplace->parent == nullptr)
            mRoot = replaceWithNode;
        else if(nodeToReplace == nodeToReplace->parent->left)
            nodeToReplace->parent->left = replaceWithNode;
        else
            nodeToReplace->parent->right = replaceWithNode;
        if(replaceWithNode)
            replaceWithNode->parent = nodeToReplace->parent;
    }
    static auto min(node_t* current) noexcept -> node_t*
    {
        while (current->left)
            current = current->left;
        return current;
    }    
    static auto max(node_t* current) noexcept -> node_t*
    {
        while (current->right)
            current = current->right;
        return current;
    }
    auto destroy(node_t* current) noexcept -> void
    {
        auto left = current->left;
        auto right = current->right;
        mAllocator.deallocate(current, 1);
        if (left)
            destroy(left);
        if (right)
            destroy(right);
    }

private:
    node_t* mRoot;
    Alloc mAllocator;
    LessFunc mLess;
    EqualFunc mEqual;
};

template<typename T, typename Alloc = std::allocator<rbnode<T>>, 
    typename LessFunc = std::less<T>, typename  EqualFunc = std::equal_to<T>>
class rbtree
{
public: using node_t = rbnode<T>;
public:
    explicit rbtree(): mAllocator(), mLess(), mEqual() 
    {
        createNil();
        mRoot = mNil;
    }
    explicit rbtree(const LessFunc& comp, const EqualFunc& equal): 
        mAllocator(), mLess(comp), mEqual(equal)
    {
        createNil();
        mRoot = mNil;
    }
    explicit rbtree(const Alloc& alloc, const LessFunc& comp, const EqualFunc& equal): 
        mAllocator(alloc), mLess(comp), mEqual(equal)
    {
        createNil();
        mRoot = mNil;
    }
    rbtree(const rbtree&) = delete;
    ~rbtree() 
    { 
        if((mRoot != nullptr) && (mRoot != mNil)) destroy(mRoot); 
        mAllocator.deallocate(mNil, 1);
    }

    auto root() const noexcept -> node_t* { return mRoot; }
    auto end() const noexcept -> node_t* { return mNil; }

    auto search(const T& elem) const noexcept -> node_t*
    {
        auto current = mRoot;
        while((current != mNil) && !mEqual(elem, current->data))
        {
            current = mLess(elem, current->data) ? current->left : current->right;  
        }
        return current;
    }

    template<typename K, typename EqualFuncCustom>
    auto search(const K& elem, const EqualFuncCustom& equalFunc) const noexcept -> node_t*
    {
        auto current = mRoot;
        while((current != mNil) && !equalFunc(elem, current->data))
        {
            current = mLess(elem, current->data) ? current->left : current->right;  
        }
        return current;
    }

    auto insert(const T& elem) noexcept -> node_t*
    {
        auto z = mAllocator.allocate(1);
        *z = node_t
        {
            .left = mNil,
            .right = mNil,
            .parent = mNil,
            .isred = true,
            .data = elem,
        };
        
        auto y = mNil;
        auto x = mRoot;

        while (x != mNil)
        {
            y = x;
            x = mLess(z->data, x->data) ? x->left : x->right;
        }

        z->parent = y;
        if (y == mNil)
            mRoot = z;
        else if (mLess(z->data, y->data))
            y->left = z;
        else
            y->right = z;

        insertfixup(z);
        return z;
    }

    auto remove(node_t* z) noexcept -> void
    {
        auto y = z;
        node_t* x = nullptr;
        auto yisred = y->isred;
        if (z->left == mNil)
        {
            x = z->right;
            transplant(z, z->right);
        }
        else if (z->right == mNil)
        {
            x = z->left;
            transplant(z, z->left);
        }
        else
        {
            y = min(z->right);
            yisred = y->isred;
            x = y->right;
            if (y->parent == z)
                x->parent = y;
            else
            {
                transplant(y, y->right);
                y->right = z->right;
                y->right->parent = y;
            }
            transplant(z, y);
            y->left = z->left;
            y->left->parent = y;
            y->isred = z->isred;
        }
        if (!yisred)
            deletefixup(x);
        
        mAllocator.deallocate(z, 1);
    }

    auto min() const noexcept -> node_t*
    {
        auto current = mRoot;
        if (!current)
            return current;
        return min(current);
    }

    auto max() const noexcept -> node_t*
    {
        auto current = mRoot;
        if (!current)
            return current;
        return max(current);
    }

    auto next(const node_t* n) const noexcept -> node_t*
    {
        if (n->right != mNil)
            return min(n->right);
        auto nextNode = n->parent;
        auto current = n;
        while(nextNode && current == nextNode->right)
        {
            current = nextNode;
            nextNode = nextNode->parent;
        }
        return nextNode;
    }

    auto prev(const node_t* n) const noexcept -> node_t*
    {
        if (n->left != mNil)
            return max(n->left);
        auto nextNode = n->parent;
        auto current = n;
        while(nextNode && current == nextNode->left)
        {
            current = nextNode;
            nextNode = nextNode->parent;
        }
        return nextNode;
    }

private:
    auto lrotate(node_t* x) noexcept ->void
    {
        auto y = x->right;
        x->right = y->left;
        if (y->left != mNil)
            y->left->parent = x;
        y->parent = x->parent;
        if (x->parent == mNil)
            mRoot = y;
        else if (x == x->parent->left)
            x->parent->left = y;
        else
            x->parent->right = y;
        y->left = x;
        x->parent = y;
    }
    auto rrotate(node_t* x) noexcept ->void
    {
        auto y = x->left;
        x->left = y->right;
        if (y->right != mNil)
            y->right->parent = x;
        y->parent = x->parent;
        if (x->parent == mNil)
            mRoot = y;
        else if (x == x->parent->right)
            x->parent->right = y;
        else
            x->parent->left = y;
        y->right = x;
        x->parent = y;
    }
    auto insertfixup(node_t* z) noexcept -> void
    {
        while(z->parent->isred)
        {
            if (z->parent == z->parent->parent->left)
            {
                auto y = z->parent->parent->right;
                if (y->isred)
                {
                    z->parent->isred = false;
                    y->isred = false;
                    z->parent->parent->isred = true;
                    z = z->parent->parent;
                }
                else
                {
                    if (z == z->parent->right)
                    {
                        z = z->parent;
                        lrotate(z);
                    }
                    z->parent->isred = false;
                    z->parent->parent->isred = true;
                    rrotate(z->parent->parent);
                }
            }
            else
            {
                auto y = z->parent->parent->left;
                if (y->isred)
                {
                    z->parent->isred = false;
                    y->isred = false;
                    z->parent->parent->isred = true;
                    z = z->parent->parent;
                }
                else
                {
                    if (z == z->parent->left)
                    {
                        z = z->parent;
                        rrotate(z);
                    }
                    z->parent->isred = false;
                    z->parent->parent->isred = true;
                    lrotate(z->parent->parent);
                }
            }
        }

        mRoot->isred = false;
    }
    auto deletefixup(node_t* x) noexcept -> void
    {
        while ((x != mRoot) && !x->isred)
        {
            if (x == x->parent->left)
            {
                auto w = x->parent->right;
                if (w->isred)
                {
                    w->isred = false;
                    x->parent->isred = true;
                    lrotate(x->parent);
                    w = x->parent->right;
                }
                if (!w->left->isred && !w->right->isred)
                {
                    w->isred = true;
                    x = x->parent;
                }
                else
                {
                    if (!w->right->isred)
                    {
                        w->left->isred = false;
                        w->isred = true;
                        rrotate(w);
                        w = x->parent->right;
                    }
                    w->isred = x->parent->isred;
                    x->parent->isred = false;
                    w->right->isred = false;
                    lrotate(x->parent);
                    x = mRoot;
                }
            }
            else
            {
                auto w = x->parent->left;
                if (w->isred)
                {
                    w->isred = false;
                    x->parent->isred = true;
                    rrotate(x->parent);
                    w = x->parent->left;
                }
                if (!w->right->isred && !w->left->isred)
                {
                    w->isred = true;
                    x = x->parent;
                }
                else
                {
                    if (!w->left->isred)
                    {
                        w->right->isred = false;
                        w->isred = true;
                        lrotate(w);
                        w = x->parent->left;
                    }
                    w->isred = x->parent->isred;
                    x->parent->isred = false;
                    w->left->isred = false;
                    rrotate(x->parent);
                    x = mRoot;
                }
            }
        }
        x->isred = false;
    }
    auto transplant(node_t* nodeToReplace, node_t* replaceWithNode) noexcept -> void
    {
        if (nodeToReplace->parent == mNil)
            mRoot = replaceWithNode;
        else if(nodeToReplace == nodeToReplace->parent->left)
            nodeToReplace->parent->left = replaceWithNode;
        else
            nodeToReplace->parent->right = replaceWithNode;
        replaceWithNode->parent = nodeToReplace->parent;
    }
    auto min(node_t* current) const noexcept -> node_t*
    {
        while (current->left != mNil)
            current = current->left;
        return current;
    }    
    auto max(node_t* current) const noexcept -> node_t*
    {
        while (current->right != mNil)
            current = current->right;
        return current;
    }
    auto destroy(node_t* current) noexcept -> void
    {
        auto left = current->left;
        auto right = current->right;
        mAllocator.deallocate(current, 1);
        if (left != mNil)
            destroy(left);
        if (right != mNil)
            destroy(right);
    }
    auto createNil() noexcept -> void 
    {
        mNil = mAllocator.allocate(1);
        *mNil = node_t
        {
            .left = nullptr,
            .right = nullptr,
            .parent = nullptr,
            .isred = false,
            .data = {}
        };
    }
private:
    node_t* mRoot;
    node_t* mNil;
    Alloc mAllocator;
    LessFunc mLess;
    EqualFunc mEqual;
};
}
