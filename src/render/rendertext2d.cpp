#include "render/rendercommon.hpp"
#include <render/rendertext2d.hpp>
#include <render/pipelines.hpp>
#include <vulkan/vulkan_core.h>

namespace
{
    struct Constants
    {
        oxm::vf2 view_size_over_2;
        oxm::vf2 view_2_over_size;
        oxm::vf2 screen_pos;
    };
}

auto ox::BufferText2D::add(const std::u32string_view iText, const oxm::vf2& iPos, const oxm::vu4& iColor) -> void
{
    mVertices.reserve(mVertices.size() + iText.size() * 6);
    auto font = mRender->get(ox::FontResource::MAIN_FONT);
    const auto advance = float(font->mAdvance);
    // TODO: check line height space
    const auto height = float(font->mHeight) + 2;
    oxm::vf2 origin = iPos;
    const auto color = oxm::packUnorm4x8(iColor);
    for (int i = 0; i < iText.size(); i++) 
    {
        const auto c = iText[i];
        if (c > 32 && c < 127)
        {
            const auto& g = font->mGlyphMap.find(c)->second;
            const ox::CharVertex v[4] = {
                {
                    .pos = { float(g.x0) + origin[0], float(g.y0) + origin[1] },
                    .uv = { float(g.u0), float(g.v0) },
                    .col = color, .image = 0,
                },
                {
                    .pos = { float(g.x1) + origin[0], float(g.y0) + origin[1] },
                    .uv = { float(g.u1), float(g.v0) },
                    .col = color, .image = 0,
                },
                {
                    .pos = { float(g.x1) + origin[0], float(g.y1) + origin[1] },
                    .uv = { float(g.u1), float(g.v1) },
                    .col = color, .image = 0,
                },
                {
                    .pos = { float(g.x0) + origin[0], float(g.y1) + origin[1] },
                    .uv = { float(g.u0), float(g.v1) },
                    .col = color, .image = 0,
                },
            };
            mVertices.push_back(v[0]);
            mVertices.push_back(v[1]);
            mVertices.push_back(v[3]);

            mVertices.push_back(v[1]);
            mVertices.push_back(v[2]);
            mVertices.push_back(v[3]);
        }

        if (c == '\n')
        {
            origin[0] = iPos[0];
            origin[1] += height;
        }
        else if (c == '\t')
            origin[0] += advance * 4;
        else
            origin[0] += advance;
    }

    mShouldLoad[mNextBucket] = true;
    mVerticesCounts[mNextBucket] = mVertices.size();
}

auto ox::BufferText2D::font() const noexcept -> const BitmapFont*
{
    return mRender->get(ox::FontResource::MAIN_FONT);
}

auto ox::BufferText2D::clear() noexcept -> void
{
    mVertices.clear();
    mShouldLoad[mNextBucket] = true;
    mVerticesCounts[mNextBucket] = mVertices.size();
}

auto ox::BufferText2D::load(VkCommandBuffer iCommandBuffer) -> bool
{
    if (!mShouldLoad[mNextBucket])
        return false;

    mShouldLoad[mNextBucket] = false;
    mCurrentBucket = mNextBucket;
    mNextBucket = (mNextBucket + 1) % MAX_CONCURRENT_FRAMES;

    if (mVertices.empty())
        return false;

    // Should use mCyrrentBucket now

    auto bytes = mVertices.size() * sizeof(CharVertex);
    auto res = mVertexBuffers[mCurrentBucket].recreate(mRender->getAllocator(), 
            VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, bytes);
    CHECK_VK(res, "[ERROR] Vertex buffer creation failed: %i\n");
    mVertexBuffers[mCurrentBucket].write(mVertices.data(), bytes, 0);

    VkBufferCopy region =
    {
        .srcOffset = 0,
        .dstOffset = 0,
        .size = mVertexBuffers[mCurrentBucket].size()
    };
    
    vkCmdCopyBuffer(iCommandBuffer, mVertexBuffers[mCurrentBucket].cpubuffer(), 
            mVertexBuffers[mCurrentBucket].gpubuffer(), 1, &region);

    return true;
}

auto ox::BufferText2D::destroy() noexcept -> void 
{
    for (auto& b : mVertexBuffers) 
        b.destroy(mRender->getAllocator());

    *this = BufferText2D{};
}

auto ox::BufferText2D::buffer() const noexcept -> const VkBuffer&
{
    return mVertexBuffers[mCurrentBucket].gpubuffer();
}

auto ox::BufferText2D::size() const noexcept -> oxm::u32
{
    return mVerticesCounts[mCurrentBucket];
}


auto ox::RenderText2D::create(const Render* iRender) -> bool
{
    mRender = iRender;

    CALL_CHECK_DESTROY_RETURN(createLayouts());
    CALL_CHECK_DESTROY_RETURN(createPipeline());
    CALL_CHECK_DESTROY_RETURN(createDescriptorSets());

    return true;
}

auto ox::RenderText2D::destroy() -> void 
{
    auto device = mRender->getDevice();

    if (mPipeline)
        vkDestroyPipeline(device, mPipeline, nullptr);

    if (mDescriptorSetLayout)
        vkDestroyDescriptorSetLayout(device, mDescriptorSetLayout, nullptr);

    if (mPipelineLayout)
        vkDestroyPipelineLayout(device, mPipelineLayout, nullptr);

    if (mSampler)
        vkDestroySampler(device, mSampler, nullptr);

    if (mDescriptorPool)
        vkDestroyDescriptorPool(device, mDescriptorPool, nullptr);

    *this = {};
}

auto ox::RenderText2D::createDescriptorSets() -> bool
{
    VkDescriptorPoolSize poolSize = 
    {
        .type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
        .descriptorCount = 1
    };
    VkDescriptorPoolCreateInfo poolInfo = 
    {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .maxSets = 1, // TODO: Maybe 2
        .poolSizeCount = 1,
        .pPoolSizes = &poolSize
    };

    auto result = vkCreateDescriptorPool(mRender->getDevice(), &poolInfo, nullptr, &mDescriptorPool);
    CHECK_AND_RETURN(result, "[ERROR] Descriptor pool creation failed: %i\n");

    VkDescriptorSetAllocateInfo info = 
    {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
        .pNext = nullptr,
        .descriptorPool = mDescriptorPool,
        .descriptorSetCount = 1,
        .pSetLayouts = &mDescriptorSetLayout
    };

    result = vkAllocateDescriptorSets(mRender->getDevice(), &info, &mDescriptorSet);
    CHECK_AND_RETURN(result, "[ERROR] Descriptor set allocation failed: %i\n");

    VkSamplerCreateInfo samplerInfo = 
    {
        .sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .magFilter = VK_FILTER_LINEAR,
        .minFilter = VK_FILTER_LINEAR,
        .mipmapMode = VK_SAMPLER_MIPMAP_MODE_NEAREST,
        .addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
        .addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
        .addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT,
        .mipLodBias = 0,
        .anisotropyEnable = VK_FALSE,
        .maxAnisotropy = 0,
        .compareEnable = VK_FALSE,
        .compareOp = VkCompareOp::VK_COMPARE_OP_NEVER,
        .minLod = 0,
        .maxLod = 0,
        .borderColor = VK_BORDER_COLOR_FLOAT_TRANSPARENT_BLACK,
        .unnormalizedCoordinates = VK_TRUE
    };
    result = vkCreateSampler(mRender->getDevice(), &samplerInfo, nullptr, &mSampler);
    CHECK_AND_RETURN(result, "[ERROR] Sampler creation failed: %i\n");

    VkDescriptorImageInfo imageInfo = 
    {
        .sampler = mSampler,
        .imageView = mRender->get(FontResource::MAIN_FONT)->mAtlas.view,
        .imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
    };
    VkWriteDescriptorSet write = 
    {
        .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
        .pNext = nullptr,
        .dstSet = mDescriptorSet,
        .dstBinding = 0,
        .dstArrayElement = 0,
        .descriptorCount = 1,
        .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
        .pImageInfo = &imageInfo,
        .pBufferInfo = nullptr,
        .pTexelBufferView = nullptr
    };
    vkUpdateDescriptorSets(mRender->getDevice(), 1, &write, 0, nullptr);
    return true;
}

auto ox::RenderText2D::createLayouts() -> bool
{
    std::array<VkDescriptorSetLayoutBinding, 1> bindings = 
    {
        VkDescriptorSetLayoutBinding{
            .binding = 0,
            .descriptorType = VkDescriptorType::VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
            .descriptorCount = 1,
            .stageFlags = VkShaderStageFlagBits::VK_SHADER_STAGE_FRAGMENT_BIT,
            .pImmutableSamplers = nullptr
        },
    };

    VkDescriptorSetLayoutCreateInfo descriptorInfo = 
    {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO, .pNext = nullptr, .flags = 0,
        .bindingCount = bindings.size(),
        .pBindings = bindings.data()
    };
    auto result = vkCreateDescriptorSetLayout(mRender->getDevice(), &descriptorInfo, nullptr, &mDescriptorSetLayout);
    CHECK_AND_RETURN(result, "Descriptor layout creation failed: %i\n");

    VkPushConstantRange pushConstantRange = 
    {
        .stageFlags = VkShaderStageFlagBits::VK_SHADER_STAGE_VERTEX_BIT,
        .offset = 0,
        .size = sizeof(Constants)
    };
    VkPipelineLayoutCreateInfo layoutInfo = 
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .setLayoutCount = 1,
        .pSetLayouts = &mDescriptorSetLayout,
        .pushConstantRangeCount = 1,
        .pPushConstantRanges = &pushConstantRange
    };

    result = vkCreatePipelineLayout(mRender->getDevice(), &layoutInfo, nullptr, &mPipelineLayout);
    CHECK_AND_RETURN(result, "Pipeline layout creation failed: %i\n");

    return true;
}

auto ox::RenderText2D::createPipeline() -> bool
{
    auto result = ox::pipelines::text2d(mRender->getDevice(), mPipelineLayout, 
            mRender->get(ox::ShaderResource::TEXT2D_VERT), 
            mRender->get(ox::ShaderResource::TEXT2D_FRAG), 
            mRender->getTargetColorFormat(), 
            &mPipeline);
    CHECK_AND_RETURN(result, "[ERROR] Pipeline creation failed: %i\n");
    return true;
}

auto ox::RenderText2D::drawBegin(VkCommandBuffer iCommandBuffer, 
        const VkRect2D& iRect, VkImageView iColorView) -> void
{ 
    VkViewport viewport = 
    {
        //.x = (float)mCreateInfo.rect.offset.x,
        .x = (float)iRect.offset.x,
        .y = (float)iRect.offset.y,
        .width = (float)iRect.extent.width,
        .height = (float)iRect.extent.height,
        .minDepth = 0.0f,
        .maxDepth = 1.0f
    };

    VkRenderingAttachmentInfo colorAttachment = 
    {
        .sType = VK_STRUCTURE_TYPE_RENDERING_ATTACHMENT_INFO,
        .pNext = nullptr,
        //.imageView = mCreateInfo.screenTarget->getFinalImage(iFrame.image).view,
        .imageView = iColorView,
        .imageLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
        .resolveMode = VkResolveModeFlagBits::VK_RESOLVE_MODE_NONE,
        .resolveImageView = VK_NULL_HANDLE, 
        .resolveImageLayout = VK_IMAGE_LAYOUT_UNDEFINED,
        .loadOp = VkAttachmentLoadOp::VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        .storeOp = VkAttachmentStoreOp::VK_ATTACHMENT_STORE_OP_STORE,
        .clearValue = { .color = {0.05f, 0.05f, 0.05f, 1.0f} }
    };

    VkRenderingInfo renderingInfo = 
    {
        .sType = VK_STRUCTURE_TYPE_RENDERING_INFO,
        .pNext = nullptr,
        .flags = 0, // TODO: maybe here can be something usefull
        .renderArea = iRect,
        .layerCount = 1, 
        .viewMask = 0,
        .colorAttachmentCount = 1,
        .pColorAttachments = &colorAttachment,
        .pDepthAttachment = nullptr, 
        .pStencilAttachment = nullptr
    };

    vkCmdBeginRendering(iCommandBuffer, &renderingInfo);

    Constants pushConstants = 
    {
        .view_size_over_2 = {viewport.width * 0.5f, viewport.height* 0.5f},
        .view_2_over_size = {2 / viewport.width, 2 / viewport.height},
        // TODO: HARDCODED
        .screen_pos = { 0, 0 }
    };

    vkCmdBindDescriptorSets(iCommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, mPipelineLayout, 
            0, 1, &mDescriptorSet, 0, nullptr);

    vkCmdBindPipeline(iCommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, mPipeline);
    vkCmdSetViewport(iCommandBuffer, 0, 1, &viewport);
    vkCmdSetScissor(iCommandBuffer, 0, 1, &iRect);
    vkCmdPushConstants(iCommandBuffer, mPipelineLayout, VK_SHADER_STAGE_VERTEX_BIT, 0, sizeof(pushConstants), &pushConstants);
}
auto ox::RenderText2D::draw(VkCommandBuffer iCommandBuffer, const BufferText2D& iBuffer) -> void
{
    VkDeviceSize offsets[1] = {0};
    vkCmdBindVertexBuffers(iCommandBuffer, 0, 1, &iBuffer.buffer(), offsets);
    vkCmdDraw(iCommandBuffer, iBuffer.size(), 1, 0, 0);
}
auto ox::RenderText2D::drawEnd(VkCommandBuffer iCommandBuffer) -> void
{
    vkCmdEndRendering(iCommandBuffer);
}
