#pragma once

#include <cinttypes>
#include <compare>
#include <concepts>
#include <functional>
#include <optional>
#include <memory>
#include <span>

#include <containers.hpp>

#include <topobase.hpp>
#include <graphentities.hpp>
#include <definitions.hpp>

namespace ox
{
struct Halfedge
{
    using TableIdx = HalfedgeIdx;
    
    HalfedgeIdx prev;
    HalfedgeIdx next;
    HalfedgeIdx twin;
    VertexIdx start;
    LoopIdx loop;
    EdgeIdx edge;
};

struct Edge
{
    using TableIdx = EdgeIdx;

    HalfedgeIdx half; 
    std::unique_ptr<ITopoCurve> curve;
};

struct Loop
{
    using TableIdx = LoopIdx;

    HalfedgeIdx start;
    std::optional<LoopIdx> next;
    FaceIdx face;
};

struct Face
{
    using TableIdx = FaceIdx;

    LoopIdx loop; // assumption: first loop in chain is always external, rest are internal.
    std::unique_ptr<ITopoSurface> surface;
};

class BRep2D;
class BRep3D
{
public:
    BRep3D() = default;
    CTORS_COPY_DELETE(BRep3D);
    CTORS_MOVE_DEFAULT(BRep3D);
    // initial ops
    static auto createPlanarFace(
            std::vector<std::span<TopoPoint>> iVertices, 
            std::vector<std::span<std::unique_ptr<ITopoCurve>>> iCurves, 
            const oxm::Plane<double>& iPlane) -> ox::maybe<BRep3D>;
    static auto createPlanarFace(
            const BRep2D& iBrep2d, 
            FaceIdx iFace2d, 
            const oxm::Plane<double>& iPlane) -> ox::maybe<BRep3D>;
    //static auto createRectCuboid(TopoPoint iPos, TopoDir iDirX, TopoDir iDirY, double iExtentX, double iExtentY, double iExtentZ) -> std::optional<BRep3D>;
    //static auto createSphere() -> BRep3D;
    // secondary ops
    auto addInnerFace(FaceIdx iFace, std::span<TopoPoint> iVertices, std::span<std::unique_ptr<ITopoCurve>> iCurves) -> bool;
    auto extrudeFace(FaceIdx iFace, double iExtension) -> bool;
 
    auto get(VertexIdx idx) const noexcept -> const Vertex3D& { return mVertices[idx.idx].data; }
    auto get(VertexIdx idx) noexcept -> Vertex3D& { return mVertices[idx.idx].data; }
       
    auto get(HalfedgeIdx idx) const noexcept -> const Halfedge& { return mHalfedges[idx.idx].data; }
    auto get(HalfedgeIdx idx) noexcept -> Halfedge& { return mHalfedges[idx.idx].data; }

    auto get(EdgeIdx idx) const noexcept -> const Edge& { return mEdges[idx.idx].data; }
    auto get(EdgeIdx idx) noexcept -> Edge& { return mEdges[idx.idx].data; }
    
    auto get(LoopIdx idx) const noexcept -> const Loop& { return mLoops[idx.idx].data; }
    auto get(LoopIdx idx) noexcept -> Loop& { return mLoops[idx.idx].data; }

    auto get(FaceIdx idx) const noexcept -> const Face& { return mFaces[idx.idx].data; }
    auto get(FaceIdx idx) noexcept -> Face& { return mFaces[idx.idx].data; }

//#define GETCHAIN_REF(id, f1, f2, f3) get(get(get(get(id).f1).f2).f3)
//#define GETCHAIN_ID(id, f1, f2, f3) get(get(get(id).f1).f2).f3

    auto print(LoopIdx idx) const noexcept -> void;
    auto print(VertexIdx idx) const noexcept -> void;
    auto print(FaceIdx idx) const noexcept -> void;
    auto print(HalfedgeIdx idx) const noexcept -> void;

    template <typename Func> requires std::invocable<Func, LoopIdx>
    auto foreach(FaceIdx idx, const Func& func) const -> void;

    template <typename Func> requires std::invocable<Func, LoopIdx>
    auto foreach(FaceIdx idx, const Func& func) -> void;

    template <typename Func> requires std::invocable<Func, HalfedgeIdx>
    auto foreach(LoopIdx idx, const Func& func) -> void;
    
    template <typename Func> requires std::invocable<Func, HalfedgeIdx>
    auto foreach(LoopIdx idx, const Func& func) const -> void;
    
    template <typename Func> requires std::invocable<Func, HalfedgeIdx>
    auto foreach(FaceIdx idx, const Func& func) -> void;
    
    template <typename Func> requires std::invocable<Func, HalfedgeIdx>
    auto foreach(FaceIdx idx, const Func& func) const -> void;

    template <typename Func> requires std::invocable<Func, const Halfedge&>
    auto foreach(const Face& face, const Func& func) const -> void;
    
    template <typename Func> requires std::invocable<Func, const Loop&>
    auto foreach(const Face& face, const Func& func) const -> void;
    
    template <typename Func> requires std::invocable<Func, const Halfedge&>
    auto foreach(const Loop& loop, const Func& func) const -> void;
    
    template <typename Func> requires std::invocable<Func, const Face&>
    auto foreach(const Vertex3D& vertex, const Func& func) const -> void;
    
    auto getFaces() const -> const std::span<const ox::maybe<Face>> { return mFaces.data(); }
    auto getFaceIds() const -> std::vector<FaceIdx> 
    {
        std::vector<FaceIdx> output;
        for (uint32_t i = 0; i < mFaces.data().size(); i++)
            if (mFaces[i].valid)
                output.push_back({i});
        return output;
    }
    auto getLoops() const -> const std::span<const ox::maybe<Loop>> { return mLoops.data(); }
    auto getLoopIds() const -> std::vector<LoopIdx> 
    {
        std::vector<LoopIdx> output;
        for (uint32_t i = 0; i < mLoops.data().size(); i++)
            if (mLoops[i].valid)
                output.push_back({i});
        return output;
    }
    auto getEdges() const -> const std::span<const ox::maybe<Edge>> { return mEdges.data(); }
    auto getEdgeIds() const -> std::vector<EdgeIdx> 
    {
        std::vector<EdgeIdx> output;
        for (uint32_t i = 0; i < mEdges.data().size(); i++)
            if (mEdges[i].valid)
                output.push_back({i});
        return output;
    }
    auto getHalfedges() const -> const std::span<const ox::maybe<Halfedge>> { return mHalfedges.data(); }
    auto getHalfedgeIds() const -> std::vector<HalfedgeIdx> 
    {
        std::vector<HalfedgeIdx> output;
        for (uint32_t i = 0; i < mHalfedges.data().size(); i++)
            if (mHalfedges[i].valid)
                output.push_back({i});
        return output;
    }
    auto getVertices() const -> const std::span<const ox::maybe<Vertex3D>> { return mVertices.data(); }
    auto getVertexIds() const -> std::vector<VertexIdx> 
    {
        std::vector<VertexIdx> output;
        for (uint32_t i = 0; i < mVertices.data().size(); i++)
            if (mVertices[i].valid)
                output.push_back({i});
        return output;
    }
    
private:

    auto add(const Vertex3D& data) -> VertexIdx { return {mVertices.add(data)}; }
    auto add(const Halfedge& data) -> HalfedgeIdx { return {mHalfedges.add(data)}; }
    auto add(Edge&& data) -> EdgeIdx { return {mEdges.add(std::move(data))}; }
    auto add(const Loop& data) -> LoopIdx { return {mLoops.add(data)}; }
    auto add(Face&& data) -> FaceIdx { return {mFaces.add(std::move(data))}; }

    ox::permavector<Vertex3D> mVertices;
    ox::permavector<Halfedge> mHalfedges;
    ox::permavector<Edge> mEdges;
    ox::permavector<Loop> mLoops;
    ox::permavector<Face> mFaces;
};

    template <typename Func> requires std::invocable<Func, LoopIdx>
    auto BRep3D::foreach(FaceIdx idx, const Func& func) const -> void
    {
        std::optional<LoopIdx> current = get(idx).loop;
        do { std::invoke(func, current.value()); current = get(current.value()).next; } while (current);
    }

    template <typename Func> requires std::invocable<Func, LoopIdx>
    auto BRep3D::foreach(FaceIdx idx, const Func& func) -> void
    {
        std::optional<LoopIdx> current = get(idx).loop;
        do { std::invoke(func, current.value()); current = get(current.value()).next; } while (current);
    }

    template <typename Func> requires std::invocable<Func, HalfedgeIdx>
    auto BRep3D::foreach(LoopIdx idx, const Func& func) -> void
    {
        HalfedgeIdx start  = get(idx).start;
        HalfedgeIdx current  = start;
        do {
            std::invoke(func, current);
            current = get(current).next;
        }while (current != start);
    }

    template <typename Func> requires std::invocable<Func, HalfedgeIdx>
    auto BRep3D::foreach(LoopIdx idx, const Func& func) const -> void
    {
        HalfedgeIdx start  = get(idx).start;
        HalfedgeIdx current  = start;
        do {
            std::invoke(func, current);
            current = get(current).next;
        }while (current != start);
    }

    template <typename Func> requires std::invocable<Func, HalfedgeIdx>
    auto BRep3D::foreach(FaceIdx idx, const Func& func) -> void
    {
        std::optional<LoopIdx> currentLoop = get(idx).loop;
        do 
        { 
            HalfedgeIdx start  = get(currentLoop.value()).start;
            HalfedgeIdx current  = start;
            do {
                std::invoke(func, current);
                current = get(current).next;
            }while (current != start);
            currentLoop = get(currentLoop.value()).next; 
        } while (currentLoop);
    }

    template <typename Func> requires std::invocable<Func, HalfedgeIdx>
    auto BRep3D::foreach(FaceIdx idx, const Func& func) const -> void
    {
        std::optional<LoopIdx> currentLoop = get(idx).loop;
        do 
        { 
            HalfedgeIdx start  = get(currentLoop.value()).start;
            HalfedgeIdx current  = start;
            do {
                std::invoke(func, current);
                current = get(current).next;
            }while (current != start);
            currentLoop = get(currentLoop.value()).next; 
        } while (currentLoop);
    }

    template <typename Func> requires std::invocable<Func, const Halfedge&>
    auto BRep3D::foreach(const Face& face, const Func& func) const -> void
    {
        std::optional<LoopIdx> currentLoop = face.loop;
        do 
        { 
            HalfedgeIdx start  = get(currentLoop.value()).start;
            HalfedgeIdx current  = start;
            do {
                std::invoke(func, get(current));
                current = get(current).next;
            }while (current != start);
            currentLoop = get(currentLoop.value()).next; 
        } while (currentLoop);
    }

    template <typename Func> requires std::invocable<Func, const Loop&>
    auto BRep3D::foreach(const Face& face, const Func& func) const -> void
    {
        std::optional<LoopIdx> current = face.loop;
        do 
        { 
            const auto& loop = get(current.value());
            std::invoke(func, loop); 
            current = loop.next; 
        } while (current);
    }

    template <typename Func> requires std::invocable<Func, const Halfedge&>
    auto BRep3D::foreach(const Loop& loop, const Func& func) const -> void
    {
        HalfedgeIdx start  = loop.start;
        HalfedgeIdx current  = start;
        do {
            const auto& halfedge = get(current);
            std::invoke(func, halfedge);
            current = halfedge.next;
        }while (current != start);
    }
    
    template <typename Func> requires std::invocable<Func, const Face&>
    auto BRep3D::foreach(const Vertex3D& vertex, const Func& func) const -> void
    {
        HalfedgeIdx start = vertex.input;
        HalfedgeIdx current = start;
        do 
        {
            std::invoke(func, get(get(get(current).loop).face));
            current = get(get(current).twin).prev;
        } while (current != start);
    }
}
