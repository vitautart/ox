#pragma once

#include <cmath>
#include <cstdio>
#include <limits>
#include <oxmath.hpp>
#include <optional>
#include <numbers>
#include <functional>
#include <span>
namespace ox
{
    class EditorCamera3D
    {
    public:
        //-----------------------------------------------------
        // Camera class that can be moved in editor-like style.
        //-----------------------------------------------------
        EditorCamera3D(
                const oxm::vf3& iEye, 
                const oxm::vf3& iTarget, 
                const oxm::vf3& iUp, 
                float iWidth, 
                float iHeight, 
                bool iPerspective, 
                float iFovVertical = std::numbers::pi_v<float> * 0.5f) : 
            mEye(iEye), 
            mTarget(iTarget), 
            mUp(iUp),
            mSize({iWidth, iHeight}),
            mFovVertical(iFovVertical), 
            mPerspective(iPerspective){};

        auto getView() const noexcept -> oxm::mf4 
        {
            return oxm::look_at_rh(mEye, mTarget, mUp);
        }

        // TODO: near and far plane hardcoded now. Maybe it would be interesting to implement "inverse z".
        // TODO: currently we linked ortho projection to target plane size, 
        // this allows to be consistent and not repeat code,
        // but if in a future we would change target plane then it can cause problem,
        // in that case we need to check if it is orhto and change another parameter to achiveour goal.
        auto getProj() const noexcept -> oxm::mf4 
        {
            if (mPerspective)
                return oxm::persp_proj_rh<float>(mFovVertical, mSize[0], mSize[1], 0.01f, 100.0f);
            else
            {
                auto size = getTargetPlaneSize();
                return oxm::ortho_proj_rh(size[0], size[1], 0.01f, 100.0f);
            }
        }

        auto pan(oxm::vf2 iMousePos) -> void
        {
            if (!mPrevPanPos)
            {
                mPrevPanPos = iMousePos;
                return;
            }

            auto mousePos = mapScreenToTargetPlane(iMousePos);
            auto prevPos = mapScreenToTargetPlane(mPrevPanPos.value());

            auto delta = mousePos - prevPos;
            if (oxm::length_sq(delta) < 0.00001f)
            {
                return;
            }
            auto worldDir = delta[1] * mUp + delta[0] * getRightDir();

            mTarget = mTarget - worldDir;
            mEye = mEye - worldDir;

            mPrevPanPos = iMousePos;
        }

        auto resetPan() -> void
        {
            mPrevPanPos = std::nullopt;
        }

        auto resetArcball() -> void
        {
            mPrevArcballPos = std::nullopt;
        }
        // TODO: currently it is simple implemetation, need to be added tracked zoom
        auto zoom(float iFactor) -> void
        {
            auto d = mTarget - mEye;
            if (oxm::length_sq(d) > 0.00000001f)
                mEye = mEye + d * iFactor;
        }

        auto arcball(oxm::vf2 iMousePos) noexcept -> void
        {
            if (!mPrevArcballPos)
            {
                mPrevArcballPos = iMousePos;
                return;
            }

            oxm::vf3 current = mapScreenToArcball(iMousePos);
            oxm::vf3 prev = mapScreenToArcball(mPrevArcballPos.value());
            oxm::vf3 axis = oxm::cross(prev, current);
            if (oxm::length_sq(axis) < 4 * std::numeric_limits<float>::epsilon())
            {
                mPrevArcballPos = iMousePos;
                return;
            }

            float angle = std::acos(oxm::dot(prev, current));

            axis = oxm::norm(axis);
            auto view = getView();
            view[3] = {0, 0, 0, 1};

            axis = (oxm::transpose(view) * oxm::grow(axis, 0.0f)).shrink<3>();

            auto rotation = oxm::transpose(oxm::rot(axis, angle));
            mEye = (rotation * oxm::grow(mEye, 1.0f)).shrink<3>();
            mUp = (rotation * oxm::grow(mUp, 0.0f)).shrink<3>();

            mPrevArcballPos = iMousePos;

        }

        auto setSize(float iWidth, float iHeight) noexcept -> void
        {
            mSize = {iWidth, iHeight};
        }
        auto setPerspective(bool iPerspective) noexcept -> void
        {
            mPerspective = iPerspective;
        }
    private:

        auto mapScreenToTargetPlane(oxm::vf2 iMousePos) const noexcept -> oxm::vf2
        {
            auto targetSize = getTargetPlaneSize();
            return  oxm::linmap(iMousePos, {0, 0}, mSize, 
                    {- targetSize[0] / 2, targetSize[1] / 2}, 
                    {targetSize[0] / 2, - targetSize[1] / 2});
        }

        auto mapScreenToArcball(oxm::vf2 iMousePos) const noexcept -> oxm::vf3
        {
            auto ar = getAspectRatio();
            float halfWidth = ar > 1.0f ? ar : 1.0f;
            float halfHeight = ar > 1.0f ? 1.0f : ar;

            oxm::vf2 v = oxm::linmap(iMousePos, {0,0}, mSize, {-halfWidth, halfHeight}, {halfWidth, -halfHeight});
            float len = oxm::length(v);
            if (len >= 1.0f) 
                return {v[0] / len, v[1] / len, 0};
            else
                return { v[0], v[1], std::sqrt(std::abs(1.0f - (v[0] * v[0] + v[1] * v[1]))) };
        }

        // x local camera axis
        auto getRightDir() const noexcept -> oxm::vf3
        {
            return oxm::norm(oxm::cross(mUp, -getForwardDir()));
        }

        // y local camera axis
        auto getUpDir() const noexcept -> oxm::vf3
        {
            return mUp;
        }

        // -z local camera axis
        auto getForwardDir() const noexcept -> oxm::vf3
        {
            return oxm::norm(mTarget - mEye);
        }

        auto getTargetPlaneSize() const noexcept -> oxm::vf2
        {
            float d = oxm::length(mEye - mTarget);
            return {2 * d * std::tan(getFovHorizontal() * 0.5f), 2 * d * std::tan(mFovVertical * 0.5f)};
        }

        auto getFovHorizontal() const noexcept -> float
        {
            return 2 * std::atan(getAspectRatio() * tan(mFovVertical * 0.5f));
        }

        auto getAspectRatio() const noexcept -> float
        {
            return mSize[0] / mSize[1];
        }


    //private:
    public:
        oxm::vf3 mEye;
        oxm::vf3 mTarget;
        oxm::vf3 mUp;
        oxm::vf2 mSize;
        bool mPerspective;
        float mFovVertical; // in radians
    private:
        std::optional<oxm::vf2> mPrevArcballPos = std::nullopt;
        std::optional<oxm::vf2> mPrevPanPos = std::nullopt;
    };
}
