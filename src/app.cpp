#include "GLFW/glfw3.h"
#include "camera.hpp"
#include "render/render.hpp"
#include "render/renderwindow.hpp"
#include <app.hpp>
#include <discretization.hpp>

static const ox::ui::Style gStyle = 
{
    .bgColorDefault = { 5, 10, 10, 255 },
    .bgColorHighlight = { 60, 60, 60, 255 },
    .bgColorDisabled = { 40, 40, 40, 255 },
    .bgColorInputField = { 0, 15, 15, 255 },
    .borderColorDefault = { 200, 200, 200, 255 },
    .borderColorHighlight = { 120, 255, 120, 255 },
    .borderColorDisabled = { 255, 255, 255, 255 },
    .textColorDefault = { 255, 255, 255, 255 },
    .textColorHighlight = { 255, 255, 255, 255 },
    .textColorDisabled = { 255, 255, 255, 255 },
    .textColorSelected = { 255, 255, 255, 255 },
    .textBgColorSelected = { 255, 255, 255, 255 },
};


ox::AppDocument::AppDocument(App* app):
    mApp{app},
    mModel{},
    mMeshSolid { &app->getRender() },
    mMeshWire { &app->getRender() },
    mModelModified {true}
{
}

ox::AppDocument::AppDocument(App* app, ox::BRep3D&& brep):
    mApp{ app },
    mModel{ std::move(brep) },
    mMeshSolid{ &app->getRender() },
    mMeshWire { &app->getRender() },
    mModelModified{ true }
{
}

auto ox::AppDocument::load(VkCommandBuffer cb) -> bool
{
    if (!mModelModified)
        return false;
    mModelModified = false;

    std::vector<ox::RenderVertex3D> pWires, pSolids;
    std::vector<uint32_t> iWires, iSolids;

    auto disc = ox::Discretization::create(mModel);
    disc.moveWireframePointsTo(pWires);
    disc.moveWireframeIndexesTo(iWires);
    disc.moveSolidPointsTo(pSolids);
    disc.moveSolidIndexesTo(iSolids);

    bool res = mMeshWire.load(cb, pWires, iWires);
    res = mMeshSolid.load(cb, pSolids, iSolids) || res;
    return res;
}


ox::AppWindow::AppWindow(App* app, uint32_t w, uint32_t h, bool& res):
    mApp{app},
    mRenderWindow{},
    mRenderView3D{},
    mRenderShape2D{},
    mRenderText2D{},
    // TODO: hardcode
    mCamera{ {0, 0, 4}, {0.0f, 0.0f, 0.0f}, {0, 1, 0}, float(w), float(h), false, std::numbers::pi / 4 },
    mView3DRect{ .offset = { 0, 0}, .extent = { w, h } },
    // TODO: hardcode
    mLeftPanel {ox::ui::Rect{15, 15, 200, 300 }, 8, 32, &app->getRender(), &gStyle},
    mInputState { AppInputState::NONE },
    mUIHoverState { AppUIHoverState::NONE },
    mCurrentFocus { nullptr, nullptr }
{
    auto& render = app->getRender();
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    glfwWindowHint(GLFW_RESIZABLE, false);
    mGLFWWindow = glfwCreateWindow(w, h, "OX CAD", NULL, NULL);
    res = mGLFWWindow != nullptr;
    if (!res)
    {
        printf("[ERROR] Can't create window\n");
        return;
    }
    glfwSetCharCallback(mGLFWWindow, fnTextInputExterior);
    glfwSetKeyCallback(mGLFWWindow, fnKeyPressExterior);
    glfwSetMouseButtonCallback(mGLFWWindow, fnMousePressExterior);
    glfwSetCursorPosCallback(mGLFWWindow, fnMouseMoveExterior);
    glfwSetScrollCallback(mGLFWWindow, fnScrollExterior);
    glfwSetWindowSizeCallback(mGLFWWindow, fnResizeExterior);
    glfwSetWindowUserPointer(mGLFWWindow, this);

    ox::RenderWindowCreateInfo info = 
    {
        .render = &render,
        .window = mGLFWWindow,
        .gfxCompQueueFamily = render.getGfxCompQueue().family,
        .presentQueueFamily = render.getPresentQueue().family,
    };
    res = mRenderWindow.create(info, [&render] (VkFormat format, VkColorSpaceKHR space)
    {
        render.setTargetColorFormat(format);
        render.setTargetColorSpace(space);
    });
    if (!res) return;

    ox::RenderViewCreateInfo viewInfo = 
    {
        .render = &render,
        .frameCount = render.getFrameCount(),
    };
    res = mRenderView3D.create(viewInfo);
    if (!res) return;
    
    res = mRenderShape2D.create(&render);
    if (!res) return;

    res = mRenderText2D.create(&render);
    if (!res) return;

    // TODO: hardcoded ui
    // TODO: maybe move this to some another function or object
    {
        auto button1 = std::make_unique<ox::ui::Button>(U"Button 1", &gStyle);
        button1->setClickHandler([]{printf("PRESS BUTTON\n");});
        std::vector<std::unique_ptr<ox::ui::Element>> elems; 
        {
            elems.push_back(std::make_unique<ox::ui::Label>(U"Label 1", &gStyle));
            elems.push_back(std::move(button1));
            elems.push_back(std::make_unique<ox::ui::Button>(U"Button 2", &gStyle));
            elems.push_back(std::make_unique<ox::ui::Button>(U"Button 3", &gStyle));
            elems.push_back(std::make_unique<ox::ui::SmallInput>(&gStyle));
            elems.push_back(std::make_unique<ox::ui::SmallInput>(&gStyle));
            elems.push_back(std::make_unique<ox::ui::SmallInput>(&gStyle));
            elems.push_back(std::make_unique<ox::ui::SmallInput>(&gStyle));
            elems.push_back(std::make_unique<ox::ui::SmallInput>(&gStyle));
        };
        mLeftPanel.add(elems);
    }

    res = true;
}


ox::AppWindow::~AppWindow() noexcept
{
    mRenderText2D.destroy();
    mRenderShape2D.destroy();
    mRenderView3D.destroy();
    mRenderWindow.destroy();
    glfwDestroyWindow(mGLFWWindow);
}

auto ox::AppWindow::prepare() -> void
{
    auto frame = mRenderWindow.acquire();
    mRenderView3D.updateViewProj(mCamera.getView(), mCamera.getProj(), frame.id);
    mRenderView3D.updateModelTform(oxm::mf4::id(), frame.id);

    int cubeViewMargin = 5;
    int cubeViewSize = 64;
    {
        // TODO: make sure we handle target same as eye case
        auto eye = 1 * oxm::norm(mCamera.mEye - mCamera.mTarget);
        ox::EditorCamera3D navCubeCamera 
        {
            eye, 
            oxm::vf3::zero(), 
            mCamera.mUp, 
            float(cubeViewSize), 
            float(cubeViewSize), 
            //mCamera.mPerspective, 
            false
        };
        mRenderView3D.updateViewProjNavCube(navCubeCamera.getView(), navCubeCamera.getProj(), frame.id);
    }

    mRenderWindow.addCommandBuffer(frame,
    [&](VkCommandBuffer cb, const ox::RenderFrame& frame)
    {
        bool syncView = false;
        // TODO: hardcoded document
        AppDocument* doc = !mApp->getDocuments().empty() ? &mApp->getDocuments().front() : nullptr;
        if (doc) syncView = doc->load(cb);

        bool syncUI = mLeftPanel.load(cb);
        if (syncView || syncUI)
        {
            VkMemoryBarrier2 barrier =
            {
                .sType = VK_STRUCTURE_TYPE_MEMORY_BARRIER_2,
                .pNext = nullptr,
                .srcStageMask = VK_PIPELINE_STAGE_2_COPY_BIT,
                .srcAccessMask = VK_ACCESS_2_TRANSFER_WRITE_BIT,
                .dstStageMask = VK_PIPELINE_STAGE_2_INDEX_INPUT_BIT | VK_PIPELINE_STAGE_2_VERTEX_ATTRIBUTE_INPUT_BIT,
                .dstAccessMask = VK_ACCESS_2_INDEX_READ_BIT | VK_ACCESS_2_VERTEX_ATTRIBUTE_READ_BIT
            };

            VkDependencyInfo deps =
            {
                .sType = VK_STRUCTURE_TYPE_DEPENDENCY_INFO,
                .pNext = nullptr,
                .dependencyFlags = 0,
                .memoryBarrierCount = 1,
                .pMemoryBarriers = &barrier,
                .bufferMemoryBarrierCount = 0,
                .pBufferMemoryBarriers = nullptr,
                .imageMemoryBarrierCount = 0,
                .pImageMemoryBarriers = nullptr
            };
            vkCmdPipelineBarrier2(cb, &deps);
        }

        mRenderView3D.drawBegin(cb, mView3DRect, frame.viewColorFinal, 
                frame.viewDepthFinal, frame.viewColorMultisampled, frame.id);
        if (doc)
        {
            mRenderView3D.draw(cb, doc->getMeshSolid());
            mRenderView3D.draw(cb, doc->getMeshWire());
        }
        mRenderView3D.drawEnd(cb);
        
        VkRect2D cubeView = 
        {
            .offset = {
                mView3DRect.offset.x + int(mView3DRect.extent.width) - cubeViewMargin - cubeViewSize,
                mView3DRect.offset.y + cubeViewMargin,
            },
            .extent ={ uint32_t(cubeViewSize), uint32_t(cubeViewSize) }
        };
        mRenderView3D.drawNavCube(cb, cubeView, frame.viewColorFinal, 
                frame.viewDepthFinal, frame.viewColorMultisampled, frame.id);

        auto lPanelRect = mLeftPanel.getRect().vkrect();

        mRenderShape2D.drawBegin(cb, lPanelRect, frame.viewColorFinal);
        mRenderShape2D.draw(cb, mLeftPanel.getBufferSolid());
        mRenderShape2D.draw(cb, mLeftPanel.getBufferWire());
        mRenderShape2D.drawEnd(cb);

        mRenderText2D.drawBegin(cb, lPanelRect, frame.viewColorFinal);
        mRenderText2D.draw(cb, mLeftPanel.getBufferText());
        mRenderText2D.drawEnd(cb);
    });
}

auto ox::AppWindow::submit() -> void
{
    auto res = mRenderWindow.submit(mApp->getRender().getGfxCompQueue().queue, 
                mApp->getRender().getPresentQueue().queue);
}

auto ox::AppWindow::isClosing() -> bool
{
    return glfwWindowShouldClose(mGLFWWindow);
}

#define TO_APP_WINDOW(window) reinterpret_cast<AppWindow*>(glfwGetWindowUserPointer(window))

auto ox::AppWindow::fnTextInputExterior(GLFWwindow* window, unsigned int codepoint) -> void
{
    TO_APP_WINDOW(window)->fnTextInputInterior(codepoint);
}
auto ox::AppWindow::fnKeyPressExterior(GLFWwindow* window, int key, int scancode, int action, int mods) -> void
{
    TO_APP_WINDOW(window)->fnKeyPressInterior(key, scancode, action, mods);
}
auto ox::AppWindow::fnMousePressExterior(GLFWwindow* window, int button, int action, int mods) -> void
{
    TO_APP_WINDOW(window)->fnMousePressInterior(button, action, mods);
}
auto ox::AppWindow::fnMouseMoveExterior(GLFWwindow* window, double x, double y) -> void
{
    TO_APP_WINDOW(window)->fnMouseMoveInterior(x, y);
}
auto ox::AppWindow::fnScrollExterior(GLFWwindow* window, double xOffset, double yOffset) -> void
{
    TO_APP_WINDOW(window)->fnScrollInterior(xOffset, yOffset);
}
auto ox::AppWindow::fnResizeExterior(GLFWwindow* window, int width,int height) -> void
{
    TO_APP_WINDOW(window)->fnResizeInterior(width, height);
}

auto ox::AppWindow::fnTextInputInterior(unsigned int codepoint) -> void
{
    if (mInputState == AppInputState::UI_INPUT && mCurrentFocus.first && mCurrentFocus.second)
    {
        mCurrentFocus.first->inputCharacter(codepoint);
        mCurrentFocus.second->scheduleUpdate();
    }
}

auto ox::AppWindow::fnKeyPressInterior(int key, int scancode, int action, int mods) -> void
{
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
    {
        glfwSetWindowShouldClose(mGLFWWindow, true);
    }

    if (key == GLFW_KEY_P && action == GLFW_RELEASE)
    {
        mCamera.setPerspective(!mCamera.mPerspective);
    }

    if (key == GLFW_KEY_LEFT && action == GLFW_PRESS)
    {
        if (mInputState == AppInputState::UI_INPUT && mCurrentFocus.first && mCurrentFocus.second)
        {
            mCurrentFocus.first->moveCursorBack();
            mCurrentFocus.second->scheduleUpdate();
        }
    }

    if (key == GLFW_KEY_RIGHT && action == GLFW_PRESS)
    {
        if (mInputState == AppInputState::UI_INPUT && mCurrentFocus.first && mCurrentFocus.second)
        {
            mCurrentFocus.first->moveCursorForward();
            mCurrentFocus.second->scheduleUpdate();
        }
    }

    if (key == GLFW_KEY_BACKSPACE && action == GLFW_PRESS)
    {
        if (mInputState == AppInputState::UI_INPUT && mCurrentFocus.first && mCurrentFocus.second)
        {
            mCurrentFocus.first->removeBack();
            mCurrentFocus.second->scheduleUpdate();
        }
    }
}
auto ox::AppWindow::fnMousePressInterior(int button, int action, int mods) -> void
{
    double x, y;
    glfwGetCursorPos(mGLFWWindow, &x, &y);
    
    const auto prev = mInputState;

    bool uihover = mLeftPanel.getRect().inside({float(x), float(y)});
    
    if (uihover)
    {
        if (mInputState != AppInputState::UI_INPUT)
            mInputState = AppInputState::NONE;

        if (action == GLFW_PRESS && button == GLFW_MOUSE_BUTTON_LEFT)
        {
            auto clickres = mLeftPanel.onClick(oxm::vf2{float(x), float(y)});
            auto inputElem = dynamic_cast<ox::ui::InputElement*>(clickres.elemFocused);
            if (inputElem)
            {
                if (mCurrentFocus.first && inputElem != mCurrentFocus.first)
                {
                    mCurrentFocus.first->setFocused(false);
                    mCurrentFocus.second->scheduleUpdate();
                    mCurrentFocus = {nullptr, nullptr};
                }
                mCurrentFocus.first = inputElem;
                mCurrentFocus.second = &mLeftPanel;
                mInputState = AppInputState::UI_INPUT;
            }
            else
            {
                mInputState = AppInputState::NONE;
            }
        }

    }
    else 
    {
        if (action == GLFW_PRESS && button == GLFW_MOUSE_BUTTON_LEFT)
        {
            mInputState = AppInputState::CAM_PAN;
        }
        else if (action == GLFW_PRESS && button == GLFW_MOUSE_BUTTON_RIGHT)
        {
            mInputState = AppInputState::CAM_ROT;
        }
        else if (action == GLFW_RELEASE)
        {
            mInputState = AppInputState::NONE;
        }
    }

    if (prev != mInputState)
    {
        if (prev == AppInputState::CAM_PAN)
        {
            mCamera.resetPan();
        }
        else if (prev == AppInputState::CAM_ROT)
        {
            mCamera.resetArcball();
        }
        else if (prev == AppInputState::UI_INPUT)
        {
            if (mCurrentFocus.first)
            {
                mCurrentFocus.first->setFocused(false);
                mCurrentFocus.second->scheduleUpdate();
                mCurrentFocus = {nullptr, nullptr};
            }
        }
    }
}
auto ox::AppWindow::fnMouseMoveInterior(double x, double y) -> void
{
    const auto prev = mInputState;
    const auto prevUIHover = mUIHoverState;

    if (mLeftPanel.getRect().inside({float(x), float(y)}))
    {
        mUIHoverState = AppUIHoverState::UI_HOVER;

        if (mInputState != AppInputState::UI_INPUT)
            mInputState = AppInputState::NONE;
    }
    else
    {
        mUIHoverState = AppUIHoverState::NONE;
    }

    if (prevUIHover != mUIHoverState && prevUIHover == AppUIHoverState::UI_HOVER)
    {
        mLeftPanel.onHoverEnd();
    }
    if (prev != mInputState)
    {
        if (prev == AppInputState::CAM_PAN)
        {
            mCamera.resetPan();
        }
        else if (prev == AppInputState::CAM_ROT)
        {
            mCamera.resetArcball();
        }
        else if (prev == AppInputState::UI_INPUT)
        {
            if (mCurrentFocus.first)
            {
                mCurrentFocus.first->setFocused(false);
                mCurrentFocus.second->scheduleUpdate();
                mCurrentFocus = {nullptr, nullptr};
            }
        }
    }

    const oxm::vf2 pos = {float(x), float(y)};
    if (mUIHoverState == AppUIHoverState::UI_HOVER)
    {
        mLeftPanel.onHover(pos);
    }

    switch (mInputState)
    {
        case AppInputState::NONE: 
        {
            break;
        }
        case AppInputState::CAM_PAN:
        {
            const oxm::vf2 offset = {float(mView3DRect.offset.x), float(mView3DRect.offset.y)};
            mCamera.pan(pos + offset);
            break;
        }
        case AppInputState::CAM_ROT:
        {
            const oxm::vf2 offset = {float(mView3DRect.offset.x), float(mView3DRect.offset.y)};
            mCamera.arcball(pos + offset);
            break;
        }
        case AppInputState::UI_INPUT:
        {
            break;
        }
    }
}
auto ox::AppWindow::fnScrollInterior(double xOffset, double yOffset) -> void
{
    double x, y;
    glfwGetCursorPos(mGLFWWindow, &x, &y);

    if (mLeftPanel.getRect().inside({float(x), float(y)}))
    {
        mLeftPanel.onScroll(yOffset);
    }
    else
    {
        mCamera.zoom(yOffset * 0.1f);
    }
}
auto ox::AppWindow::fnResizeInterior(int width,int height) -> void
{
}

ox::App::App(bool& res) :
    mRender{{ VkSampleCountFlagBits::VK_SAMPLE_COUNT_4_BIT }, res}
{
    if(!res)
    {
        printf("[ERROR] Can't create render\n");
        return;
    }

    // TODO: hardcoded windows
    mWindows.emplace_back(std::make_unique<AppWindow>(this, 800, 600, res));
    // Here just in case
    if(!res)
    {
        printf("[ERROR] Can't create main window\n");
        return;
    }
}


ox::App::~App() noexcept
{
    mWindows.clear();
    mDocuments.clear();
    glfwTerminate();
}

auto ox::App::run() -> void
{
    while(true)
    {
        auto it = std::begin(mWindows);
        if (it == mWindows.end())
        {
            printf("[INFO] No windows to run\n");
            break;
        }

        // Check first MAIN window first
        // if it is closing then all app closing
        if((*it)->isClosing())
        {
            printf("[INFO] Main window closed\n");
            mRender.wait();
            mWindows.clear();
            break;
        }

        for(;it != mWindows.end();++it)
        {
            if ((*it)->isClosing())
            {
                mRender.wait();
                it = mWindows.erase(it);
            }
        }

        for (auto& w : mWindows)
            w->prepare();

        for (auto& w : mWindows)
            w->submit();

        glfwWaitEvents();
    }
}

