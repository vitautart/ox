#include <render/res/resbuffers.hpp>
#include <memory.h>
#include <vulkan/vulkan_core.h>

auto ox::CPUBuffer::create(VmaAllocator iAllocator, const VkBufferCreateInfo& iCreateInfo) noexcept -> VkResult
{
    VmaAllocationInfo allocInfo;
    VmaAllocationCreateInfo allocCreateInfo = 
    {
        .flags = VmaAllocationCreateFlagBits::VMA_ALLOCATION_CREATE_MAPPED_BIT,
        .usage = VmaMemoryUsage::VMA_MEMORY_USAGE_CPU_ONLY
    };
    auto res = vmaCreateBuffer(iAllocator, &iCreateInfo, &allocCreateInfo, &mBuffer, &mMemory, &allocInfo);
    if (res != VK_SUCCESS)
        *this = {};
    else
    {
        mPtr = allocInfo.pMappedData;
        mSize = iCreateInfo.size;
    }
    return res;
}

auto ox::CPUBuffer::destroy(VmaAllocator iAllocator) noexcept -> void
{
    if (mBuffer && mMemory)
        vmaDestroyBuffer(iAllocator, mBuffer, mMemory);
    *this = {}; 
}

auto ox::CPUBuffer::write(const void* iSrc, VkDeviceSize iSrcSize, VkDeviceSize iDstOffset) noexcept -> void
{
    memcpy((char*)mPtr + iDstOffset, iSrc, iSrcSize);
}

auto ox::GPUPreferMappedBuffer::create(VmaAllocator iAllocator, const VkBufferCreateInfo& iCreateInfo) noexcept -> VkResult
{
    VmaAllocationInfo allocInfo;
    VmaAllocationCreateInfo allocCreateInfo = 
    {
        .flags = VmaAllocationCreateFlagBits::VMA_ALLOCATION_CREATE_MAPPED_BIT,
        .usage = VmaMemoryUsage::VMA_MEMORY_USAGE_CPU_TO_GPU
    };
    auto res = vmaCreateBuffer(iAllocator, &iCreateInfo, &allocCreateInfo, &mBuffer, &mMemory, &allocInfo);
    if (res != VK_SUCCESS)
        *this = {};
    else
    {
        mPtr = allocInfo.pMappedData;
        mSize = iCreateInfo.size;
    }
    return res;
}

auto ox::GPUPreferMappedBuffer::destroy(VmaAllocator iAllocator) noexcept -> void
{
    if (mBuffer && mMemory)
        vmaDestroyBuffer(iAllocator, mBuffer, mMemory);
    *this = {}; 
}

auto ox::GPUPreferMappedBuffer::write(const void* iSrc, VkDeviceSize iSrcSize, VkDeviceSize iDstOffset) noexcept -> void
{
    memcpy((char*)mPtr + iDstOffset, iSrc, iSrcSize);
}

auto ox::GPUBuffer::create(VmaAllocator iAllocator, const VkBufferCreateInfo& iCreateInfo) noexcept -> VkResult
{
    VmaAllocationInfo allocInfo;
    VmaAllocationCreateInfo allocCreateInfo = { .usage = VmaMemoryUsage::VMA_MEMORY_USAGE_GPU_ONLY };
    auto res = vmaCreateBuffer(iAllocator, &iCreateInfo, &allocCreateInfo, &mBuffer, &mMemory, &allocInfo);
    if (res != VK_SUCCESS)
        *this = {};
    else
        mSize = iCreateInfo.size;
    return res;
}

auto ox::GPUBuffer::destroy(VmaAllocator iAllocator) noexcept -> void
{
    if (mBuffer && mMemory)
        vmaDestroyBuffer(iAllocator, mBuffer, mMemory);
    *this = {}; 
}

auto ox::CPUtoGPUBuffer::create(VmaAllocator iAllocator, VkBufferUsageFlags iFlags, VkDeviceSize iSize) noexcept -> VkResult
{

    VkResult result;
    VkBufferCreateInfo createInfo = 
    {
        .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO, .pNext = nullptr, .flags = 0,
        .size = calculateSize(iSize),
        .usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT | iFlags,
        .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
        .queueFamilyIndexCount = 0,
        .pQueueFamilyIndices = 0
    };

    auto res = mCPU.create(iAllocator, createInfo);
    createInfo.usage = VK_BUFFER_USAGE_TRANSFER_DST_BIT | static_cast<VkBufferUsageFlags>(iFlags);
    if (res == VK_SUCCESS)
        res = mGPU.create(iAllocator, createInfo);
    return res;
}

auto ox::CPUtoGPUBuffer::recreate(VmaAllocator iAllocator, VkBufferUsageFlags iFlags, VkDeviceSize iSize) noexcept -> VkResult
{
    if (iSize <= size())
        return VK_SUCCESS;

    destroy(iAllocator);
    return create(iAllocator, iFlags, calculateSize(iSize));
}


auto ox::CPUtoGPUBuffer::destroy(VmaAllocator iAllocator) noexcept -> void
{
    mCPU.destroy(iAllocator);
    mGPU.destroy(iAllocator);
}
