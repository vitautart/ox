#pragma once
#include <render/rendercommon.hpp>
#include <oxmath.hpp>

#include <render/renderwindow.hpp>
#include <render/res/resbuffers.hpp>
#include <render/rendervertex.hpp>

#include <span>
#include <vulkan/vulkan_core.h>

namespace ox
{
    struct RenderViewCreateInfo
    {
        const Render* render;
        uint32_t frameCount;
    };

    class BufferMesh3D
    {
    public:
        explicit BufferMesh3D() = delete;
        CTORS_COPY_DELETE(BufferMesh3D);
        CTORS_MOVE_AUTO_6(BufferMesh3D, mVertexBuffers, mIndexBuffers, mVerticesCounts, mIndicesCounts, mCurrentBucket, mNextBucket);
        explicit BufferMesh3D(const ox::Render* iRender) : mRender(iRender) {};
        virtual ~BufferMesh3D() noexcept;
        auto load(VkCommandBuffer iCommandBuffer, 
                std::span<RenderVertex3D> iVertices, std::span<uint32_t> iIndices) -> bool;
    public: // getters
        auto indexbuffer() const noexcept -> const VkBuffer&;
        auto vertexbuffer() const noexcept -> const VkBuffer&;
        auto indexsize() const noexcept -> oxm::u32;
        auto vertexsize() const noexcept -> oxm::u32;
    private:
        auto destroy() noexcept -> void;
    private:
        std::array<CPUtoGPUBuffer, 2> mVertexBuffers = {};
        std::array<CPUtoGPUBuffer, 2> mIndexBuffers = {};
        std::array<oxm::u32, 2> mVerticesCounts = {};
        std::array<oxm::u32, 2> mIndicesCounts = {};
        oxm::u8 mCurrentBucket = 0;
        oxm::u8 mNextBucket = 0;
    private: // non-owned objects
        const Render* mRender = nullptr;
    };

    class BufferMesh3DSolid final : public BufferMesh3D
    {
    public:
        explicit BufferMesh3DSolid(const ox::Render* iRender) : BufferMesh3D(iRender) {};
    };

    class BufferMesh3DWire final : public BufferMesh3D
    {
    public:
        explicit BufferMesh3DWire(const ox::Render* iRender) : BufferMesh3D(iRender) {};
    };

    class RenderView3D
    {
    public:
        auto create(const RenderViewCreateInfo& iCreateInfo) -> bool;
        auto updateViewProj(const oxm::mf4& iView, const oxm::mf4& iProj, uint32_t iFrameid) -> void;
        auto updateViewProjNavCube(const oxm::mf4& iView, const oxm::mf4& iProj, uint32_t iFrameid) -> void;
        auto updateModelTform(const oxm::mf4& iModelTform, uint32_t iFrameid) -> void;

        auto drawBegin(VkCommandBuffer iCommandBuffer, const VkRect2D& iRect,
                VkImageView iColorView,
                VkImageView iDepthView,
                ox::maybe<VkImageView> iColorViewMS,
                size_t iFrameId
                ) -> void;
        auto draw(VkCommandBuffer iCommandBuffer, const BufferMesh3DSolid& iMesh) -> void;
        auto draw(VkCommandBuffer iCommandBuffer, const BufferMesh3DWire& iMesh) -> void;
        auto drawEnd(VkCommandBuffer iCommandBuffer) -> void;

        // Should be called separately outside of the scope of drawBegin and drawEnd
        // This function already have all functionality of the scope of drawBegin and drawEnd
        auto drawNavCube(VkCommandBuffer iCommandBuffer, const VkRect2D& iRect,
                VkImageView iColorView,
                VkImageView iDepthView,
                ox::maybe<VkImageView> iColorViewMS,
                size_t iFrameId
                ) -> void;

        auto destroy() -> void;

    private:
        auto createLayouts() -> bool;
        auto createPipelineForMesh() -> bool;
        auto createPipelineForWireframe() -> bool;
        auto createDescriptorSets() -> bool;
        auto createUniformBuffers() -> bool;
    private:
        bool mInitialized = false;

        VkPipeline mMeshPipeline = VK_NULL_HANDLE;
        VkPipeline mWirePipeline = VK_NULL_HANDLE;

        VkDescriptorSetLayout mDescriptorSetLayout = VK_NULL_HANDLE;

        VkDescriptorPool mDescriptorPool = VK_NULL_HANDLE;
        std::vector<VkDescriptorSet> mDescriptorSets;

        VkPipelineLayout mPipelineLayout = VK_NULL_HANDLE;

        std::vector<GPUPreferMappedBuffer> mViewProjData;
        std::vector<GPUPreferMappedBuffer> mModelTformData;

        std::vector<VkDescriptorSet> mDescriptorSetsNavCube;
        std::vector<GPUPreferMappedBuffer> mViewProjNavCube;
        // TODO: kind of redundant
        GPUPreferMappedBuffer mModelTformNavCube;

    private: // not owning objects
        RenderViewCreateInfo mCreateInfo;
    };
}
