#pragma once

#include "oxmath.hpp"
#include <graphentities.hpp>
#include <containers.hpp>

namespace ox
{
/*struct HalfedgeDCEL
{
    using TableIdx = HalfedgeIdx;
    VertexIdx start;
    HalfedgeIdx twin;
    HalfedgeIdx prev;
    HalfedgeIdx next;
};

class DCELAddOnly
{
public:
    auto get(VertexIdx idx) const noexcept -> const Vertex2D& { return mVertices[idx.idx]; }
    auto get(VertexIdx idx) noexcept -> Vertex2D& { return mVertices[idx.idx]; } 
    auto get(HalfedgeIdx idx) const noexcept -> const HalfedgeDCEL& { return mHalfedges[idx.idx]; }
    auto get(HalfedgeIdx idx) noexcept -> HalfedgeDCEL& { return mHalfedges[idx.idx]; }
    auto getVertices() const noexcept -> const std::vector<Vertex2D>& { return mVertices; }
    auto getVertices() -> std::vector<Vertex2D>& { return mVertices; }
    auto getHalfedges() const noexcept -> const std::vector<HalfedgeDCEL>& { return mHalfedges; }
    auto getHalfedges() -> std::vector<HalfedgeDCEL>& { return mHalfedges; }
    template<typename T> auto add() -> typename T::TableIdx { return {};}

    auto add(const Vertex2D& data) -> VertexIdx 
    {
        mVertices.push_back(data); 
        return { uint32_t(mVertices.size() - 1) };
    }
    auto add(const HalfedgeDCEL& data) -> HalfedgeIdx 
    { 
        mHalfedges.push_back(data); 
        return { uint32_t(mHalfedges.size() - 1) };
    }
private:
    std::vector<Vertex2D> mVertices;
    std::vector<HalfedgeDCEL> mHalfedges;
};
template<> auto DCELAddOnly::add<Vertex2D>() -> typename Vertex2D::TableIdx 
{
    mVertices.emplace_back(); 
    return { uint32_t(mVertices.size() - 1) };
}
template<> auto DCELAddOnly::add<HalfedgeDCEL>() -> typename HalfedgeDCEL::TableIdx 
{ 
    mHalfedges.emplace_back(); 
    return { uint32_t(mHalfedges.size() - 1) };
}*/
}
