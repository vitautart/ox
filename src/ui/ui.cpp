#include <string>
#include <ui/ui.hpp>
#include <GLFW/glfw3.h>
#include <vulkan/vulkan_core.h>
#include <algorithm>

auto ox::ui::Button::fillRenderData(ox::BufferShape2DSolid& sb,
            ox::BufferShape2DWire& wb,
            ox::BufferText2D& tb) -> void
{
    sb.add(mRect.pos, mRect.size, !mHovered ? mStyle->bgColorDefault : mStyle->bgColorHighlight);
    //wb.add(mRect.pos, mRect.size, mHovered ? mStyle->borderColorDefault : mStyle->borderColorHighlight);
    wb.add(mRect.pos, mRect.size, mStyle->borderColorDefault);
    auto tSize = tb.font()->measureQuick(mText);
    auto center = mRect.pos + mRect.size * 0.5f;
    oxm::vf2 tPos = 
    { 
        std::floor(center[0] - tSize[0] * 0.5f), 
        std::floor(center[1] + tSize[1] * 0.5f)
    };
    tb.add(mText, tPos, mStyle->textColorDefault);
}

auto ox::ui::InputText::fillRenderData(ox::BufferShape2DSolid& sb,
            ox::BufferShape2DWire& wb,
            ox::BufferText2D& tb) -> void
{
    mCachedFont = tb.font();
    sb.add(mRect.pos, mRect.size, mStyle->bgColorInputField);
    wb.add(mRect.pos, mRect.size, (!mHovered && !mFocused) ? mStyle->borderColorDefault : mStyle->borderColorHighlight);
    
    //////////////////
    /// Text and cursor rendering rendering
    /// /////////////

    float xsizeText = mCachedFont->measureQuick(mText)[0];
    float xsizeItem = mRect.size[0];
    auto charCount = int((xsizeItem - 2 * calculateTextMarginX()) / (float)mCachedFont->mAdvance);

    int cursorEnd = std::min((int)mText.size(), mCursorStart + charCount);
    if (mCursorText > cursorEnd)
    {
        cursorEnd = mCursorText;
        mCursorStart =std::max(0, cursorEnd - charCount);
    }
    else if (mCursorText < mCursorStart)
    {
        mCursorStart = mCursorText;
        cursorEnd = std::min((int)mText.size(), mCursorStart + charCount);
    }
    else if (mCursorStart > 0 && cursorEnd - mCursorStart < charCount)
    {
        mCursorStart -= charCount - (cursorEnd - mCursorStart);
        cursorEnd = std::min((int)mText.size(), mCursorStart + charCount);
    }


    //////////////////////////////////////////////////////////////
    const std::u32string_view textview =
    std::u32string_view { 
        mText.begin() + mCursorStart, 
        mText.begin() + cursorEnd
    }; 

    auto textsize = tb.font()->measureQuick(textview);
    auto center = mRect.pos + mRect.size * 0.5f;
    oxm::vf2 tPos = 
    { 
        std::floor(calculateTextStartX()), 
        std::floor(center[1] + textsize[1] * 0.5f)
    };
    tb.add(textview, tPos, mStyle->textColorDefault);

    if (mFocused)
    {
        int internalCursor = mCursorText - mCursorStart;
        Rect cursorRect = 
        {
            .pos = 
            { 
                (tPos[0] - 1.0f) + internalCursor * (float)tb.font()->mAdvance, 
                //tPos[1] - (tSize[1] + 2.0f) 
                mRect.pos[1] + 4 
            },
            //.size = {2.0f, tSize[1] + 2.0f}
            .size = {1.0f, mRect.size[1] - 8.0f }
        };
        sb.add(cursorRect.pos, cursorRect.size, mStyle->textColorDefault);
    }
}

auto ox::ui::InputText::onClick(const oxm::vf2& pos) -> ClickRes
{
    if(!mCachedFont) return {false, nullptr};
    mFocused = true;
    const float adv = (float)mCachedFont->mAdvance;
    const int unbounded = (int)std::round((pos[0] - calculateTextStartX()) / adv);
    mCursorText = mCursorStart + std::clamp(unbounded, 0, (int)mText.size());
    return 
    {
        .shouldUpdate = true,
        .elemFocused = this
    };
}

auto ox::ui::InputText::removeBack() -> void
{
    if (mCursorText > 0)
    {
        mText.erase(mCursorText - 1, 1);
        moveCursorBack();
    }
}

auto ox::ui::InputText::inputCharacter(char32_t c) -> void 
{
    mText.insert(mText.begin() + mCursorText, c);
    moveCursorForward();
}

auto ox::ui::InputText::calculateTextMarginX() const noexcept -> float
{
    return (float)mCachedFont->mAdvance + 2;
}

auto ox::ui::InputText::calculateTextStartX() const noexcept -> float
{
    return mRect.pos[0] + calculateTextMarginX();
}

// TODO: this is only for ASCII not utf-8
static auto charsToU32String(const char* str) -> std::u32string
{
    std::u32string str32;
    size_t i = 0;
    while(str[i] != '\0')
    {
        str32 += str[i]; 
        i++;
    }
    return str32;
}

// TODO: this is only for ASCII not utf-8
static auto u32StringToString(const std::u32string_view str32) -> std::string
{
    std::string str;
    for (auto c : str32)
        str += (char)c;

    return str;
}

ox::ui::SmallInput::SmallInput(const Style* iStyle): 
    Element(iStyle, true),
    mHovered(false), 
    mCopyButton{U"C", iStyle},
    mPasteButton{U"P", iStyle},
    mInputText{iStyle}
{
    mCopyButton.setClickHandler([&input = mInputText]
    {
        // TODO : glfw... function return utf-8 encoded string
        // but charsTo... is using only ASCII
        std::string str = u32StringToString(input.getText());
        if (!str.empty()) glfwSetClipboardString(nullptr, str.c_str());
    });
    mPasteButton.setClickHandler([&input = mInputText]
    {
        // TODO : glfw... function return utf-8 encoded string
        // but charsTo... is using only ASCII
        const char* str = glfwGetClipboardString(nullptr);
        if (str) input.setText(charsToU32String(str));
    });
}

auto ox::ui::SmallInput::fillRenderData(ox::BufferShape2DSolid& sb,
            ox::BufferShape2DWire& wb,
            ox::BufferText2D& tb) -> void
{
    mInputText.fillRenderData(sb, wb, tb);
    mCopyButton.fillRenderData(sb, wb, tb);
    mPasteButton.fillRenderData(sb, wb, tb);
}


auto ox::ui::SmallInput::setRect(const Rect& r) -> void
{
    mRect = r;
    float a = mRect.size[1];
    float gap = 4.0f;
    // Orders matters
    ui::Rect itRect = 
    {
        .pos = mRect.pos,
        .size = { mRect.size[0] - 2.0f * (a + gap), a }
    };
    ui::Rect cbRect = 
    {
        .pos = { itRect.pos[0] + itRect.size[0] + gap, mRect.pos[1] },
        .size = { a, a }
    };
    ui::Rect pbRect = 
    {
        .pos = { cbRect.pos[0] + cbRect.size[0] + gap, mRect.pos[1] },
        .size = { a, a }
    };
    mInputText.setRect(itRect);
    mCopyButton.setRect(cbRect);
    mPasteButton.setRect(pbRect);
}

auto ox::ui::SmallInput::onHover(const oxm::vf2& pos) -> bool
{
    mHovered = true;

    mInputText.onHoverEnd();
    mCopyButton.onHoverEnd();
    mPasteButton.onHoverEnd();

    if (mInputText.getRect().inside(pos))
        return mInputText.onHover(pos);
    else if (mCopyButton.getRect().inside(pos))
        return mCopyButton.onHover(pos);
    else if (mPasteButton.getRect().inside(pos))
        return mPasteButton.onHover(pos);
    return false;
}

auto ox::ui::SmallInput::onHoverEnd() -> void
{
    mHovered = false;
    mInputText.onHoverEnd();
    mCopyButton.onHoverEnd();
    mPasteButton.onHoverEnd();
}

auto ox::ui::SmallInput::onClick(const oxm::vf2& pos) -> ClickRes
{
    if (mInputText.getRect().inside(pos))
        return mInputText.onClick(pos);
    else if (mCopyButton.getRect().inside(pos))
        return mCopyButton.onClick(pos);
    else if (mPasteButton.getRect().inside(pos))
        return mPasteButton.onClick(pos);
    return 
    {
        .shouldUpdate = false,
        .elemFocused = nullptr,
    };
}

auto ox::ui::Label::fillRenderData(ox::BufferShape2DSolid&, ox::BufferShape2DWire&,
            ox::BufferText2D& tb) -> void
{
    auto tSize = tb.font()->measureQuick(mText);
    auto center = mRect.pos + mRect.size * 0.5f;
    oxm::vf2 tPos = 
    { 
        std::floor(center[0] - tSize[0] * 0.5f), 
        std::floor(center[1] + tSize[1] * 0.5f)
    };
    tb.add(mText, tPos, mStyle->textColorDefault);
}

ox::ui::LinearCanvas::LinearCanvas(const Rect& iRect, const ox::Render* iRender, const Style* iStyle)
    : mRect(iRect), mStyle(iStyle), mShouldUpdate(true), mCurrentHovered(nullptr),
    mShapeBufferSolid(iRender), mShapeBufferWire(iRender), mTextBuffer(iRender)
{
}



ox::ui::LinearCanvas::~LinearCanvas() noexcept 
{
    mShapeBufferSolid.destroy();
    mShapeBufferWire.destroy();
    mTextBuffer.destroy();
}

auto ox::ui::LinearCanvas::add(std::unique_ptr<Element> element) -> size_t
{
    mElements.push_back(std::move(element));
    mShouldUpdate = true;
    return mElements.size() - 1;
}

auto ox::ui::LinearCanvas::add(std::span<std::unique_ptr<Element>> elements) -> size_t
{
    std::move(elements.begin(), elements.end(), std::back_inserter(mElements));
    mShouldUpdate = true;
    return mElements.size() - 1;
}

auto ox::ui::LinearCanvas::removeBy(Element* element) -> bool
{
    size_t size = mElements.size();
    bool removed = std::erase_if(mElements, 
            [element](const std::unique_ptr<Element>& e){ return e.get() == element; }) < size;
    if (removed) mShouldUpdate = true;
    return  removed;
}

auto ox::ui::LinearCanvas::removeBy(size_t id) -> bool
{
    if (id >= mElements.size())
        return false;

    mElements.erase(std::next(mElements.begin(), id));
    mShouldUpdate = true;
    return true;
}

auto ox::ui::LinearCanvas::resize(const oxm::vf2& size) -> void
{
    mRect.size = size;
    mShouldUpdate = true;
}



auto ox::ui::LinearCanvas::move(const oxm::vf2& windowPos) -> void
{
    mRect.pos = windowPos;
}

auto ox::ui::Rect::inside(const oxm::vf2& iPos) const noexcept -> bool
{
    auto d = iPos - pos;
    return (d[0] < size[0]) && (d[0] > 0) && (d[1] < size[1]) && (d[1] > 0);
}

auto ox::ui::Rect::vkrect() const noexcept -> VkRect2D
{
    return 
    {
        .offset = {(int32_t)pos[0], (int32_t)pos[1]},
        .extent = {(uint32_t)size[0], (uint32_t)size[1]}
    };
}

auto ox::ui::LinearCanvas::onClick(const oxm::vf2& windowPos) -> Element::ClickRes
{
    Element::ClickRes result = { false, nullptr };
    auto pos = windowPos - mRect.pos;
    for (auto& e : mElements)
        if (e->getRect().inside(pos))
        {
            result = e->onClick(pos);
            mShouldUpdate = result.shouldUpdate || mShouldUpdate;
            break;
        }
    return result;
}

auto ox::ui::LinearCanvas::onHover(const oxm::vf2& windowPos) -> void 
{
    updateGeometry();

    auto pos = windowPos - mRect.pos;

    Element* hovered = nullptr;
    for (auto& e : mElements)
        if (e->getRect().inside(pos))
        {
            hovered = e.get();
            break;
        }

    if (mCurrentHovered && mCurrentHovered != hovered)
    {
        mShouldUpdate = true;
        mCurrentHovered->onHoverEnd();
    }

    if (hovered)
    {
        mShouldUpdate = hovered->onHover(pos) || mShouldUpdate;
        mCurrentHovered = hovered;
    }
    else
        mCurrentHovered = nullptr;
}

auto ox::ui::LinearCanvas::onHoverEnd() -> void
{
    if (mCurrentHovered)
    {
        mShouldUpdate = true;
        mCurrentHovered->onHoverEnd();
        mCurrentHovered = nullptr;
    }
}

auto ox::ui::LinearCanvas::load(VkCommandBuffer iCommandBuffer) -> bool
{
    updateGeometry();

    bool r1 = mShapeBufferSolid.load(iCommandBuffer);
    bool r2 = mShapeBufferWire.load(iCommandBuffer);
    bool r3 = mTextBuffer.load(iCommandBuffer);
    return r1 || r2 || r3;
}

ox::ui::VStretchCanvas::VStretchCanvas(const Rect& iRect, float iMargin, 
        const ox::Render* iRender, const Style* iStyle)
    : LinearCanvas{iRect, iRender, iStyle}, mMargin(iMargin)
{
}

auto ox::ui::VStretchCanvas::updateGeometry() -> void
{
    if (!mShouldUpdate)
        return;
    mShouldUpdate = false;

    mShapeBufferSolid.clear();
    mShapeBufferWire.clear();
    mTextBuffer.clear();

    mShapeBufferSolid.add(oxm::vf2::zero(), mRect.size, mStyle->bgColorDefault);
    mShapeBufferWire.add(oxm::vf2{1, 1}, mRect.size - oxm::vf2{1, 1}, mStyle->borderColorDefault);

    size_t count = 0;
    for (auto& e : mElements)
        if (e->isVisible())
            count++;

    if (count == 0) return;

    oxm::vf2 esize =
    {
        mRect.size[0] - 2 * mMargin,
        (mRect.size[1] - mMargin) / mElements.size() - mMargin
    };

    float y = mMargin;
    for (auto& e : mElements)
    {
        if (!e->isVisible())
            continue;
        e->setRect({
            .pos = { mMargin, y },
            .size = esize
        });
        e->fillRenderData(mShapeBufferSolid, mShapeBufferWire, mTextBuffer);
        y += esize[1] + mMargin;
    }
}

ox::ui::VScrolCanvas::VScrolCanvas(const Rect& iRect, float iMargin, float iElemHeight, 
            const ox::Render* iRender, const Style* iStyle)
    : LinearCanvas{iRect, iRender, iStyle}, 
    mMargin(iMargin), mElemHeight{iElemHeight}, mTopOffset{0},
    mScrollButtons { Button{U"", iStyle}, Button{U"", iStyle} }
{
    mScrollButtons[0].setClickHandler([this] { scrollBy(+4); });
    mScrollButtons[1].setClickHandler([this] { scrollBy(-4); });
}

auto ox::ui::VScrolCanvas::updateGeometry() -> void
{
    if (!mShouldUpdate)
        return;
    mShouldUpdate = false;

    mShapeBufferSolid.clear();
    mShapeBufferWire.clear();
    mTextBuffer.clear();

    float scrollbarWidth = 20;
    float scrollButtonMargin = 3;
    float scrollButtonSize = scrollbarWidth - 2 * scrollButtonMargin;

    mShapeBufferSolid.add(oxm::vf2::zero(), mRect.size, mStyle->bgColorDefault);
    mShapeBufferWire.add(oxm::vf2{1, 1}, mRect.size - oxm::vf2{1, 1}, mStyle->borderColorDefault);

////////////////////////////////////////////////////////////////////////////////////////
    Rect scrollRect = 
    {
        oxm::vf2{mRect.size[0] - scrollbarWidth, 0},
        { scrollbarWidth, mRect.size[1] }
    };

    // TODO: add posibility to use lines instead of full box, and rewrite it to line
    mShapeBufferWire.add(scrollRect.pos, scrollRect.size, mStyle->borderColorDefault);
    mScrollButtons[0].setRect({scrollRect.pos + oxm::vf2{scrollButtonMargin, scrollButtonMargin}, {scrollButtonSize, scrollButtonSize}});
    mScrollButtons[1].setRect(
    {
        {
            scrollRect.pos[0] + scrollButtonMargin,
            scrollRect.pos[1] + scrollRect.size[1] - scrollButtonMargin - scrollButtonSize
        },
        {
            scrollButtonSize, scrollButtonSize
        }
    });
    mScrollButtons[0].fillRenderData(mShapeBufferSolid, mShapeBufferWire, mTextBuffer);
    mScrollButtons[1].fillRenderData(mShapeBufferSolid, mShapeBufferWire, mTextBuffer);
/////////////////////////////////////////////////////////////////////////////////////////


    size_t count = 0;
    for (auto& e : mElements)
        if (e->isVisible())
            count++;

    if (count == 0) return;

    oxm::vf2 esize =
    {
        mRect.size[0] - 2 * mMargin - scrollbarWidth,
        mElemHeight
    };

    float y = mMargin + mTopOffset;
    for (auto& e : mElements)
    {
        if (!e->isVisible())
            continue;
        e->setRect({
            .pos = { mMargin, y },
            .size = esize
        });
        e->fillRenderData(mShapeBufferSolid, mShapeBufferWire, mTextBuffer);
        y += esize[1] + mMargin;
    }
}

auto ox::ui::VScrolCanvas::onHover(const oxm::vf2& windowPos) -> void 
{
    updateGeometry();

    auto pos = windowPos - mRect.pos;

    Element* hovered = nullptr;
    for (auto& e : mScrollButtons)
    {
        if (e.getRect().inside(pos))
        {
            hovered = &e;
            break;
        }
    }

    if (!hovered)
    {
        LinearCanvas::onHover(windowPos);
    }
    else
    {
        // TODO: extract this to LinearContainer function
        if (mCurrentHovered && mCurrentHovered != hovered)
        {
            mShouldUpdate = true;
            mCurrentHovered->onHoverEnd();
        }

        if (hovered)
        {
            mShouldUpdate = hovered->onHover(pos) || mShouldUpdate;
            mCurrentHovered = hovered;
        }
        else
            mCurrentHovered = nullptr;
    }
}

auto ox::ui::VScrolCanvas::onClick(const oxm::vf2& windowPos) -> Element::ClickRes
{
    // TODO: extract this to some common func in LinearCanvas 
    Element::ClickRes result = { false, nullptr };
    auto pos = windowPos - mRect.pos;
    for (auto& e : mScrollButtons)
    {
        if (e.getRect().inside(pos))
        {
            result = e.onClick(pos);
            mShouldUpdate = result.shouldUpdate || mShouldUpdate;
            return result;
        }
    }

    return LinearCanvas::onClick(windowPos);
}


auto ox::ui::VScrolCanvas::onScroll(oxm::f32 scroll) -> void
{
    scrollBy(scroll * 4);
}


auto ox::ui::VScrolCanvas::scrollBy(int scroll) -> void
{
    if (scroll > 0)
    {
        if (mTopOffset < 0)
        {
            mTopOffset += scroll; 
            mShouldUpdate = true;
        }
    }
    else if (scroll < 0)
    {
        auto& lastRect = mElements.back()->getRect();
        float bottom = lastRect.pos[1] + lastRect.size[1];
        if (mRect.size[1] < bottom + mMargin)
        {
            mTopOffset += scroll; 
            mShouldUpdate = true;
        }
    }
}
