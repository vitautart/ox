#pragma once

#include <oxmath.hpp>

namespace ox
{
    struct RenderVertex3D
    {
        oxm::vf3 pos;
        oxm::vf3 normal;
        oxm::vf3 col;
    };

    struct RenderVertexUI
    {
        oxm::vf2 pos;
        oxm::vf2 uv;
        oxm::u32 col;
    };

    struct CharVertex
    {
        oxm::vf2 pos;
        oxm::vf2 uv; // unnormalized (0.0 .. size)
        oxm::u32 col;
        oxm::u32 image;
    };
}
