#pragma once

//#include <stdio.h>
#include <vector>
#include <array>
#include <string>
#include <concepts>
#include <type_traits>
#include <unordered_map>

#include <vulkan/vulkan.h>

#include <vk_mem_alloc.h> // AMD VULKAN ALLOCATION LIB
#include <vulkan/vulkan_core.h>

#include <oxmath.hpp>
#include <sumtypes.hpp>
#include <render/res/resbuffers.hpp>

#include <definitions.hpp>

#define CHECK_AND_RETURN(result, msg) if (result != VK_SUCCESS) { printf(msg, result); return false; } 
#define CHECK_VK(result, msg) if (result != VK_SUCCESS) { printf(msg, result); } 

#define CALL_CHECK_DESTROY_RETURN(func_call) {auto result = func_call;\
    if(!result) {destroy(); return result;}}
#define RGBA_COMPONENT_MAPPING_STANDART {\
    .r = VK_COMPONENT_SWIZZLE_IDENTITY,\
    .g = VK_COMPONENT_SWIZZLE_IDENTITY,\
    .b = VK_COMPONENT_SWIZZLE_IDENTITY,\
    .a = VK_COMPONENT_SWIZZLE_IDENTITY} 

namespace ox
{
    static constexpr uint32_t MAX_CONCURRENT_FRAMES = 2;
    
    struct RenderQueue
    {
        uint32_t family;
        uint32_t id;
        bool initialized;
        VkQueue queue;
    };

    struct RenderFrame
    {
        uint32_t id;
        uint32_t image;
        VkImageView viewColorFinal;
        VkImageView viewDepthFinal;
        ox::maybe<VkImageView> viewColorMultisampled;
        bool valid;
    };

    enum class ImageSource
    {
        ALLOCATED,
        SWAPCHAIN,
    };

    enum class ShaderResource
    {
        DIFFUSE3D_VERT,
        DIFFUSE3D_FRAG,
        SIMPLE3D_VERT,
        SIMPLE3D_FRAG,
        UIBLOCK_VERT,
        UIBLOCK_FRAG,
        WIREFRAME3D_VERT,
        TEXT2D_VERT,
        TEXT2D_FRAG,
        SIZE
    };

    enum class FontResource
    {
        MAIN_FONT,
        SIZE
    };

    struct Glyph
    {
        oxm::i16 x0, y0, x1, y1;
        oxm::i16 u0, v0, u1, v1;
        //oxm::u32 image;
    };

    struct Image
    {
        VkImage image = VK_NULL_HANDLE;
        VkImageView view = VK_NULL_HANDLE;
        VkFormat format = VkFormat::VK_FORMAT_B8G8R8A8_SRGB;
        ImageSource source = ImageSource::ALLOCATED;
        VmaAllocation memory = VK_NULL_HANDLE;

        auto create(const VkImageCreateInfo& iImageInfo, 
            const VkImageViewCreateInfo& iViewInfo, VmaAllocator iAllocator, VkDevice iDevice) -> VkResult;
        auto create(VkImage iVkImage, ImageSource iSource, 
            const VkImageViewCreateInfo& iViewInfo, VmaAllocator iAllocator, VkDevice iDevice) -> VkResult;
        auto destroy(VmaAllocator iAllocator, VkDevice iDevice) -> void;
    };

    struct RenderTarget
    {
        auto hasColor() const noexcept -> bool { return !colorImages.empty(); }
        auto hasDepth() const noexcept -> bool { return !depthImages.empty(); }
        auto hasResolve() const noexcept -> bool { return !resolveImages.empty(); }
        auto size() const noexcept -> size_t { return colorImages.size(); }
        auto getFinalImage(size_t idx) const noexcept -> const Image& 
        { 
            return hasResolve() ? resolveImages[idx] : colorImages[idx];
        }
        std::vector<Image> colorImages;
        std::vector<Image> depthImages;
        std::vector<Image> resolveImages;
        VkExtent2D extent = {};
        VkSampleCountFlagBits samples = VkSampleCountFlagBits::VK_SAMPLE_COUNT_1_BIT;
    };   

    class BitmapFont
    {
    public:
        auto create(VkDevice iDevice, VmaAllocator iAllocator, VkCommandBuffer iCommandBuffer,
                const char* iFilepath, int iLineHeight, int iBitmapWidth, int iBitmapHeight
                ) -> ox::maybe<CPUBuffer>;

        auto measureQuick(const std::u32string_view iText) const noexcept -> oxm::vf2;
        auto destroy(VkDevice iDevice, VmaAllocator iAllocator) -> void;

        uint32_t mHeight = 0;
        uint32_t mAdvance = 0;
        // Small height for chars likes 'u', 'a', 'c', and not like 't', 'y', etc
        uint32_t mSmallCharHeight = 0;
        Image mAtlas = {};
        std::unordered_map<char32_t, Glyph> mGlyphMap = {};
    private:
        bool mInitialized = false;
    };

    auto checkPresentSupport(VkPhysicalDevice iDevice) -> bool;
    auto checkPresentSupport(VkPhysicalDevice iDevice, int iFamilyQueueId) -> bool;

    auto getDeviceScore(VkPhysicalDevice iDevice) noexcept -> float;

    template<typename FUNC, typename OUT, typename ... IN>
    auto vkenum(FUNC pFunc, std::vector<OUT>& pOut, IN... pIn) noexcept -> VkResult
        requires (std::is_invocable_r_v<VkResult, decltype(pFunc), IN..., uint32_t*, OUT*>)
    {
        uint32_t count = 0;
        auto res = pFunc(pIn..., &count, nullptr);
        if (res != VK_SUCCESS) 
            return res;
        
        pOut = std::vector<OUT>(count);
        return pFunc(pIn..., &count, pOut.data());
    }

    template<typename FUNC, typename OUT,  typename ...IN>
    auto vkenum(FUNC pFunc, std::vector<OUT>& pOut, IN... pIn) noexcept -> void
    {
        uint32_t count = 0;
        pFunc(pIn..., &count, nullptr);
        pOut = std::vector<OUT>(count);
        return pFunc(pIn..., &count, pOut.data());
    }

    template<typename T> requires std::same_as<T, uint32_t>
    constexpr auto formatof() noexcept -> VkFormat
    {
        return VK_FORMAT_R32_UINT;
    }

    template<typename T> requires std::same_as<T, float>
    constexpr auto formatof() noexcept -> VkFormat
    {
        return VK_FORMAT_R32_SFLOAT;
    }

    template<typename T> requires std::same_as<typename T::field, float>
    constexpr auto formatof() noexcept -> VkFormat
    {
        static_assert(T::rank() >= 2 || T::rank() <= 4, "Wrong count of elements in type.");
        if constexpr (T::rank() == 2) return VK_FORMAT_R32G32_SFLOAT;
        else if constexpr (T::rank() == 3) return VK_FORMAT_R32G32B32_SFLOAT;
        else if constexpr (T::rank() == 4) return VK_FORMAT_R32G32B32A32_SFLOAT;
    }

    template<typename T> requires std::same_as<typename T::field, int32_t>
    constexpr auto formatof() noexcept -> VkFormat
    {
        static_assert(T::rank() >= 2 || T::rank() <= 4, "Wrong count of elements in type.");
        if constexpr (T::rank() == 2) return VK_FORMAT_R32G32_SINT;
        else if constexpr (T::rank() == 3) return VK_FORMAT_R32G32B32_SINT;
        else if constexpr (T::rank() == 4) return VK_FORMAT_R32G32B32A32_SINT;
    }

    template<typename T> requires std::same_as<typename T::field, uint32_t>
    constexpr auto formatof() noexcept -> VkFormat
    {
        static_assert(T::rank() >= 2 || T::rank() <= 4, "Wrong count of elements in type.");
        if constexpr (T::rank() == 2) return VK_FORMAT_R32G32_UINT;
        else if constexpr (T::rank() == 3) return VK_FORMAT_R32G32B32_UINT;
        else if constexpr (T::rank() == 4) return VK_FORMAT_R32G32B32A32_UINT;
    }
}
