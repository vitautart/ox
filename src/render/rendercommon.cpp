#include <definitions.hpp>

#if defined(OX_LINUX_OS)
#define GLFW_EXPOSE_NATIVE_X11
#endif

#include <render/rendercommon.hpp>
#include <fileutils.hpp>
#include <generalutils.hpp>

#include <X11/Xlib.h>
#if defined(OX_LINUX_OS)
#include <vulkan/vulkan_xlib.h>
#endif
#include <GLFW/glfw3.h>
#include <GLFW/glfw3native.h>


#define STB_TRUETYPE_IMPLEMENTATION
#include <stb/stb_truetype.h>

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <stb/stb_image_write.h>

auto ox::Image::create(const VkImageCreateInfo& iImageInfo, 
        const VkImageViewCreateInfo& iViewInfo, VmaAllocator iAllocator, VkDevice iDevice) -> VkResult
{
    //VkImage image;
    VmaAllocation allocation;
    VmaAllocationInfo allocInfo;
    VmaAllocationCreateInfo allocCreateInfo = 
    {
        .flags = 0,
        .usage = VmaMemoryUsage::VMA_MEMORY_USAGE_GPU_ONLY,
        // .requiredFlags = 0, .preferredFlags = 0,
        // .memoryTypeBits = 0,
        // .pool = VK_NULL_HANDLE, .pUserData = nullptr, .priority = 0
    };

    auto result = vmaCreateImage(iAllocator, &iImageInfo, &allocCreateInfo, &image, &allocation, &allocInfo);
    if (result != VK_SUCCESS) return result;
    //this->image = image;
    this->memory = allocation;


    VkImageViewCreateInfo viewInfo = iViewInfo;
    viewInfo.image = image;
    //VkImageView view;
    result = vkCreateImageView(iDevice, &viewInfo, nullptr, &view);
    if (result != VK_SUCCESS) return result;
    //this->view = view;
    this->format = iImageInfo.format;
    this->source = ImageSource::ALLOCATED;

    return result;
}
auto ox::Image::create(VkImage iVkImage, ImageSource iSource, 
            const VkImageViewCreateInfo& iViewInfo, VmaAllocator iAllocator, VkDevice iDevice) -> VkResult
{
    VkImageViewCreateInfo viewInfo = iViewInfo;
    viewInfo.image = iVkImage;
    VkImageView view;
    auto result = vkCreateImageView(iDevice, &viewInfo, nullptr, &view);
    if (result != VK_SUCCESS) return result;
    this->view = view;
    this->format = iViewInfo.format;
    this->memory = VK_NULL_HANDLE;
    this->source = iSource;
    this->image = iVkImage;

    return result;
}

auto ox::Image::destroy(VmaAllocator iAllocator, VkDevice iDevice) -> void
{
    if (view)
        vkDestroyImageView(iDevice, view, nullptr);
    if (image && memory)
        vmaDestroyImage(iAllocator, image, memory);
    view = VK_NULL_HANDLE;
    image = VK_NULL_HANDLE;
    memory = VK_NULL_HANDLE;
}

auto ox::checkPresentSupport(VkPhysicalDevice iDevice) -> bool
{
    std::vector<VkQueueFamilyProperties> queueProps;
    vkenum(vkGetPhysicalDeviceQueueFamilyProperties, queueProps, iDevice);

    for (size_t i = 0; i < queueProps.size(); i++)
        if (checkPresentSupport(iDevice, i))
            return true;

    return false;
}

auto ox::checkPresentSupport(VkPhysicalDevice iDevice, int iFamilyQueueId) -> bool
{
#ifdef OX_LINUX_OS
    auto display = glfwGetX11Display();
    auto visual = XDefaultVisual(display, XDefaultScreen(display));
    return vkGetPhysicalDeviceXlibPresentationSupportKHR(iDevice, 
            iFamilyQueueId, display, visual->visualid) == VK_TRUE;
#else
    static_assert(false);
    return false;
#endif
}

auto ox::getDeviceScore(VkPhysicalDevice iDevice) noexcept -> float
{
    VkPhysicalDeviceProperties props;
    vkGetPhysicalDeviceProperties(iDevice, &props);

    float result = 0.0f;

    result += props.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU ? 1.0f : 0;
    result += props.deviceType == VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU ? 0.5f : 0;

    return result;
}

namespace ox
{
template <typename Func>
int stbtt_BakeFontBitmap(unsigned char *data, int offset,  // font location (use offset=0 for plain .ttf)
                                float pixel_height,                     // height of font in pixels
                                unsigned char *pixels, int pw, int ph,  // bitmap to be filled in
                                int first_char, int num_chars,          // characters to bake
                                const Func& fetchGlyphInfo)
{
    float scale;
    int x,y,bottom_y, i;
    stbtt_fontinfo f;
    f.userdata = NULL;
    if (!stbtt_InitFont(&f, data, offset))
    {
        printf("[ERROR] Font can't be initialized.\n");
        return -1;
    }
    STBTT_memset(pixels, 0, pw*ph); // background of 0 around pixels
    x=y=1;
    bottom_y = 1;

    scale = stbtt_ScaleForPixelHeight(&f, pixel_height);

    for (i=0; i < num_chars; ++i) 
    {
        int codepoint = first_char + i;
        int advance, lsb, x0,y0,x1,y1,gw,gh;
        int g = stbtt_FindGlyphIndex(&f, codepoint);
        stbtt_GetGlyphHMetrics(&f, g, &advance, &lsb);
        stbtt_GetGlyphBitmapBox(&f, g, scale,scale, &x0,&y0,&x1,&y1);
        gw = x1-x0;
        gh = y1-y0;
        if (x + gw + 1 >= pw)
            y = bottom_y, x = 1; // advance to next row
        if (y + gh + 1 >= ph) // check if it fits vertically AFTER potentially moving to next row
            return -i;
        STBTT_assert(x+gw < pw);
        STBTT_assert(y+gh < ph);
        stbtt_MakeGlyphBitmap(&f, pixels+x+y*pw, gw,gh,pw, scale,scale, g);
        //stbtt_MakeGlyphBitmapSubpixel(&f, pixels+x+y*pw, gw,gh,pw, scale,scale, 0.125f, 0.f, g);
        //fetchGlyphInfo(codepoint, x0, y0, x1, y1, scale * advance, x0, y0);
        fetchGlyphInfo(codepoint, x0, y0, x1, y1, x, y, x + gw, y + gh, scale * advance);
        /*
        chardata[i].xoff     = (float) x0;
        chardata[i].yoff     = (float) y0;
        */
        x = x + gw + 1;
        if (y+gh+1 > bottom_y)
            bottom_y = y+gh+1;
    }
    return bottom_y;
}
}

auto ox::BitmapFont::create(VkDevice iDevice, VmaAllocator iAllocator, VkCommandBuffer iCommandBuffer,
                const char* iFilepath, int iLineHeight, 
                int iBitmapWidth, int iBitmapHeight) -> ox::maybe<CPUBuffer>
{
    if (mInitialized)
        return {};

    mInitialized = true;
    mHeight = iLineHeight;
    size_t charsCount = 96;

    VkBufferCreateInfo bufferInfo = 
    {
        .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .size = uint32_t(iBitmapWidth * iBitmapHeight),
        .usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
        .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
        .queueFamilyIndexCount = 0,
        .pQueueFamilyIndices= nullptr
    };
    VmaAllocationCreateInfo allocInfo = 
    {
            .flags = VmaAllocationCreateFlagBits::VMA_ALLOCATION_CREATE_MAPPED_BIT,
            .usage = VmaMemoryUsage::VMA_MEMORY_USAGE_CPU_ONLY
    };
    CPUBuffer buffer;
    auto result = buffer.create(iAllocator, bufferInfo);
    if (result != VK_SUCCESS) return {};

    std::vector<unsigned char> rawFont;
    auto resbool = ox::readFile(iFilepath, true, rawFont);
    if (!resbool)
    {
        printf("[ERROR] Can't open file.\n");
        buffer.destroy(iAllocator);
        return {};
    }

    // TODO: fetch some general info about font, like max height of posible bounding box of average text, etc 
    auto fetchGlyphInfo = [&](int codepoint, 
            int x0, int y0, int x1, int y1, 
            int u0, int v0, int u1, int v1, int advance)
    {
        // TODO: this always should be the same
        mAdvance = advance;
        mGlyphMap.emplace(codepoint, Glyph
        {
            .x0 = oxm::i16(x0), .y0 = oxm::i16(y0),
            .x1 = oxm::i16(x1), .y1 = oxm::i16(y1),
            .u0 = oxm::i16(u0), .v0 = oxm::i16(v0),
            .u1 = oxm::i16(u1), .v1 = oxm::i16(v1),
        });
    };
    auto res = stbtt_BakeFontBitmap(rawFont.data(), stbtt_GetFontOffsetForIndex(rawFont.data(), 0), iLineHeight,
            (unsigned char*)buffer.data(), iBitmapWidth, iBitmapHeight, 32, charsCount, fetchGlyphInfo);

    // TODO: we use height of 'u' char to determine height,
    // it is totally wrong but in this case we avoid go though each of chars in iText
    // and it is fine for our cases
    const auto& uChar = mGlyphMap.find('u')->second;
    mSmallCharHeight = uChar.y1 - uChar.y0;
    // DEBUG
    // res = stbi_write_png("test.png", iBitmapWidth, iBitmapHeight, 
    // 1, (unsigned char*)refBuffer.ptr, iBitmapWidth);
    
    VkImageCreateInfo imageInfo = 
    {
        .sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .imageType = VkImageType::VK_IMAGE_TYPE_2D,
        .format = VkFormat::VK_FORMAT_R8_SRGB,
        .extent = { uint32_t(iBitmapWidth), uint32_t(iBitmapHeight), 1},
        .mipLevels = 1,
        .arrayLayers = 1,
        .samples = VK_SAMPLE_COUNT_1_BIT,
        .tiling = VK_IMAGE_TILING_OPTIMAL,
        .usage = VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
        .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
        .queueFamilyIndexCount = 0,
        .pQueueFamilyIndices = nullptr,
        .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
    };
    VkImageViewCreateInfo viewInfo = 
    {
        .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .image = VK_NULL_HANDLE,
        .viewType = VkImageViewType::VK_IMAGE_VIEW_TYPE_2D,
        .format = imageInfo.format,
        .components = RGBA_COMPONENT_MAPPING_STANDART,
        .subresourceRange =
        {
            .aspectMask = VkImageAspectFlagBits::VK_IMAGE_ASPECT_COLOR_BIT,
            .baseMipLevel = 0,
            .levelCount = 1,
            .baseArrayLayer = 0,
            .layerCount = 1
        }
    };
    result = mAtlas.create(imageInfo, viewInfo, iAllocator, iDevice);
    if (result != VK_SUCCESS) 
    {
        printf("[ERROR] Can't create font image.\n");
        buffer.destroy(iAllocator);
        return {};
    }

    VkImageMemoryBarrier2 imageBarrier = 
    {
        .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER_2,
        .pNext = nullptr,
        .srcStageMask = VK_PIPELINE_STAGE_2_NONE,
        .srcAccessMask = VK_ACCESS_2_NONE,
        .dstStageMask = VK_PIPELINE_STAGE_2_TRANSFER_BIT,
        .dstAccessMask = VK_ACCESS_2_TRANSFER_WRITE_BIT,
        //.dstAccessMask = VK_ACCESS_2_MEMORY_WRITE_BIT,
        .oldLayout = VK_IMAGE_LAYOUT_UNDEFINED,
        .newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .image = mAtlas.image,
        .subresourceRange =
        {
            .aspectMask = VkImageAspectFlagBits::VK_IMAGE_ASPECT_COLOR_BIT,
            .baseMipLevel = 0,
            .levelCount = 1,
            .baseArrayLayer = 0,
            .layerCount = 1
        }
    };
    VkDependencyInfo depInfo = 
    {
        .sType = VK_STRUCTURE_TYPE_DEPENDENCY_INFO,
        .pNext = nullptr,
        .dependencyFlags = 0,
        .memoryBarrierCount = 0,
        .pMemoryBarriers = nullptr,
        .bufferMemoryBarrierCount = 0,
        .pBufferMemoryBarriers = nullptr,
        .imageMemoryBarrierCount = 1,
        .pImageMemoryBarriers = &imageBarrier
    };
    vkCmdPipelineBarrier2(iCommandBuffer, &depInfo);
    VkBufferImageCopy copyInfo = 
    {
        .bufferOffset = 0,
        .bufferRowLength = 0,
        .bufferImageHeight = 0,
        .imageSubresource = 
        {
            .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
            .mipLevel = 0,
            .baseArrayLayer = 0,
            .layerCount = 1
        },
        .imageOffset = {0, 0, 0},
        .imageExtent = imageInfo.extent 
    };
    
    vkCmdCopyBufferToImage(iCommandBuffer, buffer.buffer(), mAtlas.image, 
            VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &copyInfo);

    imageBarrier = VkImageMemoryBarrier2
    {
        .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER_2,
        .pNext = nullptr,
        .srcStageMask = VK_PIPELINE_STAGE_2_TRANSFER_BIT,
        .srcAccessMask = VK_ACCESS_2_TRANSFER_WRITE_BIT,
        .dstStageMask = VK_PIPELINE_STAGE_2_FRAGMENT_SHADER_BIT,
        .dstAccessMask = VK_ACCESS_2_SHADER_READ_BIT,
        .oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        .newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
        .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .image = mAtlas.image,
        .subresourceRange =
        {
            .aspectMask = VkImageAspectFlagBits::VK_IMAGE_ASPECT_COLOR_BIT,
            .baseMipLevel = 0,
            .levelCount = 1,
            .baseArrayLayer = 0,
            .layerCount = 1
        }
    };
    depInfo = VkDependencyInfo
    {
        .sType = VK_STRUCTURE_TYPE_DEPENDENCY_INFO,
        .pNext = nullptr,
        .dependencyFlags = 0,
        .memoryBarrierCount = 0,
        .pMemoryBarriers = nullptr,
        .bufferMemoryBarrierCount = 0,
        .pBufferMemoryBarriers = nullptr,
        .imageMemoryBarrierCount = 1,
        .pImageMemoryBarriers = &imageBarrier
    };
    vkCmdPipelineBarrier2(iCommandBuffer, &depInfo);

    return {buffer};
}

auto ox::BitmapFont::measureQuick(const std::u32string_view iText) const noexcept -> oxm::vf2
{
    // TODO: only monospaced font has advance same
    const auto advance = float(mAdvance);
    return {advance * iText.size(), float(mSmallCharHeight) };
}

auto ox::BitmapFont::destroy(VkDevice iDevice, VmaAllocator iAllocator) -> void
{
    if (!mInitialized)
        return;
    mGlyphMap = {};
    mAtlas.destroy(iAllocator, iDevice);
    mAtlas = {};
    mInitialized = false;
}
