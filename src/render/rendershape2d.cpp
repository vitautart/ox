#include <render/rendershape2d.hpp>
#include <render/pipelines.hpp>

#include <memory.h>
#include <stdio.h>

// ---------------------------------------------------------------------------
auto ox::BufferShape2DBase::clear() noexcept -> void
{
    mVertices.clear();
    mShouldLoad[mNextBucket] = true;
    mVerticesCounts[mNextBucket] = mVertices.size();
}

auto ox::BufferShape2DBase::load(VkCommandBuffer iCommandBuffer) -> bool
{
    if (!mShouldLoad[mNextBucket])
        return false;

    mShouldLoad[mNextBucket] = false;
    mCurrentBucket = mNextBucket;
    mNextBucket = (mNextBucket + 1) % MAX_CONCURRENT_FRAMES;

    if (mVertices.empty())
        return false;

    // Should use mCyrrentBucket now

    auto bytes = mVertices.size() * sizeof(RenderVertexUI);
    auto res = mVertexBuffers[mCurrentBucket].recreate(mRender->getAllocator(), 
            VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, bytes);
    CHECK_VK(res, "[ERROR] Vertex buffer creation failed: %i\n");
    mVertexBuffers[mCurrentBucket].write(mVertices.data(), bytes, 0);

    VkBufferCopy region =
    {
        .srcOffset = 0,
        .dstOffset = 0,
        .size = mVertexBuffers[mCurrentBucket].size()
    };
    
    vkCmdCopyBuffer(iCommandBuffer, mVertexBuffers[mCurrentBucket].cpubuffer(), 
            mVertexBuffers[mCurrentBucket].gpubuffer(), 1, &region);

    return true;
}

auto ox::BufferShape2DBase::destroy() noexcept -> void 
{
    for (auto& b : mVertexBuffers) 
        b.destroy(mRender->getAllocator());

    *this = BufferShape2DBase{};
}

auto ox::BufferShape2DBase::buffer() const noexcept -> const VkBuffer&
{
    return mVertexBuffers[mCurrentBucket].gpubuffer();
}

auto ox::BufferShape2DBase::size() const noexcept -> oxm::u32
{
    return mVerticesCounts[mCurrentBucket];
}
// ---------------------------------------------------------------------------

auto ox::BufferShape2DSolid::add(const oxm::vf2& topleft,  const oxm::vf2& size, const oxm::vu4& color) -> void
{
    const auto col = oxm::packUnorm4x8(color);
    const ox::RenderVertexUI v[4] = 
    {
        { .pos = topleft,  .uv = {0, 0}, .col = col },
        { .pos = { topleft[0] + size[0], topleft[1] }, .uv = {1, 0}, .col = col },
        { .pos = topleft + size, .uv = {1, 1}, .col = col },
        { .pos = { topleft[0], topleft[1] + size[1] },  .uv = {0, 1}, .col = col }
    };
    mVertices.push_back(v[0]);
    mVertices.push_back(v[1]);
    mVertices.push_back(v[3]);

    mVertices.push_back(v[3]);
    mVertices.push_back(v[1]);
    mVertices.push_back(v[2]);

    mShouldLoad[mNextBucket] = true;
    mVerticesCounts[mNextBucket] = mVertices.size();
}

auto ox::BufferShape2DWire::add(const oxm::vf2& topleft,  const oxm::vf2& size, const oxm::vu4& color) -> void
{
    const auto col = oxm::packUnorm4x8(color);
    const ox::RenderVertexUI v[4] = 
    {
        { .pos = topleft,  .uv = {0, 0}, .col = col },
        { .pos = { topleft[0] + size[0], topleft[1] }, .uv = {1, 0}, .col = col },
        { .pos = topleft + size, .uv = {1, 1}, .col = col },
        { .pos = { topleft[0], topleft[1] + size[1] },  .uv = {0, 1}, .col = col }
    };

    mVertices.push_back(v[0]); mVertices.push_back(v[1]);
    mVertices.push_back(v[1]); mVertices.push_back(v[2]);
    mVertices.push_back(v[2]); mVertices.push_back(v[3]);
    mVertices.push_back(v[3]); mVertices.push_back(v[0]);

    mShouldLoad[mNextBucket] = true;
    mVerticesCounts[mNextBucket] = mVertices.size();
}


auto ox::RenderShape2D::create(const Render* iRender) -> bool
{
    if (mInitialized)
        return true;
    else
        mInitialized = true;

    mRender = iRender; 

    CALL_CHECK_DESTROY_RETURN(createLayouts());
    CALL_CHECK_DESTROY_RETURN(createPipelines());

    return true;
}

auto ox::RenderShape2D::destroy() -> void 
{
    if (!mInitialized)
        return;

    if (mFillPipeline)
        vkDestroyPipeline(mRender->getDevice(), mFillPipeline, nullptr);
    mFillPipeline = VK_NULL_HANDLE;

    if (mBorderPipeline)
        vkDestroyPipeline(mRender->getDevice(), mBorderPipeline, nullptr);
    mBorderPipeline = VK_NULL_HANDLE;

    if (mPipelineLayout)
        vkDestroyPipelineLayout(mRender->getDevice(), mPipelineLayout, nullptr);
    mPipelineLayout = VK_NULL_HANDLE;

    mInitialized = false;
}

auto ox::RenderShape2D::drawBegin(VkCommandBuffer iCommandBuffer, 
        const VkRect2D& iRect, VkImageView iColorView) const -> void
{
    VkViewport viewport = 
    {
        .x = (float)iRect.offset.x,
        .y = (float)iRect.offset.y,
        .width = (float)iRect.extent.width,
        .height = (float)iRect.extent.height,
        .minDepth = 0.0f,
        .maxDepth = 1.0f
    };

    VkRenderingAttachmentInfo colorAttachment = 
    {
        .sType = VK_STRUCTURE_TYPE_RENDERING_ATTACHMENT_INFO,
        .pNext = nullptr,
        .imageView = iColorView,
        .imageLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
        .resolveMode = VkResolveModeFlagBits::VK_RESOLVE_MODE_NONE,
        .resolveImageView = VK_NULL_HANDLE, 
        .resolveImageLayout = VK_IMAGE_LAYOUT_UNDEFINED,
        .loadOp = VkAttachmentLoadOp::VK_ATTACHMENT_LOAD_OP_CLEAR,
        .storeOp = VkAttachmentStoreOp::VK_ATTACHMENT_STORE_OP_STORE,
        .clearValue = { .color = {0.05f, 0.05f, 0.05f, 1.0f} }
    };

    VkRenderingInfo renderingInfo = 
    {
        .sType = VK_STRUCTURE_TYPE_RENDERING_INFO,
        .pNext = nullptr,
        .flags = 0, // TODO: maybe here can be something usefull
        .renderArea = iRect,
        .layerCount = 1, 
        .viewMask = 0,
        .colorAttachmentCount = 1,
        .pColorAttachments = &colorAttachment,
        .pDepthAttachment = nullptr, 
        .pStencilAttachment = nullptr
    };

    vkCmdBeginRendering(iCommandBuffer, &renderingInfo);

    vkCmdSetViewport(iCommandBuffer, 0, 1, &viewport);
    vkCmdSetScissor(iCommandBuffer, 0, 1, &iRect);
    oxm::vf2 halfvp = {viewport.width * 0.5f, viewport.height* 0.5f};
    vkCmdPushConstants(iCommandBuffer, mPipelineLayout, VK_SHADER_STAGE_VERTEX_BIT, 0, sizeof(halfvp), &halfvp);
}


auto ox::RenderShape2D::draw(VkCommandBuffer iCommandBuffer, const BufferShape2DSolid& iBuffer) const -> void
{
    vkCmdBindPipeline(iCommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, mFillPipeline);
    VkDeviceSize offsets[1] = {0};
    vkCmdBindVertexBuffers(iCommandBuffer, 0, 1, &iBuffer.buffer(), offsets);
    vkCmdDraw(iCommandBuffer, iBuffer.size(), 1, 0, 0);
}    

auto ox::RenderShape2D::draw(VkCommandBuffer iCommandBuffer, const BufferShape2DWire& iBuffer) const -> void
{
    vkCmdBindPipeline(iCommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, mBorderPipeline);
    VkDeviceSize offsets[1] = {0};
    vkCmdBindVertexBuffers(iCommandBuffer, 0, 1, &iBuffer.buffer(), offsets);
    vkCmdDraw(iCommandBuffer, iBuffer.size(), 1, 0, 0);
}

auto ox::RenderShape2D::drawEnd(VkCommandBuffer iCommandBuffer) -> void
{
    vkCmdEndRendering(iCommandBuffer);
}

auto ox::RenderShape2D::createLayouts() -> bool
{
    VkPushConstantRange pushConstantRange = 
    {
        .stageFlags = VkShaderStageFlagBits::VK_SHADER_STAGE_VERTEX_BIT,
        .offset = 0,
        .size = sizeof(oxm::vf2)
    };
    VkPipelineLayoutCreateInfo layoutInfo = 
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .setLayoutCount = 0,
        .pSetLayouts = nullptr,
        .pushConstantRangeCount = 1,
        .pPushConstantRanges = &pushConstantRange
    };

    auto result = vkCreatePipelineLayout(mRender->getDevice(), &layoutInfo, nullptr, &mPipelineLayout);
    CHECK_AND_RETURN(result, "Pipeline layout creation failed: %i\n");

    return true;
}

auto ox::RenderShape2D::createPipelines() -> bool
{
    auto result = ox::pipelines::uiblock_v1(mRender->getDevice(), mPipelineLayout, 
            mRender->get(ox::ShaderResource::UIBLOCK_VERT), 
            mRender->get(ox::ShaderResource::UIBLOCK_FRAG), 
            mRender->getTargetColorFormat(), 
            VkPrimitiveTopology::VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
            VkPolygonMode::VK_POLYGON_MODE_FILL,
            &mFillPipeline);
    CHECK_AND_RETURN(result, "[ERROR] Pipeline creation failed: %i\n");

    result = ox::pipelines::uiblock_v1(mRender->getDevice(), mPipelineLayout, 
            mRender->get(ox::ShaderResource::UIBLOCK_VERT), 
            mRender->get(ox::ShaderResource::UIBLOCK_FRAG), 
            mRender->getTargetColorFormat(), 
            VkPrimitiveTopology::VK_PRIMITIVE_TOPOLOGY_LINE_LIST,
            VkPolygonMode::VK_POLYGON_MODE_LINE,
            //VkPolygonMode::VK_POLYGON_MODE_FILL,
            &mBorderPipeline);
    CHECK_AND_RETURN(result, "[ERROR] Pipeline creation failed: %i\n");

    return true;
}
