#include "fileutils.hpp"

#include <cstdio>

namespace ox
{   

    class FileHandle 
    {
    public:
        FileHandle() = delete;
        FileHandle(const char* iFilename, const char* iMode) noexcept
        {
            mRawFile = fopen(iFilename, iMode);
        }
        FileHandle(const FileHandle&) = delete;
        auto operator = (const FileHandle&) = delete;

        auto getRawFile() noexcept -> FILE*
        {
            return mRawFile;
        }

        ~FileHandle()
        {
            if (mRawFile)
                fclose(mRawFile);
        }
    private:
        FILE* mRawFile = nullptr;
    };

    template<typename T>
    auto readFileTemplate(const char* iFilename, bool iBinary, std::vector<T>& oData) -> bool
    {
        const char* mode = iBinary ? "rb" : "r";
        auto file = FileHandle(iFilename, mode);
        auto fp = file.getRawFile();
        if(!fp)
            return false;

        int seek_res = fseek(fp, 0, SEEK_END);
        if (seek_res != 0)
            return false;

        long m_size = ftell(fp);
        if(m_size == -1L)
            return false;

        long null_term = iBinary ? 0 : 1;
        
        long size = m_size + null_term;

        oData.resize(size);
        rewind(fp);

        size_t read_res = fread(oData.data(), 1, m_size, fp);
        if(read_res != m_size)
            return false;

        if (!iBinary)
            oData.back() = '\0';

        return true;
    }
}



auto ox::readFile(const char* iFilename, bool iBinary, std::vector<char>& oData) -> bool
{
    return readFileTemplate(iFilename, iBinary, oData);
}

auto ox::readFile(const char* iFilename, bool iBinary, std::vector<unsigned char>& oData) -> bool
{
    return readFileTemplate(iFilename, iBinary, oData);
}

