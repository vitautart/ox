#pragma once

#include<vulkan/vulkan.h>
#include <sumtypes.hpp>

namespace ox { namespace pipelines 
{
    auto mesh3d_v1(
            VkDevice device, 
            VkPipelineLayout layout, 
            VkShaderModule vertexShader, 
            VkShaderModule fragmentShader, 
            VkFormat colorFormat, 
            VkFormat depthFormat,
            VkSampleCountFlagBits samples,
            VkPipeline* pipeline
            ) -> VkResult;
    auto wireframe3d_v1(
            VkDevice device, 
            VkPipelineLayout layout, 
            VkShaderModule vertexShader, 
            VkShaderModule fragmentShader, 
            VkFormat colorFormat, 
            VkFormat depthFormat,
            VkSampleCountFlagBits samples,
            VkPipeline* pipeline
            ) -> VkResult;
    auto uiblock_v1(
            VkDevice device, 
            VkPipelineLayout layout, 
            VkShaderModule vertexShader, 
            VkShaderModule fragmentShader, 
            VkFormat colorFormat, 
            VkPrimitiveTopology topology,
            VkPolygonMode polyMode,
            VkPipeline* pipeline
            ) -> VkResult;
    auto text2d(
            VkDevice device, 
            VkPipelineLayout layout, 
            VkShaderModule vertexShader, 
            VkShaderModule fragmentShader, 
            VkFormat colorFormat,
            VkPipeline* pipeline
            ) -> VkResult;
}}
