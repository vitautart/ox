#pragma once

#include <topobase.hpp>

namespace ox
{
struct TopoPlaneSurface : public ITopoSurface 
{ 
    TopoPlaneSurface(const oxm::Plane<double>& iPlane) : plane(iPlane) {} 
    virtual auto transform(const oxm::md4& iTransform) noexcept -> void
    {
        plane.point = (iTransform * oxm::grow(plane.point, 1.0)).shrink<3>();
        plane.normal = (iTransform * oxm::grow(plane.normal, 0.0)).shrink<3>();
        plane.xdir = (iTransform * oxm::grow(plane.xdir, 0.0)).shrink<3>();
        plane.ydir = oxm::norm(oxm::cross(plane.normal, plane.xdir));
    };
    virtual auto discretize(
        const std::vector<std::span<TopoPoint>>& iLoops,
        const std::function<void(const TopoPoint&, const TopoDir&)>& cbAddPoint,
        const std::function<void(uint32_t, uint32_t, uint32_t)>& cbAddTriangle
        ) const -> void;
    virtual auto normal(const TopoPoint& iPoint) const noexcept -> TopoPoint
    {
        return plane.normal;
    }
    oxm::Plane<double> plane; 
};
}
