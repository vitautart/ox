#pragma once

#include <vulkan/vulkan.h>

#include <vk_mem_alloc.h> // AMD VULKAN ALLOCATION LIB
#include <vulkan/vulkan_core.h>

#include <sumtypes.hpp>

namespace ox
{
    class CPUBuffer
    {
    public: 
        CPUBuffer() noexcept = default;
        auto create(VmaAllocator iAllocator, const VkBufferCreateInfo& iCreateInfo) noexcept -> VkResult;
        auto destroy(VmaAllocator iAllocator) noexcept -> void;

        auto buffer() const noexcept -> const VkBuffer& { return mBuffer; };
        //auto buffer() noexcept -> const VkBuffer& { return mBuffer; };
        auto size() const noexcept -> VkDeviceSize { return mSize; };
        auto write(const void* iSrc, VkDeviceSize iSrcSize, VkDeviceSize iDstOffset) noexcept -> void;
        auto data() noexcept -> void* { return mPtr; }
    private:
        VkBuffer mBuffer = VK_NULL_HANDLE;
        void* mPtr = nullptr;
        VkDeviceSize mSize = 0;
        VmaAllocation mMemory = VK_NULL_HANDLE;
    };

    class GPUBuffer
    {
    public:
        GPUBuffer() = default;
        auto create(VmaAllocator iAllocator, const VkBufferCreateInfo& iCreateInfo) noexcept -> VkResult;
        auto destroy(VmaAllocator iAllocator) noexcept -> void;
       
        auto buffer() const noexcept -> const VkBuffer& { return mBuffer; };
        auto size() const noexcept -> VkDeviceSize { return mSize; };
 
    private:
        VkBuffer mBuffer = VK_NULL_HANDLE;
        VkDeviceSize mSize = 0;
        VmaAllocation mMemory = VK_NULL_HANDLE;
    };

    class GPUPreferMappedBuffer
    {
    public: 
        GPUPreferMappedBuffer() noexcept = default;
        auto create(VmaAllocator iAllocator, const VkBufferCreateInfo& iCreateInfo) noexcept -> VkResult;
        auto destroy(VmaAllocator iAllocator) noexcept -> void;

        auto buffer() const noexcept -> const VkBuffer& { return mBuffer; };
        //auto buffer() noexcept -> const VkBuffer& { return mBuffer; };
        auto size() const noexcept -> VkDeviceSize { return mSize; };
        auto write(const void* iSrc, VkDeviceSize iSrcSize, VkDeviceSize iDstOffset) noexcept -> void;
        auto data() noexcept -> void* { return mPtr; }
    private:
        VkBuffer mBuffer = VK_NULL_HANDLE;
        void* mPtr = nullptr;
        VkDeviceSize mSize = 0;
        VmaAllocation mMemory = VK_NULL_HANDLE;
    };

    class CPUtoGPUBuffer
    {
    public:
        CPUtoGPUBuffer() noexcept = default;
        auto create(VmaAllocator iAllocator, VkBufferUsageFlags iFlags, VkDeviceSize iSize) noexcept -> VkResult;
        auto recreate(VmaAllocator iAllocator, VkBufferUsageFlags iFlags, VkDeviceSize iSize) noexcept -> VkResult;
        auto destroy(VmaAllocator iAllocator) noexcept -> void;

        auto size() const noexcept -> VkDeviceSize { return mCPU.size(); };
        auto cpubuffer() const noexcept -> const VkBuffer& { return mCPU.buffer(); }
        auto gpubuffer() const noexcept -> const VkBuffer& { return mGPU.buffer(); }
        auto write(const void* iSrc, VkDeviceSize iSrcSize, VkDeviceSize iDstOffset) noexcept -> void 
        {
            mCPU.write(iSrc, iSrcSize, iDstOffset);
        };
    private:
        static inline auto calculateSize(VkDeviceSize iSize) noexcept -> VkDeviceSize
        {
            return iSize < 1024 ? iSize * 2 : VkDeviceSize(1.5f * float(iSize));
        }

    private:
        CPUBuffer mCPU;
        GPUBuffer mGPU;
    };
}
