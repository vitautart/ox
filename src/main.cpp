#include <app.hpp>
#include <topolinecurve.hpp>

#include <test.hpp>

namespace 
{
auto printBREP(const ox::BRep3D& brep) -> void
{
    printf("-----------------------------------------\n");
    printf("- FACES: %zu\n", brep.getFaces().size());
    printf("- LOOPS: %zu\n", brep.getLoops().size());
    printf("- EDGES: %zu\n", brep.getEdges().size());
    printf("- HALFEDGES: %zu\n", brep.getHalfedges().size());
    printf("- VERTICES: %zu\n", brep.getVertices().size());

    printf("-----------------------------------------\n");
    auto fids = brep.getFaceIds();
    auto lids = brep.getLoopIds();
    auto eids = brep.getEdgeIds();
    auto heids = brep.getHalfedgeIds();
    auto vids = brep.getVertexIds();
    printf("- VALID FACES: %zu\n", fids.size());
    printf("- VALID LOOPS: %zu\n", lids.size());
    printf("- VALID EDGES: %zu\n", eids.size());
    printf("- VALID HALFEDGES: %zu\n", heids.size());
    printf("- VALID VERTEX: %zu\n", vids.size());
    printf("-----------------------------------------\n");
}

void generateBase6(std::vector<std::vector<ox::TopoPoint>>& vertices, std::vector<std::vector<std::unique_ptr<ox::ITopoCurve>>>& curves)
{
    vertices.emplace_back();
    vertices.back() = std::vector<ox::TopoPoint>
    {
        {-2,  2, 0},
        {-2, -2, 0},
        { 3, -2, 0},
        { 3,  0, 0},
        { 1,  2, 0}
    };
    vertices.emplace_back();
    vertices.back() = std::vector<ox::TopoPoint>
    {
        {-1,  1, 0},
        { 1,  1, 0},
        { 1, -1, 0},
        {-1, -1, 0},
    };
    for(auto v : vertices)
    {
        curves.emplace_back();
        auto& c = curves.back();
        for (size_t i = 0; i < v.size(); i++)
        {
            auto dir = oxm::norm(v[(i + 1) % v.size()] - v[i]);
            c.push_back(std::make_unique<ox::TopoLineCurve>(oxm::Line3D<double>{ v[i], dir }));
        }
    }
}

void generateSolid(ox::BRep3D& obrep)
{
    std::vector<std::vector<ox::TopoPoint>> vertices;
    std::vector<std::vector<std::unique_ptr<ox::ITopoCurve>>> curves;
    generateBase6(vertices, curves);

    std::vector<std::span<ox::TopoPoint>> vertsInput;
    std::vector<std::span<std::unique_ptr<ox::ITopoCurve>>> curvesInput;   
    for (auto& v : vertices)
        vertsInput.push_back(v);
     for (auto& c : curves)
        curvesInput.push_back(c);   
    auto [brep, valid] = ox::BRep3D::createPlanarFace(vertsInput, curvesInput, oxm::Plane<double>{.normal = {0, 0, 1}, .xdir = {1, 0, 0}, .ydir = {0, 1, 0} });
    if (valid)
        printf("[TEST] BREP SUCCSESSFULLY CREATED:\n");
    else
        printf("[ERROR] BREP CREATION FAILED\n");

    printBREP(brep);   

    auto fids = brep.getFaceIds();
    fids = brep.getFaceIds();
    brep.extrudeFace(fids[0], 4);

    printBREP(brep);

    obrep = std::move(brep);
}
}

auto main() -> int
{
#if 1
    ox::BRep3D brep = {};
    generateSolid(brep);

    bool res = false;
    ox::App app {res};
    if (!res)
    {
        printf("[ERROR] Can't create application\n");
        return 1;
    }
    app.addDocument(std::move(brep));
    app.run();
#else
    test2::run();
#endif
    return 0;
}
