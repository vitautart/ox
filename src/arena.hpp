#pragma once
#include <malloc.h>
#ifdef ARENA_DEBUG
#include <cstdio>
#endif

namespace ox
{
    template <typename T>
    class arena
    {
        struct chunk 
        {
            T* data;
            unsigned long long count;
            chunk* next; 
        };
    public:
        arena() = delete;
        arena(size_t chunkSize) : 
            mChunkSize(chunkSize),
            mRoot(nullptr),
            mCurrent(nullptr) {}
        ~arena(){ free(); }

        arena(const arena&) = delete;
        arena(arena&& other) noexcept
        {
            *this = other;
        }
        auto operator=(const arena& other) = delete;
        auto operator=(arena&& other) noexcept
        {
            free();
            mRoot = other.mRoot;
            mCurrent = other.mCurrent;
            mChunkSize = other.mChunkSize;
            other.mRoot = nullptr;
            other.mCurrent = nullptr;
            other.mChunkSize = 0;
        }

        auto alloc() -> T*
        {
            if (!mCurrent)
            {
                mRoot = chunkalloc();
                mCurrent = mRoot;
            }
            else if (mCurrent->count == mChunkSize)
            {
                mCurrent->next = chunkalloc();
                mCurrent = mCurrent->next;
            }
            return &(mCurrent->data[mCurrent->count++]);
        }

        auto free() noexcept -> void
        {
            auto current = mRoot;
            while (current)
            {
                chunk* temp = current;
                current = temp->next;
                delete[] temp->data;
                delete temp;
                //::free(temp->data);
                //::free(temp);
            }
        }

    private:
        auto chunkalloc() -> chunk*
        {
            //auto c = static_cast<chunk*>(malloc(sizeof(chunk)));
            auto c = new chunk;
            *c = chunk 
            {
                //.data = static_cast<T*>(malloc(mChunkSize * sizeof(T))),
                .data = new T[mChunkSize],
                .count = 0,
                .next = nullptr
            };
            return c;
        }
    private:
        chunk* mRoot;
        chunk* mCurrent;
        size_t mChunkSize;
        
    };

    template <typename T, size_t chunksize = 64>
    struct ArenaAllocator
    {
        ArenaAllocator(): mArena(chunksize) {};
        //ArenaAllocator(size_t chunkSize) : mArena(chunkSize) {} 
        using value_type = T;
        // Only allowed n = 1, because arena doesn't support continous vector-like allocations
        inline auto allocate(size_t n) -> T*
        {
#ifdef ARENA_DEBUG
            if (n > 1 || n == 0)
            {
                printf("[ERROR] wrong number of allocations for arena\n");
                return nullptr;
            }
#endif
            auto result = mArena.alloc();
            /*for (size_t i = 1; i < n; i++)
                mArena.alloc();*/
            return result; 
        }
        inline auto deallocate(T* p, size_t n) noexcept -> void { }
    private:
        arena<T> mArena;
    };
}
