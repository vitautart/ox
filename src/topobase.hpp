#pragma once

#include <vector>
#include <span>
#include <memory>
#include <functional>

#include <oxgeometry.hpp>

namespace ox
{
using TopoPoint = oxm::Point3D<double>;
using TopoPoint2D = oxm::Point2D<double>;
using TopoDir = oxm::Direction3D<double>;
using TopoDir2D = oxm::Direction2D<double>;

struct ITopoCurve 
{
    virtual auto coincide(TopoPoint iPoint) const -> bool = 0;
    // Assumption point should be at the curve (coincide)
    //virtual auto tangentAt(TopoPoint iPoint) const -> TopoDir = 0;
    virtual auto plane() const -> std::optional<oxm::Plane<double>> = 0;
    virtual auto planar() const -> bool = 0;
    virtual auto transform(const oxm::md4& iTransform) -> void = 0;
    virtual auto transformed(const oxm::md4& iTransform) const -> std::unique_ptr<ITopoCurve> = 0;
    virtual auto discretize(
            TopoPoint iStart, 
            TopoPoint iEnd, 
            std::vector<TopoPoint>& oPointList) const -> void = 0;
    virtual auto clone() const -> std::unique_ptr<ITopoCurve> = 0;
};

struct ITopoSurface 
{
    virtual auto transform(const oxm::md4& iTransform) -> void = 0;
    /*virtual auto discretize(
            const std::vector<std::span<TopoPoint>>& iLoops,
            uint32_t iStartIndex,
            std::vector<TopoSurfacePoint>& oTringlePoints, 
            std::vector<uint32_t>& oTringleIndices) const -> void = 0;*/
    virtual auto discretize(
            const std::vector<std::span<TopoPoint>>& iLoops,
            const std::function<void(const TopoPoint&, const TopoDir&)>& cbAddPoint,
            const std::function<void(uint32_t, uint32_t, uint32_t)>& cbAddTriangle
            ) const -> void = 0;
    virtual auto normal(const TopoPoint& iPoint) const noexcept -> TopoPoint = 0;
};
}
