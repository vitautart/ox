#pragma once

#include <vector>

namespace ox
{   
    auto readFile(const char* iFilename, bool iBinary, std::vector<char>& oData) -> bool;
    auto readFile(const char* iFilename, bool iBinary, std::vector<unsigned char>& oData) -> bool;
}
