CC = g++
STD = -std=c++20

RENDER_DIR = src/render
PIPELINES_DIR = $(RENDER_DIR)/pipelines
RENDER_SRC = $(RENDER_DIR)/render.cpp $(RENDER_DIR)/vma_impl.cpp $(RENDER_DIR)/rendercommon.cpp $(RENDER_DIR)/renderwindow.cpp $(RENDER_DIR)/renderview3d.cpp $(RENDER_DIR)/rendershape2d.cpp $(RENDER_DIR)/res/resbuffers.cpp $(RENDER_DIR)/rendertext2d.cpp
PIPELINES_SRC = $(PIPELINES_DIR)/mesh3d_pp.cpp $(PIPELINES_DIR)/wireframe3d_pp.cpp $(PIPELINES_DIR)/uiblock_pp.cpp $(PIPELINES_DIR)/text2d_pp.cpp
TEST_SRC = src/test2.cpp
CFILES = src/main.cpp src/fileutils.cpp src/brep.cpp src/topoplanesurface2.cpp src/discretization.cpp src/ui/ui.cpp $(RENDER_SRC) $(TEST_SRC) $(PIPELINES_SRC)

INCLUDE_PATHS = -Isrc -Iexternal/glfw/include -Iexternal/vma -Iexternal/stb
LDLIBS =  -lvulkan -lglfw -ldl -lpthread -lX11
LIBSPATH = -Lbuild
DEFINITIONS = -DOX_RENDER_DEBUG
SHADERS_BUILD = build/shaders
SHADERS = \
		  $(SHADERS_BUILD)/diffuse3d_vert.spv \
		  $(SHADERS_BUILD)/diffuse3d_frag.spv \
		  $(SHADERS_BUILD)/simple3d_vert.spv \
		  $(SHADERS_BUILD)/simple3d_frag.spv \
		  $(SHADERS_BUILD)/wireframe3d_vert.spv \
		  $(SHADERS_BUILD)/uiblock_vert.spv \
		  $(SHADERS_BUILD)/uiblock_frag.spv \
		  $(SHADERS_BUILD)/text2d_vert.spv \
		  $(SHADERS_BUILD)/text2d_frag.spv

all: $(CFILES) $(SHADERS)
	$(CC) -Wno-nullability-completeness $(STD) $(DEFINITIONS) $(LIBSPATH) $(CFILES) $(INCLUDE_PATHS) $(LDLIBS) -o build/ox
	cd build && ./ox

build/shaders/simple3d_vert.spv: src/shaders/simple3d.vert
	glslc src/shaders/simple3d.vert -o $(SHADERS_BUILD)/simple3d_vert.spv

build/shaders/wireframe3d_vert.spv: src/shaders/wireframe3d.vert
	glslc src/shaders/wireframe3d.vert -o $(SHADERS_BUILD)/wireframe3d_vert.spv

build/shaders/simple3d_frag.spv: src/shaders/simple3d.frag
	glslc src/shaders/simple3d.frag -o $(SHADERS_BUILD)/simple3d_frag.spv

build/shaders/diffuse3d_vert.spv: src/shaders/diffuse3d.vert
	glslc src/shaders/diffuse3d.vert -o $(SHADERS_BUILD)/diffuse3d_vert.spv

build/shaders/diffuse3d_frag.spv: src/shaders/diffuse3d.frag
	glslc src/shaders/diffuse3d.frag -o $(SHADERS_BUILD)/diffuse3d_frag.spv

build/shaders/uiblock_vert.spv: src/shaders/uiblock.vert
	glslc src/shaders/uiblock.vert -o $(SHADERS_BUILD)/uiblock_vert.spv

build/shaders/uiblock_frag.spv: src/shaders/uiblock.frag
	glslc src/shaders/uiblock.frag -o $(SHADERS_BUILD)/uiblock_frag.spv

build/shaders/text2d_vert.spv: src/shaders/text2d.vert
	glslc src/shaders/text2d.vert -o $(SHADERS_BUILD)/text2d_vert.spv

build/shaders/text2d_frag.spv: src/shaders/text2d.frag
	glslc src/shaders/text2d.frag -o $(SHADERS_BUILD)/text2d_frag.spv

test_forest: tests/test_forest.cpp
	$(CC) -Wno-nullability-completeness $(STD) $(DEFINITIONS) $(LIBSPATH) tests/test_forest.cpp $(INCLUDE_PATHS) $(LDLIBS) -o build/test_forest
	cd build && ./test_forest

clean:
	rm -f *.o *.a

