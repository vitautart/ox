#pragma once


namespace ox
{
    template<typename Func>
    struct defer
    {
        defer() = delete;
        defer(const defer&) = delete;
        defer(defer&&) = delete;
        defer(const Func& func) noexcept : mFunctor(func){}
        ~defer() { mFunctor(); }; 
    private:
        Func mFunctor;

    };
}
