#pragma once 

#include <utility>

namespace ox
{
template<typename T>
struct maybe
{
    maybe(const T& obj) : data(obj), valid(true) { };
    maybe(T&& obj) : data(std::move(obj)), valid(true) { };
    maybe() : data(), valid(false) {};
    T data;
    bool valid;
};

/*template<typename T, typename R>
struct either
{
    either(const T& obj, R result) : data(obj), result(result) { };
    either(T&& obj, R result) : data(std::move(obj)), result(result) { };
    either(R result) : result(false) {};
    T data;
    R result;
};*/
}
