#include "brep.hpp"

#include "oxgeometry.hpp"
#include "oxmath.hpp"
#include "topolinecurve.hpp"
#include "topoplanesurface.hpp"

#include <cassert>
#include <compare>
#include <memory>
#include <optional>

namespace ox
{
    auto operator<=> (VertexIdx idx1, VertexIdx idx2) noexcept -> std::strong_ordering 
    { return idx1.idx <=> idx2.idx;}
    auto operator<=> (HalfedgeIdx idx1, HalfedgeIdx idx2) noexcept -> std::strong_ordering
    { return idx1.idx <=> idx2.idx;}
    auto operator<=> (EdgeIdx idx1, EdgeIdx idx2) noexcept -> std::strong_ordering
    { return idx1.idx <=> idx2.idx;}
    auto operator<=> (LoopIdx idx1, LoopIdx idx2) noexcept -> std::strong_ordering
    { return idx1.idx <=> idx2.idx;}
    auto operator<=> (FaceIdx idx1, FaceIdx idx2) noexcept -> std::strong_ordering
    { return idx1.idx <=> idx2.idx;}

    auto operator== (VertexIdx idx1, VertexIdx idx2) noexcept -> bool{ return idx1.idx == idx2.idx;}
    auto operator== (HalfedgeIdx idx1, HalfedgeIdx idx2) noexcept -> bool{ return idx1.idx == idx2.idx;}
    auto operator== (EdgeIdx idx1, EdgeIdx idx2) noexcept -> bool{ return idx1.idx == idx2.idx;}
    auto operator== (LoopIdx idx1, LoopIdx idx2) noexcept -> bool{ return idx1.idx == idx2.idx;}
    auto operator== (FaceIdx idx1, FaceIdx idx2) noexcept -> bool{ return idx1.idx == idx2.idx;}

    auto operator!= (VertexIdx idx1, VertexIdx idx2) noexcept -> bool{ return idx1.idx != idx2.idx;}
    auto operator!= (HalfedgeIdx idx1, HalfedgeIdx idx2) noexcept -> bool{ return idx1.idx != idx2.idx;}
    auto operator!= (EdgeIdx idx1, EdgeIdx idx2) noexcept -> bool{ return idx1.idx != idx2.idx;}
    auto operator!= (LoopIdx idx1, LoopIdx idx2) noexcept -> bool{ return idx1.idx != idx2.idx;}
    auto operator!= (FaceIdx idx1, FaceIdx idx2) noexcept -> bool{ return idx1.idx != idx2.idx;}
}

// Normal of plane should be arrranged with first loop
auto ox::BRep3D::createPlanarFace(
            std::vector<std::span<TopoPoint>> iVertices, 
            std::vector<std::span<std::unique_ptr<ITopoCurve>>> iCurves, 
            const oxm::Plane<double>& iPlane) -> ox::maybe<BRep3D>
{
    if (iVertices.empty() || (iVertices.size() != iCurves.size()))
        return {};
    for (size_t i = 0; i < iVertices.size(); i++)
    {
        const auto& verts = iVertices[i];
        const auto& curves = iCurves[i];
        if ((verts.size() != curves.size()) || (verts.size() < 2))
            return {};
    }

    BRep3D brep = {};

    LoopIdx loop1 = brep.add(Loop{});
    LoopIdx loop2 = brep.add(Loop{});
    LoopIdx currentLoop1 = loop1;
    LoopIdx currentLoop2 = loop2;
    for (size_t id = 0; id < iVertices.size() - 1; id++)
    {
        auto l1 = brep.add(Loop{.next = std::nullopt});
        auto l2 = brep.add(Loop{.next = std::nullopt});
        brep.get(currentLoop1).next = l1;
        brep.get(currentLoop2).next = l2;
        currentLoop1 = l1;
        currentLoop2 = l2;
    }

    FaceIdx faceIdx1 = brep.add(
    { 
        .loop = loop1, 
        .surface = std::make_unique<TopoPlaneSurface>(iPlane)
    });
    // Reverse normal of second plane
    auto reversePlane = oxm::Plane<double>
    {
        .normal = iPlane.normal * -1.0, 
        .xdir = iPlane.xdir, 
        .ydir = iPlane.ydir * (-1.0),
    };
    FaceIdx faceIdx2 = brep.add(
    { 
        .loop = loop2,
        .surface = std::make_unique<TopoPlaneSurface>(reversePlane)
    });


    currentLoop1 = loop1;
    currentLoop2 = loop2;
    for (size_t id = 0; id < iVertices.size(); id++)
    {
        const auto& verts = iVertices[id];
        const auto& curves = iCurves[id];
        std::vector<VertexIdx> vertices;
        for (size_t i = 0; i < verts.size(); i++)
            vertices.push_back(brep.add(Vertex3D{}));
        std::vector<HalfedgeIdx> halfedges;
        for (size_t i = 0; i < 2 * curves.size(); i++)
            halfedges.push_back(brep.add(Halfedge{}));

        size_t halfedgesInLoop = halfedges.size() / 2;
        
        auto& l1 = brep.get(currentLoop1);
        {
            l1.start = halfedges[0];
            l1.face = faceIdx1;
        }
        auto& l2 = brep.get(currentLoop2);
        {
            l2.start = halfedges[halfedgesInLoop];
            l2.face = faceIdx2;
        }


        // Create two halfedge loops:
        // Example: |0| 0>> |1| 1>> |2| 2>> |0|
        //          |0| <<3 |1| <<4 |2| <<5 |0|
        std::vector<EdgeIdx> edges;
        for (size_t i = 0; i < halfedgesInLoop; i++)
        {
            edges.push_back(brep.add(Edge { 
                    .half = halfedges[i],
                    .curve = curves[i]->clone()
                    }));
            brep.get(halfedges[i]) = Halfedge
            {
                .prev = halfedges[(i + halfedgesInLoop - 1) % halfedgesInLoop],
                .next = halfedges[(i + 1) % halfedgesInLoop], 
                .twin = halfedges[halfedgesInLoop + i],
                .start = vertices[i],
                .loop = currentLoop1,
                .edge = edges.back()
            };

        }
        for (size_t i = 0; i < verts.size(); i++)
        {
            brep.get(vertices[i]) = Vertex3D
            {
                .input = halfedges[(i + halfedgesInLoop - 1) % halfedgesInLoop],
                .point = verts[i]
            };
        }

        for (size_t i = halfedgesInLoop; i < halfedges.size(); i++)
        {
            size_t zeroIdx = i - halfedgesInLoop;
            brep.get(halfedges[i]) = Halfedge
            {
                .prev = halfedges[(zeroIdx + 1) % halfedgesInLoop + halfedgesInLoop],
                .next = halfedges[(zeroIdx + halfedgesInLoop - 1) % halfedgesInLoop + halfedgesInLoop], 
                .twin = halfedges[zeroIdx],
                .start = vertices[(zeroIdx + 1) % halfedgesInLoop],
                .loop = currentLoop2,
                .edge = edges[zeroIdx]
            };
        }
        currentLoop1 = l1.next.value_or(LoopIdx{0});
        currentLoop2 = l2.next.value_or(LoopIdx{0});
    }
    return {std::move(brep)};
}

auto ox::BRep3D::extrudeFace(FaceIdx iFace, double iExtension) -> bool
{
    if (iExtension <= 0)
    {
        assert(false && "Extention should be only positive\n");
    }
    auto& face = get(iFace);
    auto surface = dynamic_cast<TopoPlaneSurface*>(face.surface.get());
    if (!surface)
        return false;

    auto extension = iExtension * surface->plane.normal;
    auto trf = oxm::translate(extension);
    
    // Transform face geometry by trf, adding vertices
    face.surface->transform(trf);
    foreach(iFace, 
            [this, &trf](HalfedgeIdx idx)
            {
                auto& halfedge = get(idx);
                auto& edge = get(halfedge.edge);
                auto& vertex = get(halfedge.start);
                halfedge.edge = add(Edge
                        {
                            .half = idx,
                            .curve = edge.curve->transformed(trf)
                        });
                halfedge.start = add(Vertex3D
                        {
                            .input = halfedge.prev,
                            .point = (trf * oxm::grow(vertex.point, 1.0)).shrink<3>()
                        });
                auto& twin = get(halfedge.twin);
                get(twin.start).input = twin.prev; 
                get(twin.edge).half = halfedge.twin;
            });

    // Create back quad faces around loops, after this left only connect each faces to each other
    foreach(iFace, 
            [this](HalfedgeIdx idx)
            {
                auto& baseHE = get(idx);
                auto opositeIdx = baseHE.twin;
                auto& opositeHE = get(opositeIdx);

                VertexIdx vertices[4] = 
                {
                    get(baseHE.next).start,
                    baseHE.start,
                    get(opositeHE.next).start,
                    opositeHE.start,
                };
                oxm::Point3D<double> points[4] = 
                {
                    get(vertices[0]).point,
                    get(vertices[1]).point,
                    get(vertices[2]).point,
                    get(vertices[3]).point
                };
                auto xdir = oxm::norm(points[1] - points[0]);
                // WARNING: ydir is perpendicular to xdir because it is perpendicular extrusion
                auto ydir = oxm::norm(points[2] - points[1]);

                auto loopIdx = add(Loop{ .next = std::nullopt });
                
                HalfedgeIdx halfedges[4] = 
                {
                    add(Halfedge
                    {
                        .twin = idx,
                        .start = vertices[0],
                        .loop = loopIdx,
                        .edge = baseHE.edge
                    }),
                    add(Halfedge
                    {
                        .start = vertices[1],
                        .loop = loopIdx,
                    }),
                    add(Halfedge
                    {
                        .twin = opositeIdx,
                        .start = vertices[2],
                        .loop = loopIdx,
                        .edge = opositeHE.edge
                    }), 
                    add(Halfedge
                    {
                        .start = vertices[3],
                        .loop = loopIdx,
                    }),
                };

                get(get(halfedges[0]).twin).twin = halfedges[0];
                get(get(halfedges[2]).twin).twin = halfedges[2];
                for (uint32_t i = 0; i < 4; i++)
                {
                    get(halfedges[i]).prev = halfedges[(i + 3) % 4];
                    get(halfedges[i]).next = halfedges[(i + 1) % 4];
                }
                get(halfedges[1]).edge = add(Edge
                {
                    .half = halfedges[1],
                    .curve = std::make_unique<TopoLineCurve>(oxm::Line3D<double>
                    {
                        .point = points[1],
                        .dir = ydir
                    }),
                });

                auto faceIdx = add(Face
                {
                    .loop = loopIdx,
                    .surface = std::make_unique<TopoPlaneSurface>(oxm::Plane<double>
                            {
                                .point = points[0],
                                .normal = oxm::norm(oxm::cross(xdir, ydir)),
                                .xdir = xdir,
                                .ydir = ydir
                            }),
                });
                get(loopIdx).face = faceIdx;
                get(loopIdx).start = halfedges[0];
            });

    // Connect back faces to each other
    foreach(iFace, 
            [this](HalfedgeIdx idx)
            {
                auto& current = get(idx);
                auto he1 = get(current.twin).next;
                auto he2 = get(get(current.prev).twin).prev;
                get(he1).twin = he2;
                get(he2).twin = he1;
                get(he2).edge = get(he1).edge;
            });
    
    return true;
}

auto ox::BRep3D::print(LoopIdx idx) const noexcept -> void
{
    auto faceidx = get(idx).face;
    printf("\tLOOPIDX: %i \tFACEIDX: %i", idx.idx, faceidx.idx);
    foreach(idx, [&](const HalfedgeIdx& hidx)
    {
        printf("\tv:%i \th:%i", get(hidx).start.idx, hidx.idx);
    });
    printf("\n");
}
auto ox::BRep3D::print(VertexIdx idx) const noexcept -> void
{
    printf("\tVERTEXIDX: %i\t(%f %f %f)", idx.idx, get(idx).point[0], get(idx).point[1], get(idx).point[2]);
    foreach(get(idx), [&](const Face& f)
    {
        auto faceidx = get(f.loop).face;
        printf("\tf: %i", faceidx.idx);
    });
    printf("\n");
}
auto ox::BRep3D::print(FaceIdx idx) const noexcept -> void
{
}
auto ox::BRep3D::print(HalfedgeIdx idx) const noexcept -> void
{
}
