#pragma once

#define CTORS_REMOVE_ALL(name) \
    name() = delete; \
    name(const name&) = delete; \
    name(name&&) = delete; \
    auto operator=(const name&) -> name& = delete; \
    auto operator=(name&&) -> name& = delete;

#define CTORS_DEFAULT_ALL(name) \
    explicit name() = default; \
    explicit name(const name&) = default; \
    explicit name(name&&) = default; \
    auto operator=(const name&) -> name& = default; \
    auto operator=(name&&) -> name& = default;

#define CTORS_COPY_DELETE(name) \
    explicit name(const name&) = delete; \
    auto operator=(const name&) -> name& = delete;

#define CTORS_MOVE_DELETE(name) \
    explicit name(name&&) = delete; \
    auto operator=(name&&) -> name& = delete;

#define CTORS_COPY_DEFAULT(name) \
    explicit name(const name&) = delete; \
    auto operator=(const name&) -> name& = delete;

#define CTORS_MOVE_DEFAULT(name) \
    explicit name(name&&) = default; \
    auto operator=(name&&) -> name& = default;

#define CTORS_MOVE_AUTO_6(name, p1, p2, p3, p4, p5, p6) \
explicit name(name&& other) noexcept : \
        p1{other.p1}, \
        p2{other.p2}, \
        p3{other.p3}, \
        p4{other.p4}, \
        p5{other.p5}, \
        p6{other.p6}  \
{ \
    other.p1 = {}; \
    other.p2 = {}; \
    other.p3 = {}; \
    other.p4 = {}; \
    other.p5 = {}; \
    other.p6 = {}; \
} \
auto operator=(name&& other) noexcept \
{ \
    destroy(); \
    p1 = other.p1; \
    p2 = other.p2; \
    p3 = other.p3; \
    p4 = other.p4; \
    p5 = other.p5; \
    p6 = other.p6; \
    other.p1 = {}; \
    other.p2 = {}; \
    other.p3 = {}; \
    other.p4 = {}; \
    other.p5 = {}; \
    other.p6 = {}; \
}

namespace ox
{
#ifdef OX_RENDER_DEBUG
    constexpr bool IS_RENDER_DEBUG = true;
#else
    constexpr bool IS_RENDER_DEBUG = false;
#endif

#ifdef __linux__
#define OX_LINUX_OS
    constexpr bool IS_LINUX_OS = true;
#else 
    constexpr bool IS_LINUX_OS = false;
#endif
}
