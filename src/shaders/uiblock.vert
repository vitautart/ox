#version 450
#extension GL_ARB_separate_shader_objects : enable

layout (location = 0) in vec2 pos;
layout (location = 1) in vec2 uv;
layout (location = 2) in uint col;

layout (location = 0) out vec4 out_color;
layout (location = 1) out vec2 out_uv;

layout (push_constant) uniform Viewport { vec2 halfsize; } viewport;

void main()
{
    gl_Position = vec4((pos.x - viewport.halfsize.x)/ viewport.halfsize.x, (pos.y - viewport.halfsize.y)/ (viewport.halfsize.y), 0, 1);
    out_color = unpackUnorm4x8(col);
    out_uv = uv;
}
