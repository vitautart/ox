#pragma once

#include <render/rendervertex.hpp>
#include <render/rendercommon.hpp>
#include <render/render.hpp>
#include <render/res/resbuffers.hpp>

#include <vector>
#include <vulkan/vulkan_core.h>

namespace ox
{
    struct BufferShape2DBase
    {
    public:
        CTORS_DEFAULT_ALL(BufferShape2DBase);
        explicit BufferShape2DBase(const Render* iRender) : mRender(iRender) {};
        auto destroy() noexcept -> void;
        auto clear() noexcept -> void;
        auto load(VkCommandBuffer iCommandBuffer) -> bool;
        auto buffer() const noexcept -> const VkBuffer&;
        auto size() const noexcept -> oxm::u32;

    protected:
        std::vector<RenderVertexUI> mVertices;
        CPUtoGPUBuffer mVertexBuffers[2] = {};
        bool mShouldLoad[2] = {false, false};
        oxm::u32 mVerticesCounts[2] = {};
        oxm::u8 mCurrentBucket = 0;
        oxm::u8 mNextBucket = 0;
    protected: // non-owned objects
        const Render* mRender = nullptr;
    };

    class BufferShape2DSolid : public BufferShape2DBase
    {
    public:
        CTORS_DEFAULT_ALL(BufferShape2DSolid);
        explicit BufferShape2DSolid(const Render* iRender) : BufferShape2DBase(iRender) {};
        auto add(const oxm::vf2& topleft,  const oxm::vf2& size, const oxm::vu4& color) -> void;
        // In case you will add some new members here and not in base class, 
        // take a look to DESTROY method, and override it if needed.
    };

    class BufferShape2DWire : public BufferShape2DBase
    {
    public:
        CTORS_DEFAULT_ALL(BufferShape2DWire);
        explicit BufferShape2DWire(const Render* iRender) : BufferShape2DBase(iRender) {};
        auto add(const oxm::vf2& topleft,  const oxm::vf2& size, const oxm::vu4& color) -> void;
        // In case you will add some new members here and not in base class, 
        // take a look to DESTROY method, and override it if needed.
    };

    class RenderShape2D
    {
    public:
        auto create(const Render* iRender) -> bool;
        auto destroy() -> void;

        auto drawBegin(VkCommandBuffer iCommandBuffer, const VkRect2D& iRect, VkImageView iColorView) const -> void;
        auto draw(VkCommandBuffer iCommandBuffer, const BufferShape2DSolid& iBuffer) const -> void;
        auto draw(VkCommandBuffer iCommandBuffer, const BufferShape2DWire& iBuffer) const -> void;
        auto drawEnd(VkCommandBuffer iCommandBuffer) -> void;
        
    private:
        auto createLayouts() -> bool;
        auto createPipelines() -> bool;

    private:
        bool mInitialized = false;
        VkPipelineLayout mPipelineLayout = VK_NULL_HANDLE;
        VkPipeline mFillPipeline = VK_NULL_HANDLE;
        VkPipeline mBorderPipeline = VK_NULL_HANDLE;

    private: // non owning objects
        const Render* mRender = nullptr;
    };
}
