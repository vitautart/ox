#version 450
#extension GL_ARB_separate_shader_objects : enable

layout (location = 0) in vec3 pos;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec3 col;

layout (location = 0) out vec3 out_color;
layout (location = 1) out vec3 out_world_normal;

struct UnifiedCamera
{
    mat4x4 view_proj;
    mat4x4 view;
    mat4x4 proj;
    mat4x4 view_inv;
};

layout (set = 0, binding = 0) uniform Camera { UnifiedCamera data; } camera;
layout (set = 0, binding = 1) uniform Model { mat4x4 trf; } model;

void main()
{
    //vec4 world_pos =  model.trf * vec4(pos, 1);
    //gl_Position = camera.data.view_proj * world_pos;
    vec4 world_normal = model.trf * vec4(normal, 0);
    gl_Position = camera.data.view_proj * model.trf * vec4(pos, 1);
    out_color = col;
    out_world_normal = world_normal.xyz;
}
