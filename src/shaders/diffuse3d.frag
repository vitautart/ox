#version 450
#extension GL_ARB_separate_shader_objects : enable

layout (location = 0) in vec3 in_color;
layout (location = 1) in vec3 in_world_normal;

layout (location = 0) out vec4 out_color;

struct UnifiedCamera
{
    mat4x4 view_proj;
    mat4x4 view;
    mat4x4 proj;
    mat4x4 view_inv;
};

layout (set = 0, binding = 0) uniform Camera { UnifiedCamera data; } camera;

// light configuration
const float ambient_intensity = 0.9;
const float diffuse_intensity = 0.1;
const vec3 diffuse = vec3(1.0, 1.0, 1.0);

void main()
{
    vec3 ambient = in_color;

    vec3 view_pos = vec3(camera.data.view_inv[3][0], camera.data.view_inv[3][1], camera.data.view_inv[3][2]);
    vec3 light_dir = normalize(view_pos);
    float ndotl = dot(normalize(in_world_normal), light_dir);

    //vec3 ambient_color = clamp(ambient_intensity * ambient, vec3(0, 0, 0), vec3(1, 1, 1));
    vec3 ambient_color = ambient_intensity * ambient;
    //vec3 diffuse_color = clamp(ndotl * diffuse_intensity * diffuse, vec3(0, 0, 0), vec3(1, 1, 1));
    vec3 diffuse_color = ndotl * diffuse_intensity * diffuse;

    out_color = vec4(clamp(ambient_color + diffuse_color, vec3(0, 0, 0), vec3(1, 1, 1)), 1);
}
