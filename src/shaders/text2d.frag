#version 450
#extension GL_ARB_separate_shader_objects : enable

layout (location = 0) in vec4 in_color;
layout (location = 1) in vec2 in_uv;

layout (location = 0) out vec4 out_color;

layout(set = 0, binding = 0) uniform sampler2D font;

void main()
{
    float intensity = textureLod(font, in_uv, 0).r;
    out_color = vec4(in_color.rgb, intensity);
}
