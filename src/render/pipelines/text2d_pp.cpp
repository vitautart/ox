#include <render/pipelines.hpp>
#include <render/rendervertex.hpp>
#include <render/rendercommon.hpp>
#include <array>
#include <vulkan/vulkan_core.h>

auto ox::pipelines::text2d(
            VkDevice device, 
            VkPipelineLayout layout, 
            VkShaderModule vertexShader, 
            VkShaderModule fragmentShader, 
            VkFormat colorFormat,
            VkPipeline* pipeline
            ) -> VkResult
{
    std::array<VkPipelineShaderStageCreateInfo, 2> shaders = 
    {
        VkPipelineShaderStageCreateInfo
        {
            .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO, .pNext = nullptr, .flags = 0,
            .stage = VkShaderStageFlagBits::VK_SHADER_STAGE_VERTEX_BIT,
            .module = vertexShader,
            .pName = "main",
            .pSpecializationInfo = nullptr
        },
        VkPipelineShaderStageCreateInfo
        {
            .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO, .pNext = nullptr, .flags = 0,
            .stage = VkShaderStageFlagBits::VK_SHADER_STAGE_FRAGMENT_BIT,
            .module = fragmentShader,
            .pName = "main",
            .pSpecializationInfo = nullptr
        },
    };

    VkVertexInputBindingDescription vertexBinding = 
    {
        .binding = 0,
        .stride = sizeof(CharVertex),
        .inputRate = VkVertexInputRate::VK_VERTEX_INPUT_RATE_VERTEX
    };

    std::array<VkVertexInputAttributeDescription, 4> vertexAttributes =
    {
        VkVertexInputAttributeDescription
        {
            .location = 0,
            .binding = 0,
            .format = formatof<decltype(CharVertex::pos)>(),
            .offset = offsetof(CharVertex, pos)
        },
        VkVertexInputAttributeDescription
        {
            .location = 1,
            .binding = 0,
            .format = formatof<decltype(CharVertex::uv)>(),
            .offset = offsetof(CharVertex, uv)
        },
        VkVertexInputAttributeDescription
        {
            .location = 2,
            .binding = 0,
            .format = VK_FORMAT_R32_UINT,
            .offset = offsetof(CharVertex, col)
        },
        VkVertexInputAttributeDescription
        {
            .location = 3,
            .binding = 0,
            .format = VK_FORMAT_R32_UINT,
            .offset = offsetof(CharVertex, image)
        }
    };

    VkPipelineVertexInputStateCreateInfo vertexInput = 
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO, .pNext = nullptr, .flags = 0,
        .vertexBindingDescriptionCount = 1,
        .pVertexBindingDescriptions = &vertexBinding,
        .vertexAttributeDescriptionCount = vertexAttributes.size(),
        .pVertexAttributeDescriptions = vertexAttributes.data()
    };

    VkPipelineInputAssemblyStateCreateInfo assemblyInput = 
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO, .pNext = nullptr, .flags = 0,
        .topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
        .primitiveRestartEnable = VK_FALSE
    };

    VkPipelineViewportStateCreateInfo viewportState = 
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO, .pNext = nullptr, .flags = 0,
        .viewportCount = 1,
        .pViewports = nullptr,
        .scissorCount = 1,
        .pScissors = nullptr
    };

    VkPipelineRasterizationStateCreateInfo rasterization = 
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO, .pNext = nullptr, .flags = 0,
        .depthClampEnable = VK_FALSE,
        .rasterizerDiscardEnable = VK_FALSE,
        .polygonMode = VK_POLYGON_MODE_FILL,
        .cullMode = VkCullModeFlagBits::VK_CULL_MODE_NONE,
        .frontFace = VkFrontFace::VK_FRONT_FACE_CLOCKWISE,
        .depthBiasEnable = VK_FALSE,
        .depthBiasConstantFactor = 0,
        .depthBiasClamp = 0,
        .depthBiasSlopeFactor = 0,
        .lineWidth = 1.0f,
    };

    VkPipelineMultisampleStateCreateInfo multisampling = 
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO, .pNext = nullptr, .flags = 0,
        .rasterizationSamples = VkSampleCountFlagBits::VK_SAMPLE_COUNT_1_BIT,
        .sampleShadingEnable = VK_FALSE,
        .minSampleShading = 0,
        .pSampleMask = 0,
        .alphaToCoverageEnable = VK_FALSE,
        .alphaToOneEnable = 0
    };
    VkPipelineDepthStencilStateCreateInfo depthStencil = 
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .depthTestEnable = VK_FALSE,
        .depthWriteEnable = VK_FALSE,
        .depthCompareOp = VK_COMPARE_OP_LESS,
        .depthBoundsTestEnable = VK_FALSE,
        .stencilTestEnable = VK_FALSE,
        .front = {},
        .back = {},
        .minDepthBounds = 0,
        .maxDepthBounds = 0
    };

    // TODO: add color blending
    // rgb   = srcColorBlendFactor * newColor + dstColorBlendFactor * oldColor
    // alpha = srcAlphaBlendFactor * newAlpha + dstAlphaBlendFactor * oldAlpha
    VkPipelineColorBlendAttachmentState colorBlendAttachment = 
    {
        .blendEnable = VK_TRUE,
        .srcColorBlendFactor = VkBlendFactor::VK_BLEND_FACTOR_SRC_ALPHA,
        .dstColorBlendFactor = VkBlendFactor::VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA,
        .colorBlendOp = VkBlendOp::VK_BLEND_OP_ADD,
        .srcAlphaBlendFactor = VkBlendFactor::VK_BLEND_FACTOR_ONE,
        .dstAlphaBlendFactor = VkBlendFactor::VK_BLEND_FACTOR_ZERO,
        .alphaBlendOp = VkBlendOp::VK_BLEND_OP_ADD,
        .colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | 
                VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT
    };

    VkPipelineColorBlendStateCreateInfo colorBlend = 
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .logicOpEnable = VK_FALSE,
        .logicOp = VK_LOGIC_OP_COPY,
        .attachmentCount = 1,
        .pAttachments = &colorBlendAttachment,
        .blendConstants = {0, 0, 0, 0}
    };

    std::array<VkDynamicState, 2> dynamicStates = 
    {
        VkDynamicState::VK_DYNAMIC_STATE_VIEWPORT, 
        VkDynamicState::VK_DYNAMIC_STATE_SCISSOR
    };
    VkPipelineDynamicStateCreateInfo dynamicStateInfo = 
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO, .pNext = nullptr, .flags = 0,
        .dynamicStateCount = dynamicStates.size(),
        .pDynamicStates = dynamicStates.data()
    };

    VkFormat colorFormats[] = { colorFormat };
    VkPipelineRenderingCreateInfo renderingInfo = 
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_RENDERING_CREATE_INFO,
        .pNext = nullptr,
        .viewMask = 0,
        .colorAttachmentCount = sizeof(colorFormats) / sizeof(colorFormats[0]),
        .pColorAttachmentFormats = colorFormats,
        .depthAttachmentFormat = VK_FORMAT_UNDEFINED,
        .stencilAttachmentFormat = VK_FORMAT_UNDEFINED,
    };

    VkGraphicsPipelineCreateInfo pipelineInfo = 
    {
        .sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO, 
        .pNext = &renderingInfo, 
        .flags = 0,
        .stageCount = shaders.size(),
        .pStages = shaders.data(),
        .pVertexInputState = &vertexInput,
        .pInputAssemblyState = &assemblyInput,
        .pTessellationState = nullptr,
        .pViewportState = &viewportState, 
        .pRasterizationState = &rasterization,
        .pMultisampleState = &multisampling,
        .pDepthStencilState = &depthStencil,
        .pColorBlendState = &colorBlend,
        .pDynamicState = &dynamicStateInfo,
        .layout = layout,
        .renderPass = nullptr,
        .subpass = 0,
        .basePipelineHandle = VK_NULL_HANDLE,
        .basePipelineIndex = 0
    };
    return vkCreateGraphicsPipelines(device, VK_NULL_HANDLE, 1, &pipelineInfo, nullptr, pipeline);
}
