#pragma once

#include <span>
#include <string_view>
#include <vector>
#include <string>
#include <functional>
#include <memory>

#include <oxmath.hpp>
#include <sumtypes.hpp>

#include <render/rendertext2d.hpp>
#include <render/rendershape2d.hpp>

namespace ox::ui
{

struct Style 
{
    oxm::vu4 bgColorDefault;
    oxm::vu4 bgColorHighlight;
    oxm::vu4 bgColorDisabled;
    oxm::vu4 bgColorInputField;
    oxm::vu4 borderColorDefault;
    oxm::vu4 borderColorHighlight;
    oxm::vu4 borderColorDisabled;
    oxm::vu4 textColorDefault;
    oxm::vu4 textColorHighlight;
    oxm::vu4 textColorDisabled;
    oxm::vu4 textColorSelected;
    oxm::vu4 textBgColorSelected;
};

struct Rect
{
    oxm::vf2 pos;
    oxm::vf2 size;
    auto inside(const oxm::vf2& iPos) const noexcept -> bool;
    auto vkrect() const noexcept -> VkRect2D;
};

struct Element
{
    struct ClickRes
    {
        bool shouldUpdate;
        Element* elemFocused;
    };
    explicit Element(const Style* iStyle, bool iVisible) : mStyle(iStyle), mVisible(iVisible) {}
    virtual ~Element(){}
    virtual auto fillRenderData(ox::BufferShape2DSolid&, ox::BufferShape2DWire&, ox::BufferText2D&) -> void = 0;
    // TODO: maybe add distinct enum to signal what ecactly geometry need to update
    // wireframe, solid, text, etc
    // Return true if need update rendering geometry
    virtual auto onHover(const oxm::vf2& pos) -> bool { return false; }
    virtual auto onHoverEnd() -> void {}

    // TODO: maybe add distinct enum to signal what ecactly geometry need to update
    // wireframe, solid, text, etc
    // Return true if need update rendering geometry
    virtual auto onClick(const oxm::vf2& pos) -> ClickRes
    {
        return { false, nullptr }; 
    }

    virtual auto setRect(const Rect& r) -> void { mRect = r; };
    
    auto getRect() const noexcept -> const Rect& { return mRect; }
    auto isVisible() const noexcept -> bool { return mVisible; }
    auto setVisible(bool visible) noexcept -> void { mVisible = visible; }
    auto setStyle(const Style* iStyle) noexcept -> void { mStyle = iStyle; };

protected:
    Rect mRect; // relative to canvas origin
    const Style* mStyle;
    bool mVisible;
};

class Button final: public Element
{
public:
    Button(const std::u32string& iText, const Style* iStyle)
        : mText(iText), mHovered(false), Element(iStyle, true), mClickFn([]{}) {}
    virtual ~Button() {}
    auto fillRenderData(ox::BufferShape2DSolid& sb, ox::BufferShape2DWire& wb, 
            ox::BufferText2D& tb) -> void override;
    auto onHover(const oxm::vf2& pos) -> bool override 
    { 
        bool old = mHovered; 
        mHovered = true; 
        return old != mHovered; 
    }
    auto onHoverEnd() -> void override { mHovered = false; }
    auto onClick(const oxm::vf2& pos) -> ClickRes override 
    { 
        mClickFn();
        return { true, nullptr }; 
    }
    auto setClickHandler(const std::function<void()>& clickfn) -> void { mClickFn = clickfn; }

private:
    std::u32string mText;
    bool mHovered;
    std::function<void()> mClickFn;
};

class InputElement
{
public:
    ~InputElement() noexcept {};
    virtual auto setText(const std::u32string& text) -> void = 0;
    virtual auto getText() const -> const std::u32string_view = 0;
    virtual auto setCursorBeforeChar(size_t charpos) -> void = 0;
    virtual auto removeBack() -> void = 0;
    virtual auto removeForward() -> void = 0;
    virtual auto moveCursorBack() -> void = 0;
    virtual auto moveCursorForward() -> void = 0;
    virtual auto inputCharacter(char32_t c) -> void = 0;
    virtual auto setFocused(bool focused) -> void = 0;
};

class InputText final: public Element, public InputElement
{
public:
    InputText(const Style* iStyle): 
        Element(iStyle, true),
        mText(U"0.0111"),
        mCursorText(0),
        mCursorStart(0),
        //mCursorEnd(mText.size()),
        mCachedFont(nullptr),
        mHovered(false),
        mFocused(false)
    {}
    virtual ~InputText() {}
    auto fillRenderData(ox::BufferShape2DSolid& sb, ox::BufferShape2DWire& wb, 
            ox::BufferText2D& tb) -> void override;
    auto onHover(const oxm::vf2& pos) -> bool override
    { 
        bool old = mHovered; 
        mHovered = true; 
        return old != mHovered; 
    }
    auto onHoverEnd() -> void override { mHovered = false; }
    auto onClick(const oxm::vf2& pos) -> ClickRes override;
public:
    auto setText(const std::u32string& text) -> void override { mText = text; mCursorText = 0; }
    auto getText() const -> const std::u32string_view override { return mText; }
    auto setCursorBeforeChar(size_t charpos) -> void override { /*TODO*/ }
    auto removeBack() -> void override;
    auto removeForward() -> void override { /*TODO*/ }
    auto moveCursorBack() -> void override { if (mCursorText > 0) mCursorText--; }
    auto moveCursorForward() -> void override { if (mCursorText < mText.size()) mCursorText++; }
    auto inputCharacter(char32_t c) -> void override;
    auto setFocused(bool focused) -> void override { mFocused = focused; }
private:
    auto calculateTextStartX() const noexcept -> float;
    auto calculateTextMarginX() const noexcept -> float;
private:
    std::u32string mText;
    int mCursorText;
    int mCursorStart;
    const ox::BitmapFont* mCachedFont;
    bool mHovered;
    bool mFocused;
};


class SmallInput final: public Element
{
public:
    SmallInput(const Style* iStyle);
    virtual ~SmallInput() {}
    auto fillRenderData(ox::BufferShape2DSolid& sb, ox::BufferShape2DWire& wb, 
            ox::BufferText2D& tb) -> void override;
    auto onHover(const oxm::vf2& pos) -> bool override;
    auto onHoverEnd() -> void override;
    auto onClick(const oxm::vf2& pos) -> ClickRes override;
    auto setRect(const Rect& r) -> void override;
public:
    auto setText(const std::u32string& text) -> void { mInputText.setText(text); }
    auto getText() const -> const std::u32string_view { return mInputText.getText(); }
private:
    bool mHovered;
    Button mCopyButton;
    Button mPasteButton;
    InputText mInputText;
};

class Label final: public Element
{
public:
    Label(const std::u32string& iText, const Style* iStyle)
        : mText(iText), Element(iStyle, true) {}
    virtual ~Label() {}
    std::u32string mText;
    auto fillRenderData(ox::BufferShape2DSolid& sb, ox::BufferShape2DWire& wb, 
            ox::BufferText2D& tb) -> void override;
};

class LinearCanvas
{
public:
    LinearCanvas(const Rect& iRect, const ox::Render* iRender, const Style* iStyle);
    virtual ~LinearCanvas() noexcept;
    auto scheduleUpdate() -> void { mShouldUpdate = true; }
    auto add(std::unique_ptr<Element> element) -> size_t;
    auto add(std::span<std::unique_ptr<Element>> elements) -> size_t;
    auto removeBy(Element* element) -> bool;
    auto removeBy(size_t id) -> bool;
    auto resize(const oxm::vf2& size) -> void;
    auto move(const oxm::vf2& windowPos) -> void;
    auto load(VkCommandBuffer iCommandBuffer) -> bool;
    // windowPos is relative to window upper-left corner position
    virtual auto onHover(const oxm::vf2& windowPos) -> void;
    virtual auto onHoverEnd() -> void;
    // windowPos is relative to window upper-left corner position
    virtual auto onClick(const oxm::vf2& windowPos) -> Element::ClickRes;
    virtual auto onScroll(oxm::f32 scroll) -> void {};
public:
    auto getRect() const noexcept -> const Rect& { return mRect; }
    auto getBufferSolid() const noexcept -> const ox::BufferShape2DSolid& { return mShapeBufferSolid; }
    auto getBufferWire() const noexcept -> const ox::BufferShape2DWire& { return mShapeBufferWire; }
    auto getBufferText() const noexcept -> const ox::BufferText2D& { return mTextBuffer; }
private:
    virtual auto updateGeometry() -> void = 0;
protected:
    std::vector<std::unique_ptr<Element>> mElements;
    Rect mRect; // relative to window origin
    Element* mCurrentHovered;
    const Style* mStyle;
    bool mShouldUpdate;
    ox::BufferShape2DSolid mShapeBufferSolid;
    ox::BufferShape2DWire mShapeBufferWire;
    ox::BufferText2D mTextBuffer;
};

class VStretchCanvas final : public LinearCanvas
{
public:
    VStretchCanvas(const Rect& iRect, float iMargin, const ox::Render* iRender, const Style* iStyle);
private:
    auto updateGeometry() -> void override;
private:
    float mMargin;
};

class VScrolCanvas final : public LinearCanvas
{
public:
    VScrolCanvas(const Rect& iRect, float iMargin, float iElemHeight, 
            const ox::Render* iRender, const Style* iStyle);
    auto onHover(const oxm::vf2& windowPos) -> void override;
    auto onClick(const oxm::vf2& windowPos) -> Element::ClickRes override;
    auto onScroll(oxm::f32 scroll) -> void override;
private:
    auto updateGeometry() -> void override;
    auto scrollBy(int scroll) -> void;
private:
    float mMargin;
    float mElemHeight;
    int mTopOffset;
    std::array<Button, 2> mScrollButtons;
};

}
