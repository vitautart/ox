#pragma once 

#include <cinttypes>
#include <vector>
#include <span>

#include <sumtypes.hpp>

namespace ox
{
template <typename T>
class permavector
{
public:

    auto add(const T& data) -> uint32_t
    {
        if (mFreelist.empty())
        {
            mData.emplace_back(data);
            return mData.size() - 1;
        }
        else 
        {
            uint32_t idx = mFreelist.back();
            mFreelist.pop_back();
            mData[idx] = data;
            return idx;
        }
    }

    auto add(T&& data) -> uint32_t
    {
        if (mFreelist.empty())
        {
            mData.emplace_back(std::move(data));
            return mData.size() - 1;
        }
        else 
        {
            uint32_t idx = mFreelist.back();
            mFreelist.pop_back();
            mData[idx] = std::move(data);
            return idx;
        }
    }

    auto add() -> uint32_t
    {
        if (mFreelist.empty())
        {
            mData.emplace_back();
            return mData.size() - 1;
        }
        else 
        {
            uint32_t idx = mFreelist.back();
            mFreelist.pop_back();
            //mData[idx] = data;
            return idx;
        }
    }

    auto remove(uint32_t idx) -> bool
    {
        if (mData.empty() || idx > (mData.size() - 1))
            return false;

        if (idx < (mData.size() - 1))
        {
            mFreelist.push_back(idx);
            mData[idx].isValid = false;
        }
        else // idx == (mData.size() - 1)
        {
            mData.pop_back();
        }

        return true;
    }

    auto data() const noexcept -> const std::span<const maybe<T>> { return mData; }

    auto operator[](uint32_t idx) noexcept -> maybe<T>& 
    {
#ifdef OX_TABLE_IDX_CHECK
        if (idx >= mData.size())
            printf("ERROR: table size is %lu but idx is %u", mData.size(), idx);
        else if (!mData[idx].isValid)
            printf("ERROR: data in the table is not valid at idx %u", idx);
#endif
        return mData[idx]; 
    }

    auto operator[](uint32_t idx) const noexcept -> const maybe<T>& 
    {
#ifdef OX_TABLE_IDX_CHECK
        if (idx >= mData.size())
            printf("ERROR: table size is %lu but idx is %u", mData.size(), idx);
        else if (!mData[idx].isValid)
            printf("ERROR: data in the table is not valid at idx %u", idx);
#endif
        return mData[idx]; 
    }

private:
    std::vector<maybe<T>> mData;
    std::vector<uint32_t> mFreelist;
};

template <typename T>
class staticvector
{
public:
    staticvector() = default;
    staticvector(size_t capacity): mSize(0), mCapacity(capacity) { alloc(); }
    staticvector(const staticvector& v) { copy(v); }
    staticvector(staticvector&& v) noexcept : 
        mData(v.mData), mSize(v.mSize), mCapacity(v.mCapacity) 
    { 
        v.mData = nullptr; 
        v.mSize = 0; 
        v.mCapacity = 0; 
    }
    auto operator=(const staticvector& v) 
    { 
        clear_reserve(v.mCapacity);
        copy(v);
    } 
    auto operator=(staticvector&& v) noexcept 
    { 
        delete[] mData;
        mData = v.mData;
        mSize = v.mSize;
        mCapacity = v.mCapacity;
        v.mData = nullptr;
        v.mSize = 0;
        v.mCapacity = 0;
    }
    ~staticvector() noexcept { delete[] mData; }
    auto clear() noexcept -> void { mSize = 0; }
    auto clear_reserve(size_t capacity) -> void
    {
        mSize = 0;
        if (mCapacity < capacity)
        {
            delete[] mData;
            mCapacity = capacity;
            alloc();
        }
    }
    auto push_back(T&& e) noexcept -> void
    {
        mData[mSize] = std::forward<T>(e);
        mSize++;
    }
    auto pop_back() noexcept -> void { mSize--; }
    auto operator[](size_t i) const noexcept -> const T& { return mData[i]; }
    auto operator[](size_t i) noexcept -> T& { return mData[i]; }
    auto size() const noexcept -> size_t { return mSize; }
    auto capacity() const noexcept -> size_t { return mCapacity; }
    auto data() noexcept -> T* { return mData; }
    auto data() const noexcept -> const T* { return mData; }
    auto back() const noexcept -> const T& { return mData[mSize - 1]; }
    auto back() noexcept -> T& { return mData[mSize - 1]; }
    auto empty() const noexcept -> bool { return mSize == 0; }
private:
    template<typename T1>
    auto copy(const staticvector<T1>& v) -> void 
        requires std::is_trivially_copyable<T1>::value
    {
        mSize = v.mSize; 
        mCapacity = v.mCapacity;
        memcpy(mData, v.mData, mCapacity * sizeof(T));
    }
    template<typename T1>
    auto copy(const staticvector<T1>& v) -> void 
        requires (!std::is_trivially_copyable<T1>::value)
    {
        mSize = v.mSize; 
        mCapacity = v.mCapacity;
        for (size_t i = 0; i < mSize; i++) mData[i] = v.mData[i];
    }
    auto alloc() -> void 
    {
        mData = new T[mCapacity];
        if (!mData)
        {
            mSize = 0;
            mCapacity = 0;
            throw std::bad_alloc();
        }
    }

private:
    T* mData = nullptr;
    size_t mSize = 0;
    size_t mCapacity = 0;
};

}
