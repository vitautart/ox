#include <definitions.hpp>
#include <render/render.hpp>
#include <render/rendercommon.hpp>
#include <render/rendervertex.hpp>
#include <fileutils.hpp>
#include <generalutils.hpp>

#include <assert.h>
#include <set>
#include <stdio.h>

#include <algorithm>
#include <vulkan/vulkan_core.h>

#include "GLFW/glfw3.h"
#include "render/res/resbuffers.hpp"

namespace 
{
    static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(VkDebugUtilsMessageSeverityFlagBitsEXT pSeverity, 
            VkDebugUtilsMessageTypeFlagsEXT pType, 
            const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData, 
            void* pUserData) noexcept
    {
        printf("[VALIDATION] %s\n", pCallbackData->pMessage);
        if (pSeverity & VkDebugUtilsMessageSeverityFlagBitsEXT::VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT)
            assert(false);
        return VK_FALSE;
    }

    static auto destroyDebugger(VkInstance iInstance, VkDebugUtilsMessengerEXT iDebugger) -> void
    {
        auto vkDestroyDebugUtilsMessengerEXT =(PFN_vkDestroyDebugUtilsMessengerEXT) 
            vkGetInstanceProcAddr(iInstance, "vkDestroyDebugUtilsMessengerEXT");
        assert(vkDestroyDebugUtilsMessengerEXT);
        vkDestroyDebugUtilsMessengerEXT(iInstance, iDebugger, nullptr);
    }

    auto constexpr static getInstanceExtensions() noexcept
    {
        return std::array<const char*, 3>
        {
            "VK_KHR_surface", 
            "VK_KHR_xlib_surface",
            VK_EXT_DEBUG_UTILS_EXTENSION_NAME
        };
    }

    auto constexpr static getDeviceExtensions() noexcept
    {
        return  std::array<const char*, 1> 
        {
            VK_KHR_SWAPCHAIN_EXTENSION_NAME,
        };
    }

    auto constexpr static getLayers() noexcept
    {
        constexpr size_t size = ox::IS_RENDER_DEBUG ? 1 : 0;
        std::array<const char*, size> data;

        if constexpr (ox::IS_RENDER_DEBUG)
            data[0] = "VK_LAYER_KHRONOS_validation";
        return data; 
    }
    auto constexpr static getDepthFormats() noexcept
    {
        return std::array<VkFormat, 3>
        {
            VkFormat::VK_FORMAT_D32_SFLOAT,
            VkFormat::VK_FORMAT_D32_SFLOAT_S8_UINT,
            VkFormat::VK_FORMAT_D24_UNORM_S8_UINT
        };
    }
}

ox::Render::Render(const RenderCreateInfo& info, bool& result) noexcept
{
    result = false;
    if (!glfwInit()) return;
    if(!createInstance()) return;
    if(!createDebugger()) return;
    if(!choosePhysicalDevice()) return;
    if(!chooseQueueFamilies()) return;
    if(!createDevice()) return;

    getQueues();
    printDeviceInfo();

    if(!createAllocator()) return;
    if(!loadResources()) return;

    mSamplesCount = info.samplesCount;

    result = true;
}

ox::Render::~Render() noexcept
{
    mCubeMesh.buffer.destroy(mAllocator);
    mCubeMesh = {};

    for (size_t i = 0; i < static_cast<size_t>(ShaderResource::SIZE); i++)
    {
        auto shader = mShaders[i];
        if (shader != VK_NULL_HANDLE)
            vkDestroyShaderModule(mDevice, shader, nullptr);
    }

    for (size_t i = 0; i < static_cast<size_t>(FontResource::SIZE); i++)
        mFonts[i].destroy(mDevice, mAllocator);

    if (mAllocator)
        vmaDestroyAllocator(mAllocator);

    if (mDevice)
        vkDestroyDevice(mDevice, nullptr);

    if (mDebugger)
        destroyDebugger(mInstance, mDebugger);

    if (mInstance)
        vkDestroyInstance(mInstance, nullptr);
}

static inline auto createSimpleCube(std::vector<ox::RenderVertex3D>& storage, 
        oxm::vf3 xcolor, oxm::vf3 ycolor, oxm::vf3 zcolor) -> void
{
    oxm::vf3 pos[8] = 
    {
        { 0.5,  0.5,  0.5}, // 0
        { 0.5,  0.5, -0.5}, // 1
        { 0.5, -0.5,  0.5}, // 2
        {-0.5,  0.5,  0.5}, // 3
        { 0.5, -0.5, -0.5}, // 4
        {-0.5, -0.5,  0.5}, // 5
        {-0.5,  0.5, -0.5}, // 6
        {-0.5, -0.5, -0.5}  // 7 
    };

    oxm::vf3 norm[6] = 
    {
        { 1,  0,  0}, // 0
        { 0,  1,  0}, // 1
        { 0,  0,  1}, // 2
        {-1,  0,  0}, // 3
        { 0, -1,  0}, // 4
        { 0,  0, -1}, // 5
    };

    // + z
    storage.push_back({ .pos = pos[0], .normal = norm[2], .col = zcolor });
    storage.push_back({ .pos = pos[3], .normal = norm[2], .col = zcolor });
    storage.push_back({ .pos = pos[5], .normal = norm[2], .col = zcolor });
    storage.push_back({ .pos = pos[5], .normal = norm[2], .col = zcolor });
    storage.push_back({ .pos = pos[2], .normal = norm[2], .col = zcolor });
    storage.push_back({ .pos = pos[0], .normal = norm[2], .col = zcolor });

    // - z
    storage.push_back({ .pos = pos[7], .normal = norm[5], .col = zcolor });
    storage.push_back({ .pos = pos[6], .normal = norm[5], .col = zcolor });
    storage.push_back({ .pos = pos[1], .normal = norm[5], .col = zcolor });
    storage.push_back({ .pos = pos[1], .normal = norm[5], .col = zcolor });
    storage.push_back({ .pos = pos[4], .normal = norm[5], .col = zcolor });
    storage.push_back({ .pos = pos[7], .normal = norm[5], .col = zcolor });


    // + x
    storage.push_back({ .pos = pos[0], .normal = norm[0], .col = xcolor });
    storage.push_back({ .pos = pos[2], .normal = norm[0], .col = xcolor });
    storage.push_back({ .pos = pos[4], .normal = norm[0], .col = xcolor });
    storage.push_back({ .pos = pos[4], .normal = norm[0], .col = xcolor });
    storage.push_back({ .pos = pos[1], .normal = norm[0], .col = xcolor });
    storage.push_back({ .pos = pos[0], .normal = norm[0], .col = xcolor });

    // - x
    storage.push_back({ .pos = pos[7], .normal = norm[3], .col = xcolor });
    storage.push_back({ .pos = pos[5], .normal = norm[3], .col = xcolor });
    storage.push_back({ .pos = pos[3], .normal = norm[3], .col = xcolor });
    storage.push_back({ .pos = pos[3], .normal = norm[3], .col = xcolor });
    storage.push_back({ .pos = pos[6], .normal = norm[3], .col = xcolor });
    storage.push_back({ .pos = pos[7], .normal = norm[3], .col = xcolor });

    // + y
    storage.push_back({ .pos = pos[0], .normal = norm[1], .col = ycolor });
    storage.push_back({ .pos = pos[1], .normal = norm[1], .col = ycolor });
    storage.push_back({ .pos = pos[6], .normal = norm[1], .col = ycolor });
    storage.push_back({ .pos = pos[6], .normal = norm[1], .col = ycolor });
    storage.push_back({ .pos = pos[3], .normal = norm[1], .col = ycolor });
    storage.push_back({ .pos = pos[0], .normal = norm[1], .col = ycolor });

    // + y
    storage.push_back({ .pos = pos[7], .normal = norm[4], .col = ycolor });
    storage.push_back({ .pos = pos[4], .normal = norm[4], .col = ycolor });
    storage.push_back({ .pos = pos[2], .normal = norm[4], .col = ycolor });
    storage.push_back({ .pos = pos[2], .normal = norm[4], .col = ycolor });
    storage.push_back({ .pos = pos[5], .normal = norm[4], .col = ycolor });
    storage.push_back({ .pos = pos[7], .normal = norm[4], .col = ycolor });
}

auto ox::Render::loadResources() -> bool
{
    const char* shaderPaths[static_cast<size_t>(ox::ShaderResource::SIZE)] = {};
    const char* fontPaths[static_cast<size_t>(ox::FontResource::SIZE)] = {};
    int fontSizes[static_cast<size_t>(ox::FontResource::SIZE)] = {};

    shaderPaths[static_cast<size_t>(ox::ShaderResource::DIFFUSE3D_VERT)] = "shaders/diffuse3d_vert.spv";
    shaderPaths[static_cast<size_t>(ox::ShaderResource::DIFFUSE3D_FRAG)] = "shaders/diffuse3d_frag.spv";
    shaderPaths[static_cast<size_t>(ox::ShaderResource::SIMPLE3D_VERT)] = "shaders/simple3d_vert.spv";
    shaderPaths[static_cast<size_t>(ox::ShaderResource::SIMPLE3D_FRAG)] = "shaders/simple3d_frag.spv";
    shaderPaths[static_cast<size_t>(ox::ShaderResource::UIBLOCK_VERT)] = "shaders/uiblock_vert.spv";
    shaderPaths[static_cast<size_t>(ox::ShaderResource::UIBLOCK_FRAG)] = "shaders/uiblock_frag.spv";
    shaderPaths[static_cast<size_t>(ox::ShaderResource::WIREFRAME3D_VERT)] = "shaders/wireframe3d_vert.spv";
    shaderPaths[static_cast<size_t>(ox::ShaderResource::TEXT2D_VERT)] = "shaders/text2d_vert.spv";
    shaderPaths[static_cast<size_t>(ox::ShaderResource::TEXT2D_FRAG)] = "shaders/text2d_frag.spv";

    fontPaths[static_cast<size_t>(ox::FontResource::MAIN_FONT)] = "fonts/iosevka-fixed-regular.ttf";
    fontSizes[static_cast<size_t>(ox::FontResource::MAIN_FONT)] = 20;

    for (size_t i = 0; i < static_cast<size_t>(ox::ShaderResource::SIZE); i++)
    {
        auto filepath = shaderPaths[i];
        std::vector<char> data;
        auto result = readFile(filepath, true, data);
        if (!result)
        {
            printf("[ERROR] Couldn't read file: %s\n", filepath);
            return false;
        }

        VkShaderModuleCreateInfo info = 
        {
            .sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
            .pNext = nullptr,
            .flags = 0,
            .codeSize = data.size(),
            .pCode = reinterpret_cast<const uint32_t*>(data.data())
        };
        VkShaderModule shader;
        auto resultvk = vkCreateShaderModule(mDevice, &info, nullptr, &shader);

        if (resultvk != VK_SUCCESS)
        {
            printf("[ERROR] Couldn't create shader module from file: %s error code: %i\n", filepath, resultvk);
            return false;
        }
        mShaders[i] = shader;
    }

/////////////////////////////////////////////////////////////////////////////////////////
    std::vector<ox::RenderVertex3D> cubeMesh;
    createSimpleCube(cubeMesh, {0.8f, 0.2f, 0.2f}, {0.2f, 0.8f, 0.2f}, {0.2f, 0.2f, 0.8f});
    mCubeMesh.vertcount = cubeMesh.size();
    size_t cubeMeshByteCount = sizeof(ox::RenderVertex3D) * cubeMesh.size();
    VkBufferCreateInfo cubeInfo = 
    {
        .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .size = cubeMeshByteCount,
        .usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
        .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
        .queueFamilyIndexCount = 0,
        .pQueueFamilyIndices = 0,
    };

    ox::CPUBuffer cubeBuffer = {};
    auto result = cubeBuffer.create(mAllocator, cubeInfo);
    ox::defer deleter1([&cubeBuffer, a = mAllocator]() mutable { cubeBuffer.destroy(a); });
    if (result != VK_SUCCESS)
    {
        printf("[ERROR] CubeMesh creation failed: %i\n", result);
        return false;
    }

    cubeInfo.usage = VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;
    result = mCubeMesh.buffer.create(mAllocator, cubeInfo);
    if (result != VK_SUCCESS)
    {
        printf("[ERROR] CubeMesh creation failed: %i\n", result);
        return false;
    }
    cubeBuffer.write(cubeMesh.data(), cubeMeshByteCount, 0);
/////////////////////////////////////////////////////////////////////////////////////////
    
    VkCommandPool commandPool = VK_NULL_HANDLE;
    VkCommandPoolCreateInfo poolInfo = 
    {
        .sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
        .pNext = nullptr,
        .flags = VK_COMMAND_POOL_CREATE_TRANSIENT_BIT,
        .queueFamilyIndex = mGfxCompQueue.family
    };
    result = vkCreateCommandPool(mDevice, &poolInfo, nullptr, &commandPool);
    CHECK_AND_RETURN(result, "[ERROR] Command buffer pool creation failed: %i\n");
    ox::defer ds1([d = this->mDevice, commandPool]{vkDestroyCommandPool(d, commandPool, nullptr);});

        
    VkCommandBufferAllocateInfo info = 
    {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
        .pNext = nullptr,
        .commandPool = commandPool,
        .level = VkCommandBufferLevel::VK_COMMAND_BUFFER_LEVEL_PRIMARY,
        .commandBufferCount = 1
    };

    VkCommandBuffer cb = VK_NULL_HANDLE;
    result = vkAllocateCommandBuffers(mDevice, &info, &cb);
    if (result != VK_SUCCESS)
    {
        printf("[ERROR] Command buffer allocation failed: %i\n", result);
        return false;
    }
    ox::defer ds2([d = this->mDevice, commandPool, cb]{vkFreeCommandBuffers(d, commandPool, 1, &cb);});

    std::vector<CPUBuffer> cachedBuffers;
    {
        VkCommandBufferBeginInfo cbBeginInfo = 
        { 
            .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO, 
            .flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT
        };
        vkBeginCommandBuffer(cb, &cbBeginInfo);
        ox::defer ds3([cb]{vkEndCommandBuffer(cb);});
        for (size_t i = 0; i < static_cast<size_t>(ox::FontResource::SIZE); i++)
        {
            auto buf = mFonts[i].create(mDevice, mAllocator, cb, fontPaths[i], fontSizes[i], 512, 512);
            if (!buf.valid)
                return false;
            cachedBuffers.push_back(buf.data);
        }
        VkBufferCopy region = { .srcOffset = 0, .dstOffset = 0, .size = cubeMeshByteCount };
        vkCmdCopyBuffer(cb, cubeBuffer.buffer(), mCubeMesh.buffer.buffer(), 1, &region);
    }

    VkSubmitInfo submitInfo =
    {
        .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
        .waitSemaphoreCount = 0,
        .pWaitSemaphores = nullptr,
        .pWaitDstStageMask = nullptr,
        .commandBufferCount = 1,
        .pCommandBuffers = &cb,
        .signalSemaphoreCount = 0,
        .pSignalSemaphores = nullptr
    };

    result = vkQueueSubmit(mGfxCompQueue.queue, 1, &submitInfo, VK_NULL_HANDLE);
    CHECK_AND_RETURN(result, "[ERROR] Can't submit queue for resources loading: %i\n");

    result = vkQueueWaitIdle(mGfxCompQueue.queue);
    CHECK_AND_RETURN(result, "[ERROR] Can't wait queue for resources loading: %i\n");

    for (auto& b : cachedBuffers)
        b.destroy(mAllocator);

    return true;
}

auto ox::Render::get(ox::ShaderResource res) const noexcept -> VkShaderModule
{
    if (res >= ox::ShaderResource::SIZE)
    {
        printf("[ERROR] Wrong ShaderResource enum : %i. Maximum enum posible is: %i\n", int(res), int (ox::ShaderResource::SIZE));
        return nullptr;
    }
    return mShaders[static_cast<size_t>(res)];
}

auto ox::Render::get(ox::FontResource res) const noexcept -> const BitmapFont*
{
    if (res >= ox::FontResource::SIZE)
    {
        printf("[ERROR] Wrong FontResource enum : %i. Maximum enum posible is: %i\n", int(res), int (ox::FontResource::SIZE));
        return nullptr;
    }
    return &mFonts[static_cast<size_t>(res)];
}

auto ox::Render::wait() -> void
{
    if (mDevice)
        vkDeviceWaitIdle(mDevice);
}

auto ox::Render::createInstance() -> bool
{
    const auto layers = getLayers();
    const auto extensionsInstance = getInstanceExtensions();

    VkApplicationInfo appInfo = 
    {
        .sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
        .pNext = nullptr,
        .pApplicationName = "OX CAD",
        .applicationVersion = VK_MAKE_VERSION(1, 0, 0),
        .pEngineName = "OX ENGINE",
        .engineVersion = VK_MAKE_VERSION(1, 0, 0),
        .apiVersion = VK_API_VERSION_1_3
    };

    printf("[INFO] Vulkan API version: %u\n", appInfo.apiVersion);

    VkValidationFeatureEnableEXT enableValidationFeatures[] = 
    {
        VkValidationFeatureEnableEXT::VK_VALIDATION_FEATURE_ENABLE_SYNCHRONIZATION_VALIDATION_EXT,
    };
    VkValidationFeaturesEXT validationFeatures = 
    {
        .sType = VK_STRUCTURE_TYPE_VALIDATION_FEATURES_EXT,
        .pNext = nullptr,
        .enabledValidationFeatureCount = sizeof(enableValidationFeatures) / sizeof(enableValidationFeatures[0]),
        .pEnabledValidationFeatures = enableValidationFeatures,
        .disabledValidationFeatureCount = 0,
        .pDisabledValidationFeatures = nullptr
    };
    VkInstanceCreateInfo info = 
    {
        .sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
        // TO REMOVE SYNCHRONIZATION DEBUG, COMMENT THIS OUT
        //.pNext = &validationFeatures,
        .pNext = nullptr,
        .flags = 0,
        .pApplicationInfo = &appInfo,
        .enabledLayerCount = (uint32_t)(layers.size()),
        .ppEnabledLayerNames = layers.empty() ? nullptr : layers.data(),
        .enabledExtensionCount = (uint32_t)extensionsInstance.size(),
        .ppEnabledExtensionNames = extensionsInstance.data()
    };

    auto result = vkCreateInstance(&info, nullptr, &mInstance);
    CHECK_AND_RETURN(result, "[ERROR] Instance creation failed: %i\n");

    return true;
}

auto ox::Render::createDebugger() -> bool
{
    if constexpr (!ox::IS_RENDER_DEBUG)
        return true;

    printf("[INFO] DEBUGGER CREATED\n");
    VkDebugUtilsMessengerCreateInfoEXT debuggerInfo = 
    {
        .sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT,
        .pNext = nullptr,
        .flags = 0,
        .messageSeverity = 
            VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | 
            VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | 
            VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT,
        .messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | 
            VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | 
            VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT,
        .pfnUserCallback = debugCallback,
        .pUserData = nullptr
    };
    auto vkCreateDebugUtilsMessengerEXT = (PFN_vkCreateDebugUtilsMessengerEXT) vkGetInstanceProcAddr
        (mInstance, "vkCreateDebugUtilsMessengerEXT");
    auto result = vkCreateDebugUtilsMessengerEXT(mInstance, &debuggerInfo, nullptr, &mDebugger);
    CHECK_AND_RETURN(result, "[ERROR] Debugger creation failed: %i\n");

    return true;
}

auto ox::Render::choosePhysicalDevice() -> bool
{
    std::vector<VkPhysicalDevice> devices;
    ox::vkenum(vkEnumeratePhysicalDevices, devices, mInstance);

    using DeviceCost = std::pair<VkPhysicalDevice, float>; 
    std::vector<DeviceCost> chosenDevices;
    
    for (auto device : devices)
        if (checkPresentSupport(device))
            chosenDevices.emplace_back(device, getDeviceScore(device));

    if (chosenDevices.empty())
    {
        printf("[ERROR] Couldn't find appropriate physical device\n");
        return false;
    }

    std::sort(chosenDevices.begin(), chosenDevices.end(), 
            [](DeviceCost lhs, DeviceCost rhs) { return lhs.second > rhs.second; });

    mPhysicalDevice = chosenDevices[0].first;

    bool found = false;
    for (const auto& format : getDepthFormats())
    {
        VkFormatProperties props;
        vkGetPhysicalDeviceFormatProperties(mPhysicalDevice, format, &props);
        if (props.optimalTilingFeatures & VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT)
        {
            mTargetDepthFormat = format;
            found = true;
            break;
        }
    }
    if (!found)
    {
        printf("[ERROR] Couldn't find needed depth format\n");
        return false;
    }

    return true;
}

auto ox::Render::chooseQueueFamilies() -> bool
{
    std::vector<VkQueueFamilyProperties> queueProps;
    vkenum(vkGetPhysicalDeviceQueueFamilyProperties, queueProps, mPhysicalDevice);

    for (uint32_t i = 0; i < queueProps.size(); i++)
    {
        if ((queueProps[i].queueFlags & (VK_QUEUE_GRAPHICS_BIT | VK_QUEUE_COMPUTE_BIT)) == 0)
            continue;
        if (checkPresentSupport(mPhysicalDevice, i))
        {
            mGfxCompQueue = { .family = i, .id = 0, .initialized = true};
            mPresentQueue = { .family = i, .id = 0, .initialized = true};
            break;
        }
        else
        {
            mGfxCompQueue = { .family = i, .id = 0, .initialized = true};
        }
    }

    if (!mGfxCompQueue.initialized)
    {
        printf("[ERROR] Couldn't find combined graphic and compute queue\n");
        return false;
    }

    for (uint32_t i = 0; i < queueProps.size(); i++)
    {
        if (mPresentQueue.initialized)
            break;
        if (checkPresentSupport(mPhysicalDevice, i))
        {
            mPresentQueue = { .family = i, .id = 0, .initialized = true};
            break;
        }
    }

    if (!mPresentQueue.initialized)
    {
        printf("[ERROR] Couldn't find present queue\n");
        return false;
    }

    for (uint32_t i = 0; i < queueProps.size(); i++)
    {
        uint32_t id = 0;
        if (mGfxCompQueue.family == i)
        {
            if (queueProps[i].queueCount < 2) continue;
            else id = 1; 
        }
        if (mPresentQueue.family == i)
        {
            if (queueProps[i].queueCount < 2) continue;
            else id = 1; 
        }
        if ((queueProps[i].queueFlags & VK_QUEUE_COMPUTE_BIT) == 0)
            continue;
        mComputeQueue = { .family = i, .id = id, .initialized = true};
        break;
    }

    if (!mComputeQueue.initialized)
    {
        printf("[ERROR] Couldn't find standalone compute queue\n");
        return false;
    }

    return true;
}

static auto createQueueInfos (const std::vector<ox::RenderQueue>& queues) -> std::vector<VkDeviceQueueCreateInfo>
{
    static const float priorities[2] = {1.0f, 1.0f};

    std::vector<VkDeviceQueueCreateInfo> infos;
    std::set<std::pair<uint32_t, uint32_t>> uniqueQueues; 

    for (auto queue : queues)
        uniqueQueues.insert({queue.family, queue.id});

    for (auto queue : uniqueQueues)
    {
        auto family = queue.first;
        auto it = std::find_if(infos.begin(), infos.end(), [family](const VkDeviceQueueCreateInfo& info)
                {return info.queueFamilyIndex == family;});
        if (it != infos.end())
            it->queueCount++;
        else
            infos.push_back({
            .sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
            .pNext = nullptr, .flags = 0,
            .queueFamilyIndex = family,
            .queueCount = 1,
            .pQueuePriorities = priorities});
    }
    return infos;
}

auto ox::Render::createDevice() -> bool
{
    auto queueInfos = createQueueInfos({mGfxCompQueue, mPresentQueue, mComputeQueue});

    VkPhysicalDeviceSynchronization2Features synchro2 =
    {
        .sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SYNCHRONIZATION_2_FEATURES,
        .pNext = nullptr,
        .synchronization2 = true
    };
    VkPhysicalDeviceDynamicRenderingFeatures dynamicRenderingFeature = 
    {
        .sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DYNAMIC_RENDERING_FEATURES,
        .pNext = &synchro2,
        .dynamicRendering = true
    };

    VkPhysicalDeviceFeatures2 features2 = 
    {
        .sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2,
        .pNext = &dynamicRenderingFeature,
        .features = 
        {
            //.robustBufferAccess;
            //.fullDrawIndexUint32;
            //.imageCubeArray;
            //.independentBlend;
            //.geometryShader;
            //.tessellationShader;
            //.sampleRateShading;
            //.dualSrcBlend;
            //.logicOp;
            .multiDrawIndirect = VK_TRUE,
            //.drawIndirectFirstInstance;
            //.depthClamp;
            //.depthBiasClamp;
            .fillModeNonSolid = VK_TRUE,
            //.depthBounds;
            .wideLines = VK_TRUE,
            //.largePoints;
            //.alphaToOne;
            //.multiViewport;
            .samplerAnisotropy = VK_TRUE,
            //.textureCompressionETC2;
            //.textureCompressionASTC_LDR;
            //.textureCompressionBC;
            //.occlusionQueryPrecise;
            //.pipelineStatisticsQuery;
            .vertexPipelineStoresAndAtomics = VK_TRUE,
            //.fragmentStoresAndAtomics;
            //.shaderTessellationAndGeometryPointSize;
            //.shaderImageGatherExtended;
            //.shaderStorageImageExtendedFormats;
            //.shaderStorageImageMultisample;
            //.shaderStorageImageReadWithoutFormat;
            //.shaderStorageImageWriteWithoutFormat;
            //.shaderUniformBufferArrayDynamicIndexing;
            //.shaderSampledImageArrayDynamicIndexing;
            //.shaderStorageBufferArrayDynamicIndexing;
            //.shaderStorageImageArrayDynamicIndexing;
            //.shaderClipDistance;
            //.shaderCullDistance;
            //.shaderFloat64;
            //.shaderInt64;
            //.shaderInt16;
            //.shaderResourceResidency;
            //.shaderResourceMinLod;
            //.sparseBinding;
            //.sparseResidencyBuffer;
            //.sparseResidencyImage2D;
            //.sparseResidencyImage3D;
            //.sparseResidency2Samples;
            //.sparseResidency4Samples;
            //.sparseResidency8Samples;
            //.sparseResidency16Samples;
            //.sparseResidencyAliased;
            //.variableMultisampleRate;
            //.inheritedQueries;
        }
    };
    auto extensions = getDeviceExtensions();
    auto layers = getLayers();
    VkDeviceCreateInfo deviceInfo =
    {
        .sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
        .pNext = &features2, 
        .flags = 0,
        .queueCreateInfoCount = (uint32_t)(queueInfos.size()),
        .pQueueCreateInfos = queueInfos.data(),
        .enabledLayerCount = (uint32_t)(layers.size()),
        .ppEnabledLayerNames = layers.empty() ? nullptr : layers.data(),
        .enabledExtensionCount = (uint32_t)extensions.size(),
        .ppEnabledExtensionNames = extensions.empty() ? nullptr : extensions.data(),
        .pEnabledFeatures =  nullptr//&features
    };

    auto result = vkCreateDevice(mPhysicalDevice, &deviceInfo, nullptr, &mDevice);
    CHECK_AND_RETURN(result, "[ERROR] Device creation failed: %i\n");

    return true;
}

auto ox::Render::getQueues() -> void
{
    vkGetDeviceQueue(mDevice, mGfxCompQueue.family, mGfxCompQueue.id, &mGfxCompQueue.queue);
    vkGetDeviceQueue(mDevice, mPresentQueue.family, mPresentQueue.id, &mPresentQueue.queue);
    vkGetDeviceQueue(mDevice, mComputeQueue.family, mComputeQueue.id, &mComputeQueue.queue);
}


auto ox::Render::printDeviceInfo() -> void
{
    VkPhysicalDeviceProperties props;
    vkGetPhysicalDeviceProperties(mPhysicalDevice, &props);
    printf("------------------------------------------------------------\n");
    printf("DEVICE: %s \tVERSION: %u\n", props.deviceName, props.driverVersion);
    printf("GRAPHIC AND COMPUTE QUEUE:\tFAMILY: %i \tID: %i\n", mGfxCompQueue.family, mGfxCompQueue.id);
    printf("PRESENT QUEUE:\t\t\tFAMILY: %i \tID: %i\n", mPresentQueue.family, mPresentQueue.id);
    printf("STANDALONE COMPUTE QUEUE:\tFAMILY: %i \tID: %i\n", mComputeQueue.family, mComputeQueue.id);
    printf("------------------------------------------------------------\n");
}

auto ox::Render::createAllocator() -> bool
{
    VmaAllocatorCreateInfo info = 
    {
        .flags = 0,
        .physicalDevice = mPhysicalDevice,
        .device = mDevice,
        /// Preferred size of a single `VkDeviceMemory` block to be allocated from large heaps > 1 GiB. Optional.
        /** Set to 0 to use default, which is currently 256 MiB. */
        .preferredLargeHeapBlockSize = 0,
        .pAllocationCallbacks = nullptr,
        .pDeviceMemoryCallbacks = nullptr,
        //.frameInUseCount = 2,
        .pHeapSizeLimit = nullptr,
        .pVulkanFunctions = nullptr,
        //.pRecordSettings = nullptr,
        .instance = mInstance,
        .vulkanApiVersion = VK_API_VERSION_1_2
    };

    auto result = vmaCreateAllocator(&info, &mAllocator);
    CHECK_AND_RETURN(result, "[ERROR] Allocator creation failed: %i\n");

    return true;
}
