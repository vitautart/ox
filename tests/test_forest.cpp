#include <forest.hpp>
#include <containers.hpp>

template <typename TREE>
bool tree_test()
{
    auto bst = TREE();
    bst.insert(1);
    bst.insert(2);
    bst.insert(3);
    bst.insert(4);
    bst.insert(5);
    bst.insert(6);
    bst.insert(7);
    bst.insert(8);
    bst.insert(8);
    bst.insert(0);

    printf("Height: %zu\n", ox::tree::height(bst));

    auto node = bst.min(); if (node->data != 0) return false;
    node = bst.next(node); if (node->data != 1) return false;
    node = bst.next(node); if (node->data != 2) return false;
    node = bst.next(node); if (node->data != 3) return false;
    node = bst.next(node); if (node->data != 4) return false;
    node = bst.next(node); if (node->data != 5) return false;
    node = bst.next(node); if (node->data != 6) return false;
    node = bst.next(node); if (node->data != 7) return false;
    node = bst.next(node); if (node->data != 8) return false;
    node = bst.next(node); if (node->data != 8) return false;
    node = bst.next(node); if (node != bst.end()) return false;
    
    node = bst.max();      if (node->data != 8) return false;
    node = bst.prev(node); if (node->data != 8) return false;
    node = bst.prev(node); if (node->data != 7) return false;
    node = bst.prev(node); if (node->data != 6) return false;
    node = bst.prev(node); if (node->data != 5) return false;
    node = bst.prev(node); if (node->data != 4) return false;
    node = bst.prev(node); if (node->data != 3) return false;
    node = bst.prev(node); if (node->data != 2) return false;
    node = bst.prev(node); if (node->data != 1) return false;
    node = bst.prev(node); if (node->data != 0) return false;
    node = bst.prev(node); if (node != bst.end()) return false;

    typename TREE::node_t* node_rem = nullptr;
    node_rem = bst.search(4); if (node_rem->data != 4) return false; bst.remove(node_rem);
    node_rem = bst.search(6); if (node_rem->data != 6) return false; bst.remove(node_rem);
    node_rem = bst.search(4); if (node_rem != bst.end()) return false;
    node_rem = bst.search(6); if (node_rem != bst.end()) return false;

    node = bst.min();      if (node->data != 0) return false;
    node = bst.next(node); if (node->data != 1) return false;
    node = bst.next(node); if (node->data != 2) return false;
    node = bst.next(node); if (node->data != 3) return false;
    node = bst.next(node); if (node->data != 5) return false;
    node = bst.next(node); if (node->data != 7) return false;
    node = bst.next(node); if (node->data != 8) return false;
    node = bst.next(node); if (node->data != 8) return false;
    node = bst.next(node); if (node != bst.end()) return false;

    node_rem = bst.search(0); if (node_rem->data != 0) return false; bst.remove(node_rem);
    node_rem = bst.search(8); if (node_rem->data != 8) return false; bst.remove(node_rem);
    node_rem = bst.search(8); if (node_rem->data != 8) return false; bst.remove(node_rem);
    node_rem = bst.search(4); if (node_rem != bst.end()) return false;
    node_rem = bst.search(6); if (node_rem != bst.end()) return false;

    node = bst.max();      if (node->data != 7) return false;
    node = bst.prev(node); if (node->data != 5) return false;
    node = bst.prev(node); if (node->data != 3) return false;
    node = bst.prev(node); if (node->data != 2) return false;
    node = bst.prev(node); if (node->data != 1) return false;
    node = bst.prev(node); if (node != bst.end()) return false;

    if (ox::tree::lower(bst, 5.0f, [](float x, int y){ return x > y; })->data != 3) return false;
    if (ox::tree::lower(bst, 4.0f, [](float x, int y){ return x > y; })->data != 3) return false;
    if (ox::tree::lower(bst, 1, [](int x, int y){ return x > y; }) != bst.end()) return false;

    if (ox::tree::upper(bst, 5.0f, [](float x, int y){ return x < y; })->data != 7) return false;
    if (ox::tree::upper(bst, 4.0f, [](float x, int y){ return x < y; })->data != 5) return false;
    if (ox::tree::upper(bst, 7, [](int x, int y){ return x < y; }) != bst.end()) return false;

    printf("Height: %zu\n", ox::tree::height(bst));

    ox::tree::inorderwalk(bst, [](int i){printf(" %i ", i);}); printf("\n");

    node_rem = bst.search(7); if (node_rem->data != 7) return false; bst.remove(node_rem);
    node_rem = bst.search(5); if (node_rem->data != 5) return false; bst.remove(node_rem);
    node_rem = bst.search(3); if (node_rem->data != 3) return false; bst.remove(node_rem);
    node_rem = bst.search(2); if (node_rem->data != 2) return false; bst.remove(node_rem);
    node_rem = bst.search(1); if (node_rem->data != 1) return false; bst.remove(node_rem);

    return true;
}

auto test_staticvector() -> bool
{
    auto v = ox::staticvector<int>(10);
    if (v.size() != 0) return false;
    if (v.capacity() != 10) return false;

    v.push_back(2);
    v.push_back(4);
    v.push_back(6);
    if (v[0] != 2) return false;
    if (v[1] != 4) return false;
    if (v[2] != 6) return false;
    v.pop_back();
    
    if (v.back() != 4) return false;

    v.clear_reserve(20);

    if (v.size() != 0) return false;
    if (!v.empty()) return false;
    if (v.capacity() != 20) return false;

    v.push_back(3);
    v.push_back(5);
    v.push_back(7);
    v.push_back(9);
    if (v[0] != 3) return false;
    if (v[1] != 5) return false;
    if (v[2] != 7) return false;
    if (v[3] != 9) return false;
    
    if (v.size() != 4) return false;
    if (v.capacity() != 20) return false;

    return true;
}

#include <oxmath.hpp>

struct SweeplineEdge2
{
    double a, b;
};

inline auto createSweeplineEdge(const oxm::vd2& p1, const oxm::vd2& p2) noexcept -> SweeplineEdge2
{
    const auto x3x4 = p1[0] - p2[0];

    const auto y3y4r = 1 / (p1[1] - p2[1]);
   
    const auto y3x4x3y4 = p1[1] * p2[0] - p1[0] * p2[1];
    
    return 
    {
        .a = x3x4 * y3y4r,
        .b = y3x4x3y4 * y3y4r,
    };
}
inline auto ysweeplineIntX3(double y, double a, double b) noexcept -> double
{
    return y * a + b;
}
auto test_sweepline() -> bool
{
    auto sw1 = createSweeplineEdge({2, -1}, {0, -3});
    auto x = ysweeplineIntX3(-2, sw1.a, sw1.b);
    if (std::abs(x - 1) > 0.00001) return false;
    x = ysweeplineIntX3(-1, sw1.a, sw1.b);
    if (std::abs(x - 2) > 0.00001) return false;

    sw1 = createSweeplineEdge({2, 2}, {4, 4});
    x = ysweeplineIntX3(3, sw1.a, sw1.b);
    if (std::abs(x - 3) > 0.00001) return false;
    x = ysweeplineIntX3(4, sw1.a, sw1.b);
    if (std::abs(x - 4) > 0.00001) return false;

    sw1 = createSweeplineEdge({-2, 4}, {-2, 1} );
    x = ysweeplineIntX3(2, sw1.a, sw1.b);
    if (std::abs(x - (-2)) > 0.00001) return false;
    x = ysweeplineIntX3(1, sw1.a, sw1.b);
    if (std::abs(x - (-2)) > 0.00001) return false;
    return true;
}

#define RUNTEST(name) printf("[TEST] " #name " started\n");\
    if (!name()) printf("[TEST] " #name " failed\n");\
    else printf("[TEST] " #name " succesful\n");\

auto main () -> int
{
    RUNTEST(tree_test<ox::bst<int>>);
    printf ("\n");
    RUNTEST(tree_test<ox::rbtree<int>>);
    printf ("\n");
    RUNTEST(test_staticvector);
    printf ("\n");
    RUNTEST(test_sweepline);
    printf ("\n");
    return 0;
}
