#pragma once

#include <cinttypes> 
#include <limits>
#include <compare>
#include <oxgeometry.hpp>


namespace ox
{

struct VertexIdx { uint32_t idx; };
struct HalfedgeIdx { uint32_t idx; };
struct EdgeIdx { uint32_t idx; };
struct LoopIdx { uint32_t idx; };
struct FaceIdx { uint32_t idx; };

constexpr VertexIdx IDX_INV_VERT = { std::numeric_limits<uint32_t>::max() };
constexpr HalfedgeIdx IDX_INV_HALFEDGE = { std::numeric_limits<uint32_t>::max() };
constexpr EdgeIdx IDX_INV_EDGE = { std::numeric_limits<uint32_t>::max() };
constexpr LoopIdx IDX_INV_LOOP = { std::numeric_limits<uint32_t>::max() };
constexpr FaceIdx IDX_INV_FACE = { std::numeric_limits<uint32_t>::max() };

auto operator<=> (VertexIdx idx1, VertexIdx idx2) noexcept -> std::strong_ordering;
auto operator<=> (HalfedgeIdx idx1, HalfedgeIdx idx2) noexcept -> std::strong_ordering;
auto operator<=> (EdgeIdx idx1, EdgeIdx idx2) noexcept -> std::strong_ordering;
auto operator<=> (LoopIdx idx1, LoopIdx idx2) noexcept -> std::strong_ordering;
auto operator<=> (FaceIdx idx1, FaceIdx idx2) noexcept -> std::strong_ordering;

auto operator== (VertexIdx idx1, VertexIdx idx2) noexcept -> bool;
auto operator== (HalfedgeIdx idx1, HalfedgeIdx idx2) noexcept -> bool;
auto operator== (EdgeIdx idx1, EdgeIdx idx2) noexcept -> bool;
auto operator== (LoopIdx idx1, LoopIdx idx2) noexcept -> bool;
auto operator== (FaceIdx idx1, FaceIdx idx2) noexcept -> bool;

auto operator!= (VertexIdx idx1, VertexIdx idx2) noexcept -> bool;
auto operator!= (HalfedgeIdx idx1, HalfedgeIdx idx2) noexcept -> bool;
auto operator!= (EdgeIdx idx1, EdgeIdx idx2) noexcept -> bool;
auto operator!= (LoopIdx idx1, LoopIdx idx2) noexcept -> bool;
auto operator!= (FaceIdx idx1, FaceIdx idx2) noexcept -> bool;

struct Vertex2D
{
    using TableIdx = VertexIdx;

    HalfedgeIdx input;
    oxm::Point2D<double> point;
};

struct Vertex3D
{
    using TableIdx = VertexIdx;
    
    HalfedgeIdx input;
    oxm::Point3D<double> point;
};

}
