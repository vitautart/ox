#pragma once

namespace ox
{
template<typename T, T size>
class ringindexer
{
public:
    auto next() noexcept -> T { mCounter = (mCounter + 1) % size; return mCounter; }
    auto last() const noexcept -> T { return mCounter; }
    auto reset() noexcept -> void { mCounter = 0; }
private:
    T mCounter = 0;
};
}
