#include "brep.hpp"
#include "oxgeometry.hpp"
#include "oxmath.hpp"
#include "render/rendervertex.hpp"
#include <cmath>
#include <cstdint>
#include <discretization.hpp>

#include <iterator>
#include <unordered_set>

// TODO: use simplest possible discretization
auto ox::Discretization::create(const BRep3D& iBrep) -> Discretization
{
    Discretization output;

    output.createWireframe(iBrep);
    output.createSolid(iBrep);

    return output;
}

namespace ox
{
    inline auto edgeEndNormal(const BRep3D& iBrep, const Vertex3D& iVertex) -> oxm::Direction3D<double>
    {
        oxm::Direction3D<double> normalSum = {};
        size_t faceCount = 0;
        iBrep.foreach(iVertex, [&brep = iBrep, &iVertex, &normalSum, &faceCount](const Face& face)
        {
            normalSum = normalSum + face.surface->normal(iVertex.point);
            faceCount++;
        });
        // WARNING : we assume that we do not have average normal with length 0, 
        // e.g. edge of two coplanar faces
        return oxm::norm(normalSum * (1.0 / faceCount));
    }
    /*inline auto edgeEndNormal(const BRep3D& iBrep, const Vertex3D& iVertex) -> oxm::Direction3D<double>
    {
        oxm::Direction3D<double> normalSum = {};
        std::vector<oxm::vd3> normals;
        iBrep.foreach(iVertex, [&brep = iBrep, &iVertex, &normalSum, &normals](const Face& face)
        {
            normals.push_back(face.surface->normal(iVertex.point));
        });

        for (size_t i = 0; i < normals.size(); i++)
        {
            auto& a = normals[i];
            auto& b = normals[(i + 1) % normals.size()];
            normalSum += oxm::norm((a + b) * 0.5) * std::sqrt(2 / (oxm::dot(a, b) + 1));
        }
        return normalSum;
    }*/
}

// TODO: cache normals on vertexes, or even cache in multimap for each face
// TODO: cache data on halfedges
auto ox::Discretization::createWireframe(const BRep3D& iBrep) -> void
{
    std::vector<TopoPoint> points;
    std::vector<std::optional<oxm::vf3>> cachedVertexNormals (iBrep.getVertices().size());

    for (const auto& [edge, valid] : iBrep.getEdges())
    {
        if (!valid)
            continue;
        points.clear();

        const auto surf1 = iBrep.get(iBrep.get(iBrep.get(edge.half).loop).face).surface.get();
        const auto surf2 = iBrep.get(iBrep.get(iBrep.get(iBrep.get(edge.half).twin).loop).face).surface.get();
        const auto startIdx = iBrep.get(edge.half).start;
        const auto endIdx = iBrep.get(iBrep.get(edge.half).twin).start;
        const auto& start = iBrep.get(startIdx);
        const auto& end = iBrep.get(endIdx);

        edge.curve->discretize(start.point, end.point, points);

        if (points.empty())
            continue;

        // Process start point
        {
            auto& cachedVertex = cachedVertexNormals[startIdx.idx];
            if (!cachedVertex)
                cachedVertex = edgeEndNormal(iBrep, start).cast<float>();

            mWireframeIndexes.push_back(uint32_t(mWireframePoints.size()));
            mWireframePoints.push_back(RenderVertex3D{
                    .pos = points[0].cast<float>(), 
                    .normal = cachedVertex.value(), 
                    .col = {0.05, 0.05, 0.05} });
        }

        // Process middle points
        for (size_t i = 1; (i + 1) < points.size(); i++)
        {
            // WARNING : we assume that we do not have average normal with length 0, 
            auto normal = oxm::norm((surf1->normal(points[i]) + surf2->normal(points[i])) * 0.5).cast<float>();

            mWireframeIndexes.push_back(uint32_t(mWireframePoints.size()));
            mWireframeIndexes.push_back(uint32_t(mWireframePoints.size()));
            mWireframePoints.push_back(RenderVertex3D{
                    .pos = points[i].cast<float>(), 
                    .normal = normal, 
                    .col = {0.05, 0.05, 0.05} });
        }

        // Process end point
        {
            auto& cachedVertex = cachedVertexNormals[endIdx.idx];
            if (!cachedVertex)
                cachedVertex = edgeEndNormal(iBrep, end).cast<float>();

            mWireframeIndexes.push_back(uint32_t(mWireframePoints.size()));
            mWireframePoints.push_back(RenderVertex3D{
                    .pos = points.back().cast<float>(), 
                    .normal = cachedVertex.value(), 
                    .col = {0.05, 0.05, 0.05} });
        }
    }
}

auto ox::Discretization::createSolid(const BRep3D& iBrep) -> void
{
    mSolidIndexes.clear();
    mSolidPoints.clear();
    auto loopPoints = std::vector<TopoPoint>(128);
    auto loops = std::vector<std::span<TopoPoint>>{};
    auto loopsInterval = std::vector<std::pair<size_t, size_t>>{};

    auto pointAdder = std::function([this] (const TopoPoint& pos, const TopoDir& normal) 
    {
        mSolidPoints.push_back(RenderVertex3D{
            .pos = pos.cast<float>(), 
            .normal = normal.cast<float>(), 
            .col = {0.5, 0.5, 0.5} });
    });

    uint32_t startIndex = mSolidPoints.size();
    auto triangleAdder = std::function([this, &startIndex] (uint32_t p1, uint32_t p2, uint32_t p3) 
    {
        mSolidIndexes.push_back(startIndex + p1);
        mSolidIndexes.push_back(startIndex + p2);
        mSolidIndexes.push_back(startIndex + p3);
    });

    for (const auto& [face, valid] : iBrep.getFaces())
    {
        if (!valid)
            continue;

        loopPoints.clear();
        loopsInterval.clear();
        loops.clear();

        iBrep.foreach(face, [&brep = iBrep, &loopPoints, &loopsInterval] (const Loop& loop)
        {
            size_t start = loopPoints.size();
            brep.foreach(loop, [&brep, &loopPoints](const Halfedge& halfedge)
            {
                brep.get(halfedge.edge).curve->discretize(
                        brep.get(halfedge.start).point, 
                        brep.get(brep.get(halfedge.next).start).point, 
                        loopPoints);
                loopPoints.pop_back();
            });
            loopsInterval.emplace_back(start, size_t(loopPoints.size() - start));
        });

        // Should do this because of invalidation of loopPoints iterators
        for (auto interval : loopsInterval)
            loops.emplace_back(loopPoints.begin() + interval.first, interval.second);

        startIndex = mSolidPoints.size();
        face.surface->discretize(loops, pointAdder, triangleAdder);
    }
}
