#include "topoplanesurface.hpp"
#include "oxgeometry.hpp"
#include "oxmath.hpp"
#include "topobase.hpp"
#include "arena.hpp"
#include <queue>
#include <cassert>
//#include <cstdint>
//#include <type_traits>

namespace {

struct AdjustedTrianglePair;

// https://youtu.be/1TUUevxkvp4
// This is a special structure to contain direct acyclic graph
// of triangles in time space and general graph for spatial connectness.
// TODO: currently this struct has size 72 bytes, to reduce it and put to cache line
// we may combine adjusted and childs fields in one DelaunayTriangles[3] field
// because thay used with mutual exclusion, leaf triangles are using only adjusted,
// and other are using only childs.
struct DelaunayTriangle
{
    // Adjusted triangles lay on the edge its adjusted
    DelaunayTriangle* adjusted[3];
    // Children is not sorted in any order and is filled from start to end there are no gaps with nullptr
    DelaunayTriangle* childs[3]; 
    // vertices ids
    uint32_t ids[3];

    // Used only in CDT
    AdjustedTrianglePair* edgeIds[3];

    // used only if triangle is leaf, others have invalid value
    uint32_t leafId;  
    // Side id of correspondent adjusted triangle 
    uint8_t adjustedSideId[3]; 
};

struct Point2DConnected
{
    ox::TopoPoint2D point;
    uint32_t next; 
    uint32_t prev; 
};

#define CDT_INVALIDPAIR 4

struct AdjustedTrianglePair
{
    DelaunayTriangle* tr;
    uint8_t edgeId;

    inline auto second() const noexcept -> DelaunayTriangle*
    {
        return tr->adjusted[edgeId];
    }
    inline auto secondId() const noexcept -> uint8_t
    {
        return tr->adjustedSideId[edgeId];
    }
    inline auto hasSecond() const noexcept -> bool
    {
        return edgeId < 3;
    }
};

struct DelaunayResources
{
    ox::arena<DelaunayTriangle> arena;
    std::vector<Point2DConnected> points;
    // This vector contains leaf triangles, that guarantee has vertex 
    // which is points[id] and pointer to that triangle is trianglepoints[id]
    std::vector<DelaunayTriangle*> trianglepoints;
    // List of all triangle leaves
    // Needed to quickly take all of triangles at hte end. We can't go though triangle tri, because it has shared childs.
    std::vector<DelaunayTriangle*> leaves;

    ox::arena<AdjustedTrianglePair> pairArena;
    // Intersecting edges: used for CDT algo
    std::queue<AdjustedTrianglePair*> intersections; 
    // New non-intersecting edges: used for CDT
    // TODO: made this multipurpouse stack for recursive use on DT and CDT
    std::vector<AdjustedTrianglePair*> nonintersections;
};

auto refineTriangles(DelaunayResources& resources, const AdjustedTrianglePair& pair) noexcept -> void;

// https://www.newcastle.edu.au/__data/assets/pdf_file/0017/22508/13_A-fast-algorithm-for-constructing-Delaunay-triangulations-in-the-plane.pdf
// Returns true if we need to swap triangle diagonals
// TODO: check if this works for concave triangles
inline auto criterionDelaunaySloan(const ox::TopoPoint2D& pTri1, 
        const ox::TopoPoint2D& pTri2, 
        const ox::TopoPoint2D& pTri3, 
        const ox::TopoPoint2D& pTest) noexcept -> bool
{
    const auto p13 = pTri1 - pTri3;
    const auto p23 = pTri2 - pTri3;
    const auto p1p = pTri1 - pTest;
    const auto p2p = pTri2 - pTest;
    return (p13[0] * p23[0] + p13[1] * p23[1]) * (p2p[0] * p1p[1] - p1p[0] * p2p[1]) <
        (p13[1] * p23[0] - p13[0] * p23[1]) * (p2p[0] * p1p[0] + p1p[1] * p2p[1]);
    /*const auto cosa = p13[0] * p23[0] + p13[1] * p23[1];
    const auto cosb = p2p[0] * p1p[0] + p2p[1] * p1p[1];
    if ((cosa >= 0) && (cosb >= 0))
        return false;
    if ((cosa < 0) && (cosb < 0))
        return true;
    const auto sina = p13[0] * p23[1] - p23[0] * p13[1];
    const auto sinb = p2p[0] * p1p[1] - p1p[0] * p2p[1];
    
    return (sina * cosb + sinb * cosa) < 0;*/
}

inline auto criterionDelaunaySloan(const DelaunayResources& resources, 
        const AdjustedTrianglePair& pair) noexcept -> bool
{
    const DelaunayTriangle* t1 = pair.tr;
    const DelaunayTriangle* t2 = pair.second();

    const auto& points = resources.points;
    return criterionDelaunaySloan(points[t2->ids[0]].point,
            points[t2->ids[1]].point,
            points[t2->ids[2]].point,
            points[t1->ids[(pair.edgeId + 2)%3]].point);
}

inline auto isConvex(const DelaunayResources& resources,
        const AdjustedTrianglePair& pair) noexcept -> bool
{
    const auto t1 = pair.tr;
    const auto t2 = pair.second();
    const auto adjSideId = pair.secondId();

    const auto& points = resources.points;
    return oxm::is_convex(oxm::Quadrangle2D<double>{
            points[t2->ids[(adjSideId + 1) % 3]].point,
            points[t2->ids[(adjSideId + 2) % 3]].point,
            points[t2->ids[adjSideId]].point,
            points[t1->ids[(pair.edgeId + 2)%3]].point});
}

inline auto crosses(const ox::TopoPoint2D& pTri1, const ox::TopoPoint2D& pTri2, 
        const ox::TopoPoint2D& pTri3, const ox::TopoPoint2D& p) noexcept -> oxm::vd3
{
    return 
    {
        oxm::cross(pTri2 - pTri1, p - pTri1),
        oxm::cross(pTri3 - pTri2, p - pTri2),
        oxm::cross(pTri1 - pTri3, p - pTri3)
    };
}

inline auto createSuperTriangle(const oxm::aabbd2& bb, ox::TopoPoint2D& p1, 
        ox::TopoPoint2D& p2, ox::TopoPoint2D& p3) noexcept -> void
{
    auto s = bb.size() * 0.5;
    auto bbb = oxm::aabbd2
    {
        .min = bb.min - s,
        .max = bb.max + s
    };

    auto size = bbb.size();

    // gap = p3[1] - bbb.max[1] = p1[0] + bbb.min[0] = p2[0] - bbb.max[0]
    // trinagle p1 and p2 lay on bbb min horizontal line
    // p3 lays over bbb max horizontal line

    double gap = std::sqrt(size[1] * size[0] * 0.5);

    p1 = { bbb.min[0] - gap, bbb.min[1] };
    p2 = { bbb.max[0] + gap, bbb.min[1] };
    p3 = { bbb.center()[0], bbb.max[1] + gap };

}

inline auto getEdge(const DelaunayResources& resources, const DelaunayTriangle* t, const uint8_t sideId) noexcept -> oxm::LineSegment2D<double>
{
    return
    {
        resources.points[t->ids[sideId]].point,
        resources.points[t->ids[(sideId + 1) % 3]].point,
    };
}

// Assume we do not have points on the triangle vertex only on edge
// Assume that t->adjusted array corresponds to same edge wich triangles are sharing
auto getTriangleAtPoint(const std::vector<Point2DConnected>& points, 
        const ox::TopoPoint2D& p, DelaunayTriangle* t) noexcept -> AdjustedTrianglePair
{
    auto crs = crosses(points[t->ids[0]].point, points[t->ids[1]].point, points[t->ids[2]].point, p);

    constexpr auto e = 8 * oxm::DISTANCE_EPSILON<double>;

    if ((crs[0] > -e) && (crs[1] > -e) && (crs[2] > -e)) // point inside triangle or on its edge
    {
        for (size_t i = 0; i < 3; i++)
        {
            auto child = t->childs[i];
            if (!child) // Can break here because childs is filled from start to end without nullptr gaps
                break;
            auto result = getTriangleAtPoint(points, p, child);
            if (result.tr)
                return result;
        }

        // if we haven't returned yet, than it is leaf triangle
        if (crs[0] < e) // point on the 0 edge
            return { t, 0 };
        else if (crs[1] < e) // point on the 1 edge
            return { t, 1 };
        else if (crs[2] < e) // point on the 2 edge
            return { t, 2 };
        else // point inside triangle
            return { t, CDT_INVALIDPAIR };
    }
    
    return { nullptr };
}

// TODO: check if needed
inline auto getSecondCommonEdgeId(const DelaunayTriangle* t1, const DelaunayTriangle* t2) noexcept -> uint8_t
{
    for (uint32_t tr2EdgeId = 0; tr2EdgeId < 3; tr2EdgeId++)
        if (t2->adjusted[tr2EdgeId] == t1) return tr2EdgeId;
    return CDT_INVALIDPAIR;
}

auto rebindAdjustedToChild2(const DelaunayTriangle* parent, uint8_t parentSideId, DelaunayTriangle* child, uint8_t childSideId) noexcept -> uint8_t
{

    *(parent->edgeIds[parentSideId]) = {.tr = child, .edgeId = childSideId};
    const auto adjusted = parent->adjusted[parentSideId];
    if (adjusted)
    {
        auto adjustedSideId = parent->adjustedSideId[parentSideId];
        adjusted->adjusted[adjustedSideId] = child;
        adjusted->adjustedSideId[adjustedSideId] = childSideId;
        return adjustedSideId;
    }
    return 4;
}

auto rebindEdge(const DelaunayTriangle* oldt, uint8_t oldSideId, DelaunayTriangle* newt, uint8_t newSideId) noexcept -> void
{
    auto edge = oldt->edgeIds[oldSideId];
    if (edge)
        *edge = {.tr = newt, .edgeId = newSideId};
}

auto rebindAdjustedToChild(const DelaunayTriangle* parent, uint8_t parentSideId, DelaunayTriangle* child, uint8_t childSideId) noexcept -> uint8_t
{
    const auto adjusted = parent->adjusted[parentSideId];
    if (adjusted)
    {
        // TODO: check if needed
        //auto adjustedSideId = getSecondCommonEdgeId(parent, adjusted);
        auto adjustedSideId = parent->adjustedSideId[parentSideId];
        adjusted->adjusted[adjustedSideId] = child;
        adjusted->adjustedSideId[adjustedSideId] = childSideId;
        return adjustedSideId;
    }
    return 4;
}

auto divideTriangleBy3(DelaunayResources& resources,
        uint32_t pId, DelaunayTriangle* t) noexcept -> void
{
    for (size_t i = 0; i < 3; i++)
        t->childs[i] = resources.arena.alloc();

    // TODO: manualy unroll this loop
    for (uint8_t i = 0; i < 3; i++)
    {
        uint8_t adjustedSideId = rebindAdjustedToChild(t, i, t->childs[i], 0);

        *t->childs[i] = DelaunayTriangle
        {
            .adjusted = {t->adjusted[i], t->childs[(i+1)%3], t->childs[(i+2)%3]},
            .childs = {nullptr, nullptr, nullptr},
            .ids = {t->ids[i], t->ids[(i+1)%3], pId},
            .leafId = i == 2 ? t->leafId : (uint32_t)resources.leaves.size() + i,
            .adjustedSideId = { adjustedSideId, 2, 1 }
        };
    }

    resources.leaves.push_back(t->childs[0]);
    resources.leaves.push_back(t->childs[1]);
    resources.leaves[t->leafId] = t->childs[2]; 

    for (size_t i = 0; i < 3; i++)
    {
        auto tpair = AdjustedTrianglePair
        {
            .tr = t->childs[i],
            .edgeId = 0
        };
        refineTriangles(resources, tpair);
    }
}

auto divide2TrianglesBy2(DelaunayResources& resources, uint32_t pId, const AdjustedTrianglePair& pair) noexcept -> void
{
    DelaunayTriangle* t1 = pair.tr;
    DelaunayTriangle* t2 = pair.second();

    for (size_t i = 0; i < 2; i++)
        t1->childs[i] = resources.arena.alloc();
    for (size_t i = 0; i < 2; i++)
        t2->childs[i] = resources.arena.alloc();

    {
        const uint8_t idIds1[] = {pair.edgeId, uint8_t((pair.edgeId + 1) % 3), uint8_t((pair.edgeId + 2) % 3)};
        const uint8_t adjustedSideId1 = rebindAdjustedToChild(t1, idIds1[2], t1->childs[0], 2);
        const uint8_t adjustedSideId2 = rebindAdjustedToChild(t1, idIds1[1], t1->childs[1], 1);

        *t1->childs[0] = DelaunayTriangle
        {
            .adjusted = {t2->childs[1], t1->childs[1], t1->adjusted[idIds1[2]]},
            .childs = {nullptr, nullptr, nullptr},
            .ids = {t1->ids[idIds1[0]], pId, t1->ids[idIds1[2]]},
            .leafId = t1->leafId,
            .adjustedSideId = {0, 2, adjustedSideId1}
        };
        *t1->childs[1] = DelaunayTriangle
        {
            .adjusted = {t2->childs[0], t1->adjusted[idIds1[1]], t1->childs[0]},
            .childs = {nullptr, nullptr, nullptr},
            .ids = {pId, t1->ids[idIds1[1]], t1->ids[idIds1[2]]},
            .leafId = t2->leafId,
            .adjustedSideId = {0, adjustedSideId2, 1}
        };
    }

    {
        const uint8_t tr2EdgeId = t1->adjustedSideId[pair.edgeId];
        const uint8_t idIds2[] = {tr2EdgeId, uint8_t((tr2EdgeId + 1) % 3), uint8_t((tr2EdgeId + 2) % 3)};
        const uint8_t adjustedSideId1 = rebindAdjustedToChild(t2, idIds2[2], t1->childs[0], 2);
        const uint8_t adjustedSideId2 = rebindAdjustedToChild(t2, idIds2[1], t1->childs[1], 1);

        *t2->childs[0] = DelaunayTriangle
        {
            .adjusted = {t1->childs[1], t2->childs[1], t2->adjusted[idIds2[2]]},
            .childs = {nullptr, nullptr, nullptr},
            .ids = {t2->ids[idIds2[0]], pId, t2->ids[idIds2[2]]},
            .leafId = (uint32_t)resources.leaves.size(),
            .adjustedSideId = {0, 2, adjustedSideId1}
        };
        *t2->childs[1] = DelaunayTriangle
        {
            .adjusted = {t1->childs[0], t2->adjusted[idIds2[1]], t2->childs[0]},
            .childs = {nullptr, nullptr, nullptr},
            .ids = {pId, t2->ids[idIds2[1]], t2->ids[idIds2[2]]},
            .leafId = (uint32_t)resources.leaves.size() + 1,
            .adjustedSideId = {0, adjustedSideId2, 1}
        };
    }

    resources.leaves[t1->leafId] = t1->childs[0];
    resources.leaves[t2->leafId] = t1->childs[1];
    resources.leaves.push_back(t2->childs[0]);
    resources.leaves.push_back(t2->childs[1]);
    
    {
        AdjustedTrianglePair pairs[] = 
        {
            {
                .tr = t1->childs[0],
                .edgeId = 2
            },
            {
                .tr = t1->childs[1],
                .edgeId = 1
            },
            {
                .tr = t2->childs[0],
                .edgeId = 2
            },
            {
                .tr = t2->childs[1],
                .edgeId = 1
            }
        };

        for (size_t i = 0; i < 4; i++)
            refineTriangles(resources, pairs[i]);
    }
}

// TODO: for recursive free algo AdjustedTrianglePair
auto refineTriangles(DelaunayResources& resources, const AdjustedTrianglePair& pair) noexcept -> void
{
    DelaunayTriangle* t1 = pair.tr;
    DelaunayTriangle* t2 = pair.second();
    // Check if tr1 or tr2 is leaf triangles in time graph
    if (!t1 || !t2 || t1->childs[0] || t2->childs[0])
        return;

    if (criterionDelaunaySloan(resources, pair))
    {
        t1->childs[0] = resources.arena.alloc();
        t1->childs[1] = resources.arena.alloc();
        t2->childs[0] = t1->childs[0];
        t2->childs[1] = t1->childs[1];    

        {
            const uint8_t tr2EdgeId = pair.secondId();
            const uint8_t idIds1[] = {pair.edgeId, uint8_t((pair.edgeId + 1) % 3), uint8_t((pair.edgeId + 2) % 3)};
            const uint8_t idIds2[] = {tr2EdgeId, uint8_t((tr2EdgeId + 1) % 3), uint8_t((tr2EdgeId + 2) % 3)};
            
            {
                const uint8_t adjustedSideId1 = rebindAdjustedToChild(t1, idIds1[2], t1->childs[0], 1);
                const uint8_t adjustedSideId2 = rebindAdjustedToChild(t2, idIds2[1], t1->childs[0], 2);
                *t1->childs[0] = DelaunayTriangle
                {
                    .adjusted = {t1->childs[1], t1->adjusted[idIds1[2]], t2->adjusted[idIds2[1]] },
                    .childs = {nullptr, nullptr, nullptr},
                    .ids = {t2->ids[idIds2[2]], t1->ids[idIds1[2]], t1->ids[idIds1[0]]},
                    .leafId = t1->leafId,
                    .adjustedSideId = {0, adjustedSideId1, adjustedSideId2}
                };
            }
            {
                const uint8_t adjustedSideId1 = rebindAdjustedToChild(t2, idIds2[2], t1->childs[1], 1);
                const uint8_t adjustedSideId2 = rebindAdjustedToChild(t1, idIds1[1], t1->childs[1], 2);
                *t1->childs[1] = DelaunayTriangle
                {
                    .adjusted = {t1->childs[0], t2->adjusted[idIds2[2]], t1->adjusted[idIds1[1]] },
                    .childs = {nullptr, nullptr, nullptr},
                    .ids = {t1->ids[idIds1[2]], t2->ids[idIds2[2]], t2->ids[idIds2[0]]},
                    .leafId = t2->leafId,
                    .adjustedSideId = {0, adjustedSideId1, adjustedSideId2}
                };
            }

            resources.leaves[t1->leafId] = t1->childs[0];
            resources.leaves[t2->leafId] = t1->childs[1];
        }

        AdjustedTrianglePair pairs[] = 
        {
            {
                .tr = t1->childs[0],
                .edgeId = 1
            },
            {
                .tr = t1->childs[0],
                .edgeId = 2
            },
            {
                .tr = t1->childs[1],
                .edgeId = 1
            },
            {
                .tr = t1->childs[1],
                .edgeId = 2
            }
        };

        // RECURSION
        for (size_t i = 0; i < 4; i++)
            refineTriangles(resources, pairs[i]);
    }
}

auto isConstrained(const DelaunayResources& resources, const AdjustedTrianglePair& pair) noexcept -> bool
{
    const auto endId1 = resources.points[pair.tr->ids[pair.edgeId]].next;
    const auto endId2 = resources.points[pair.tr->ids[pair.edgeId]].prev;
    const auto endTri = pair.tr->ids[(pair.edgeId + 1) % 3];
    return endTri == endId1 || endTri == endId2;
}

auto swapInplace(const AdjustedTrianglePair& pair) noexcept -> AdjustedTrianglePair
{
    DelaunayTriangle* t1 = pair.tr;
    DelaunayTriangle* t2 = pair.second();

    const uint8_t tr2EdgeId = pair.secondId();
    const uint8_t idIds1[] = {pair.edgeId, uint8_t((pair.edgeId + 1) % 3), uint8_t((pair.edgeId + 2) % 3)};
    const uint8_t idIds2[] = {tr2EdgeId, uint8_t((tr2EdgeId + 1) % 3), uint8_t((tr2EdgeId + 2) % 3)};

    DelaunayTriangle newt1, newt2;
    {
        const uint8_t adjustedSideId1 = rebindAdjustedToChild(t1, idIds1[2], t1, 1);
        const uint8_t adjustedSideId2 = rebindAdjustedToChild(t2, idIds2[1], t1, 2);
        rebindEdge(t1, idIds1[2], t1, 1);
        rebindEdge(t2, idIds2[1], t1, 2);
        newt1 = DelaunayTriangle
        {
            .adjusted = {t2, t1->adjusted[idIds1[2]], t2->adjusted[idIds2[1]] },
            .childs = {nullptr, nullptr, nullptr},
            .ids = {t2->ids[idIds2[2]], t1->ids[idIds1[2]], t1->ids[idIds1[0]]},
            .leafId = t1->leafId,
            .adjustedSideId = {0, adjustedSideId1, adjustedSideId2}
        };
    }
    {
        const uint8_t adjustedSideId1 = rebindAdjustedToChild(t2, idIds2[2], t2, 1);
        const uint8_t adjustedSideId2 = rebindAdjustedToChild(t1, idIds1[1], t2, 2);
        rebindEdge(t2, idIds2[2], t2, 1);
        rebindEdge(t1, idIds1[1], t2, 2);
        newt2 = DelaunayTriangle
        {
            .adjusted = {t1, t2->adjusted[idIds2[2]], t1->adjusted[idIds1[1]] },
            .childs = {nullptr, nullptr, nullptr},
            .ids = {t1->ids[idIds1[2]], t2->ids[idIds2[2]], t2->ids[idIds2[0]]},
            .leafId = t2->leafId,
            .adjustedSideId = {0, adjustedSideId1, adjustedSideId2}
        };
    }

    *t1 = newt1;
    *t2 = newt1;

    return  
    {
        .tr = t1,
        .edgeId = 0
    };
}

auto bindEdge(AdjustedTrianglePair* ptr, const AdjustedTrianglePair& value) noexcept -> void
{
        *ptr = value;
        ptr->tr->edgeIds[ptr->edgeId] = ptr;
        ptr->second()->edgeIds[ptr->secondId()] = ptr;
}

auto refineIntersection(DelaunayResources& resources, const AdjustedTrianglePair& pair) noexcept -> void
{
    if (!isConstrained(resources, pair) && criterionDelaunaySloan(resources, pair))
    {
        AdjustedTrianglePair pairs[4];
        {
            auto newPair = resources.pairArena.alloc();
            bindEdge(newPair, swapInplace(pair));
            const uint8_t sideId1 = newPair->edgeId;
            const uint8_t sideId2 = newPair->secondId();

            pairs[0] = AdjustedTrianglePair {
                .tr = newPair->tr,
                .edgeId = uint8_t((sideId1 + 1) % 3)
            };
            pairs[1] = AdjustedTrianglePair {
                .tr = newPair->tr,
                .edgeId = uint8_t((sideId1 + 2) % 3)
            };
            pairs[2] = AdjustedTrianglePair {
                .tr = newPair->second(),
                .edgeId = uint8_t((sideId2 + 1) % 3)
            };
            pairs[3] = AdjustedTrianglePair {
                .tr = newPair->second(),
                .edgeId = uint8_t((sideId2 + 2) % 3)
            };
        }
        // RECURSION
        for (size_t i = 0; i < 4; i++)
            refineIntersection(resources, pairs[i]);
    }
}

auto edgeOnPoint(const DelaunayResources& resources, uint8_t startId) noexcept -> AdjustedTrianglePair
{
    auto t = resources.trianglepoints[startId];
    uint8_t sideId;
    for (sideId = 0; sideId < 3; sideId++)
        if (t->ids[sideId] == startId)
            break;
    return 
    {
        .tr = t,
        .edgeId = sideId
    };
}

auto edgeExists(const DelaunayResources& resources, const AdjustedTrianglePair& startPair, uint32_t endId) noexcept -> bool
{
    DelaunayTriangle* current = startPair.tr;
    uint8_t currentSideId = startPair.edgeId;
    do
    {
        if ((current->ids[(currentSideId + 1) % 3] == endId) || 
            (current->ids[(currentSideId + 2) % 3] == endId))
            return true;

        auto temp = current->adjustedSideId[currentSideId];
        current = current->adjusted[currentSideId];
        currentSideId = (temp + 1) % 3;
    } while(current != startPair.tr);
    return false;
}

auto firstIntersection(DelaunayResources& resources, const AdjustedTrianglePair& startPair, const oxm::LineSegment2D<double>& edge) noexcept -> AdjustedTrianglePair
{
    DelaunayTriangle* current = startPair.tr;
    uint8_t currentSideId = startPair.edgeId;
    uint8_t currentSideIdExternal =  (currentSideId + 1) % 3;
    while(!oxm::is_intersected(edge, getEdge(resources, current, currentSideIdExternal)))
    {
        auto temp = current->adjustedSideId[currentSideId];
        current = current->adjusted[currentSideId];
        currentSideId = temp;
        currentSideIdExternal = (currentSideId + 1) % 3;
    };
    return 
    { 
        .tr = current,
        .edgeId = currentSideIdExternal
    };
}

auto findIntersections(DelaunayResources& resources, const AdjustedTrianglePair& startPair, const oxm::LineSegment2D<double>& edge, uint32_t endId) noexcept -> void
{
    auto tr1 = startPair.tr;
    auto tr2 = startPair.second();
    auto interSideId1 = startPair.edgeId;
    auto interSideId2 = startPair.secondId();
    while(true)
    {
        auto pair = resources.pairArena.alloc();
        bindEdge(pair, {tr1, interSideId1});
        //tr1->edgeIds[interSideId1] = pair;
        //tr2->edgeIds[interSideId2] = pair;
        resources.intersections.push(pair);

        // Check if adj triangle contain endId
        if (tr2->ids[(interSideId2 + 2) % 3] == endId)
            return;

        // Check if next edge intersects
        if (oxm::is_intersected(edge, getEdge(resources, tr2, (interSideId2 + 1) % 3)))
        {
            tr1 = tr2;
            interSideId1 = (interSideId2 + 1) % 3;
            interSideId2 = tr1->adjustedSideId[interSideId1];
            tr2 = tr1->adjusted[interSideId1];
            continue;
        }

        // Check if prev edge intersects
        if (oxm::is_intersected(edge, getEdge(resources, tr2, (interSideId2 + 2) % 3)))
        {
            tr1 = tr2;
            interSideId1 = (interSideId2 + 2) % 3;
            interSideId2 = tr1->adjustedSideId[interSideId1];
            tr2 = tr1->adjusted[interSideId1];
            continue;
        }

        assert(false);
    }
}

} // anon namespace

auto ox::TopoPlaneSurface::discretize(
        const std::vector<std::span<TopoPoint>>& iLoops,
        const std::function<void(const TopoPoint&, const TopoDir&)>& cbAddPoint,
        const std::function<void(uint32_t, uint32_t, uint32_t)>& cbAddTriangle) 
    const -> void
{
    size_t pointCount = 0;
    for (const auto& loop : iLoops)
        pointCount += loop.size();

    if (pointCount < 3 || iLoops[0].empty())
        return;

    DelaunayResources resources = 
    {
        .arena = ox::arena<DelaunayTriangle>(256),
        .points = std::vector<Point2DConnected>{},
        .trianglepoints = std::vector<DelaunayTriangle*>(pointCount),
        .leaves = std::vector<DelaunayTriangle*>(),
        .pairArena = ox::arena<AdjustedTrianglePair>(256),
        .intersections = std::queue<AdjustedTrianglePair*>(),
        .nonintersections = std::vector<AdjustedTrianglePair*>()
    };

    auto& points = resources.points;
    points.reserve(pointCount);
    // TODO: estimate more carefully leaves reserve
    resources.leaves.reserve(pointCount);

    auto bb = oxm::aabbd2::invalid();
    uint32_t offset = points.size();;
    for (const auto& loop : iLoops)
    {
        uint32_t loopSize = loop.size();
        for (uint32_t i = 0; i < loopSize; i++)
        {
            auto p = plane.parametric(loop[i]);
            bb = oxm::enlarge(bb, p);
            cbAddPoint(loop[i], plane.normal);
            points.push_back(Point2DConnected {
                    .point = p,
                    .next = offset + (i + 1) % loopSize,
                    .prev = offset + (i + (loopSize - 1)) % loopSize});
        }
        offset += loopSize;
    }
    auto root = resources.arena.alloc();
   
    // Take points count without supertrianle points
    uint32_t pointCountWSt = points.size();
    // Take points count for first (ie. external) loop
    uint32_t pointCountELp = iLoops[0].size();
    // Add three points for supertriangle
    for (size_t i = 0; i < 3; i++)
        points.emplace_back();
    //resources.points.resize(points.size());

    createSuperTriangle(bb, 
            points[pointCountWSt].point, 
            points[pointCountWSt + 1].point, 
            points[pointCountWSt + 2].point);

    resources.leaves.push_back(root);
    *root = DelaunayTriangle { .ids = {pointCountWSt, pointCountWSt + 1, pointCountWSt + 2}, .leafId = 0 };

    // Create non-constrained Delaunay triangulation
    for (size_t i = 0; i < pointCountWSt; i++)
    {
        const auto pair = getTriangleAtPoint(points, points[i].point, root);

        if (!pair.hasSecond())
            divideTriangleBy3(resources, i, pair.tr);
        else
            divide2TrianglesBy2(resources, i, pair);
    }

    for (auto t : resources.leaves)
    {
        if(t->ids[0] < pointCountWSt)
            resources.trianglepoints[t->ids[0]] = t;
        if(t->ids[1] < pointCountWSt)
            resources.trianglepoints[t->ids[1]] = t;
        if(t->ids[2] < pointCountWSt)
            resources.trianglepoints[t->ids[2]] = t;
    }

    // Create Constrained Delaunay triangulation
    for (uint32_t startId = 0; startId < pointCountWSt; startId++)
    {
        uint32_t endId = points[startId].next;
        const auto edge = oxm::LineSegment2D<double>
        {
            resources.points[startId].point,
            resources.points[endId].point
        };

        auto startPair = edgeOnPoint(resources, startId);
        if (!edgeExists(resources, startPair, endId))
        {
            startPair = firstIntersection(resources, startPair, edge);

            findIntersections(resources, startPair, edge, endId);

            while (!resources.intersections.empty())
            {
                const auto pair = resources.intersections.front();
                if (isConvex(resources, *pair))
                {
                    auto newPair = resources.pairArena.alloc();
                    bindEdge(newPair, swapInplace(*pair));
                    if (oxm::is_intersected(edge, getEdge(resources, newPair->tr, newPair->edgeId)))
                        resources.intersections.push(newPair);
                    else 
                        resources.nonintersections.push_back(newPair);
                }
                else
                {
                    resources.intersections.push(pair);
                }
                resources.intersections.pop();
            }

            while (!resources.nonintersections.empty())
            {
                auto pair = resources.nonintersections.back();
                resources.nonintersections.pop_back();
                refineIntersection(resources, *pair);
            }
        }

        resources.pairArena = ox::arena<AdjustedTrianglePair>(256);
    }

    for (auto t : resources.leaves)
    {
        const auto p1 = t->ids[0];
        const auto p2 = t->ids[1];
        const auto p3 = t->ids[2];
        const auto p1next = resources.points[p1].next;
        const auto p2next = resources.points[p2].next;
        const auto p3next = resources.points[p3].next;

        // TODO: maybe third and first check is implies with each other, so one of them can be removed
        // need to be checked with geometry with holes
        if
        (
            (p1next == p2 || p2next == p3 || p3next == p1) // check if triangle is not external triangle that seats in concavity
            && (p1 < pointCountWSt && p2 < pointCountWSt && p3 < pointCountWSt) // check if triangle is not contain points from supertriangle
            && (p1 < pointCountELp || p2 < pointCountELp || p3 < pointCountELp) // check if triangle contains at least one point from external loop
        )
            cbAddTriangle(p1, p2, p3);
    }
}
