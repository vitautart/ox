#pragma once

#include <oxmath.hpp>
#include <limits>
#include <optional>
#include <variant>

namespace oxm
{

template <oxm::RealField T>
inline constexpr T ANGLE_EPSILON = 4 * std::numeric_limits<T>::epsilon();

template <oxm::RealField T>
inline constexpr T DISTANCE_EPSILON = 4 * std::numeric_limits<T>::epsilon();

template <typename T> using Point2D = oxm::vec<T, 2>;
template <typename T> using Direction2D = oxm::vec<T, 2>;
template <typename T> using Point3D = oxm::vec<T, 3>;
template <typename T> using Direction3D = oxm::vec<T, 3>;

template <oxm::RealField T>
struct Line2D
{
    Point2D<T> point;
    Direction2D<T> dir;
    inline auto interpolate (T t) const noexcept -> Point2D<T>
    {
        return point + t * dir; 
    }
};

template <oxm::RealField T>
struct Line3D
{
    Point3D<T> point;
    Direction3D<T> dir;
    inline auto interpolate (T t) const noexcept -> Point3D<T>
    {
        return point + t * dir; 
    }
};

template <oxm::RealField T>
struct Circle2D
{
    Point2D<T> center;
    //Direction3D<T> xdir; = {1, 0}
    //Direction3D<T> ydir; = {0, 1}
    T radius;
    inline auto interpolate (T t) const noexcept -> Point2D<T>
    {
        return 
        {
            center[0] + radius * cos(t),
            center[1] + radius * sin(t)
        };
    }
};

template <oxm::RealField T>
struct Circle3D
{
    Point3D<T> center;
    Direction3D<T> xdir;
    Direction3D<T> ydir;
    T radius;
    inline auto interpolate (T t) const noexcept -> Point3D<T>
    {
        return center + radius * cos(t) * xdir + radius * sin(t) * ydir; 
    }
};



template <oxm::RealField T>
struct Ellipse2D
{
    Point2D<T> center;
    Direction2D<T> xradius;
    Direction2D<T> yradius;
    inline auto interpolate (T t) const noexcept -> Point2D<T>
    {
        return center + cos(t) * xradius + sin(t) * yradius; 
    }
};

template <oxm::RealField T>
struct Ellipse3D
{
    Point3D<T> center;
    Direction3D<T> xradius;
    Direction3D<T> yradius;
    inline auto interpolate (T t) const noexcept -> Point3D<T>
    {
        return center + cos(t) * xradius + sin(t) * yradius; 
    }
};

template <oxm::RealField T>
struct CircleSegment3D
{
    Circle3D<T> circle;
    T p1, p2;

    inline auto start() const noexcept -> Point3D<T> { return circle.interpolate(p1); }
    inline auto end() const noexcept -> Point3D<T> { return circle.interpolate(p2); }
    inline auto interpolate (T t) const noexcept -> Point3D<T>
    {
        return circle.interpolate(t); 
    }
};

template <oxm::RealField T>
struct EllipticSegment3D
{
    Ellipse3D<T> ellipse;
    T p1, p2;
    inline auto start() const noexcept -> Point3D<T> { return ellipse.interpolate(p1); }
    inline auto end() const noexcept -> Point3D<T> { return ellipse.interpolate(p2); }
    inline auto interpolate (T t) const noexcept -> Point3D<T> { return ellipse.interpolate(t); }
};

template <oxm::RealField T>
struct LineSegment2D
{
    Point2D<T> p1;
    Point2D<T> p2;
};

template <oxm::RealField T>
struct LineSegment3D
{
    Point3D<T> p1;
    Point3D<T> p2;
};

template <oxm::RealField T>
struct Quadrangle2D
{    
    Point2D<T> points[4];
};

template <oxm::RealField T>
struct Plane
{
    Point3D<T> point;
    Direction3D<T> normal;
    Direction3D<T> xdir;
    Direction3D<T> ydir;
    inline auto interpolate (T t, T s) const noexcept -> Point3D<T>
    {
        return point + t * xdir + s * ydir;
    }

    // Parametric returns surface point in surface space
    // It is revert of interpolation
    inline auto parametric(const Point3D<T>& p) const noexcept -> Point2D<T>
    {
        const auto d = p - point;
        return 
        {
            oxm::dot(d, xdir),
            oxm::dot(d, ydir)
        };
    }
};

template <oxm::RealField T>
struct Sphere
{
    Point3D<T> center;
    //oxm::vec<T, 2> u; = {1, 0, 0}
    //oxm::vec<T, 2> v; = {0, 1, 0}
    //oxm::vec<T, 2> w; = {0, 0, 1}
    T radius;
    inline auto interpolate (T t, T s) const noexcept -> Point3D<T>
    {
        return 
        {
            center[0] + radius * sin(s) * cos(t),
            center[1] + radius * sin(s) * sin(t),
            center[2] + radius * sin(s),
        };
    }
};

template <oxm::RealField T>
struct Cylinder
{
    Point3D<T> center;
    Direction3D<T> xdir;
    Direction3D<T> ydir;
    Direction3D<T> zdir;
    T radius;
    inline auto interpolate (T angle, T height) const noexcept -> Point3D<T>
    {
        return center + radius * cos(angle) * xdir + radius * sin(angle) * ydir + height * zdir;
    }
};

template <oxm::RealField T>
struct Cone
{
    Point3D<T> apex;
    Direction3D<T> xdir;
    Direction3D<T> ydir;
    Direction3D<T> zdir;
    T slope; // radius / height
    inline auto interpolate (T angle, T height) const noexcept -> Point3D<T>
    {
        T radius = height * slope;
        return apex + radius * cos(angle) * xdir + radius * sin(angle) * ydir + height * zdir;
    }
};

template <typename T, size_t capacity>
struct Array
{
    size_t size;
    T data[capacity];
};

enum class InterType
{
    EMPTY,
    POINT,
    LINE,
    LINES,
    CIRCLE,
    ELLIPSE,
    // NURBS
};

template <typename T1, typename T2, typename T3>
struct Intersection3
{
    InterType type;
    union
    {
        T1 data1;
        T2 data2;
        T3 data3;
    } data;
};

template <typename T1, typename T2>
struct Intersection2
{
    InterType type;
    union 
    {
        T1 data1;
        T2 data2;
    } data;
};

template <typename T1>
struct Intersection1
{
    InterType type;
    T1 data;
};

template <oxm::RealField T>
inline auto is_intersected(const LineSegment2D<T>& l1, const LineSegment2D<T>& l2) noexcept -> bool
{
    auto p11p12 = l1.p2 - l1.p1;
    auto p21p22 = l2.p2 - l2.p1;
    return  ((oxm::cross(p11p12, l2.p2 - l1.p2) * oxm::cross(p11p12, l2.p1 - l1.p2)) < 0) && 
            ((oxm::cross(p21p22, l1.p2 - l2.p2) * oxm::cross(p21p22, l1.p1 - l2.p2)) < 0);
}

template <oxm::RealField T>
inline auto sgndistance(const Plane<T>& plane, const Point3D<T>& point) noexcept -> T
{
    return oxm::dot(plane.normal, point - plane.point);
}

template <oxm::RealField T>
inline auto absdistance(const Plane<T>& plane, const Point3D<T>& point) noexcept -> T
{
    return std::abs(oxm::dot(plane.normal, point - plane.point));
}

template <oxm::RealField T>
inline auto absdistance(const Line3D<T>& line, const Point3D<T>& point) noexcept -> T
{
    return oxm::length(oxm::cross(line.point - point, line.point + line.dir - point));
}

template <oxm::RealField T>
inline auto coincide(const Plane<T>& plane, const Point3D<T>& point) noexcept -> T
{
    return absdistance(plane, point) < DISTANCE_EPSILON<T>;
}

template <oxm::RealField T>
inline auto coincide(const Line3D<T>& line, const Point3D<T>& point) noexcept -> T
{
    return absdistance(line, point) < DISTANCE_EPSILON<T>;
}

template <oxm::RealField T>
inline auto coincide(const Plane<T>& plane, const Line3D<T>& line) noexcept -> T
{
    // TODO:
}

template <oxm::RealField T>
inline auto project(const Line3D<T>& line, const Point3D<T>& point) noexcept -> Point3D<T>
{
    return line.point + oxm::dot(line.dir, point - line.point) * line.dir;
}

template <oxm::RealField T>
inline auto project(const Plane<T>& plane, const Point3D<T>& point) noexcept -> Point3D<T>
{
    return point + oxm::dot(plane.dir, plane.point - point) * plane.normal;
}

template <oxm::RealField T>
inline auto fintersect(const Line3D<T>& line, const Plane<T>& plane) noexcept -> Point3D<T>
{
    return line.point + line.dir * (oxm::dot(plane.point - line.point, plane.normal) / oxm::dot(line.dir, plane.normal));
}

// Intersection point can be outside segmnents, but lies on lines spawned by segments
template <oxm::RealField T>
inline auto fintersect(const LineSegment2D<T>& line1, const LineSegment2D<T>& line2) noexcept -> Point2D<T>
{
    const auto x1x2 = line1.p1[0] - line1.p2[0];
    const auto x3x4 = line2.p1[0] - line2.p2[0];
    const auto y1y2 = line1.p1[1] - line1.p2[1];
    const auto y3y4 = line2.p1[1] - line2.p2[1];
    const auto one_over_denom = 1 / (x1x2 * y3y4 - x3x4 * y1y2); 
    const auto x1y2y1x2 = line1.p1[0] * line1.p2[1] - line1.p1[1] * line1.p2[0];
    const auto x3y4y3x4 = line2.p1[0] * line2.p2[1] - line2.p1[1] * line2.p2[0];
    return 
    {
        (x1y2y1x2 * x3x4 - x3y4y3x4 * x1x2) * one_over_denom,
        (x1y2y1x2 * y3y4 - x3y4y3x4 * y1y2) * one_over_denom
    };
}

template <oxm::RealField T>
auto intersect2Dlike(const Circle3D<T>& circle, const Line3D<T>& line) noexcept -> Array<Point3D<T>, 2>
{
    auto centeronline = project(line, circle.center);
    auto length = oxm::length(centeronline - circle.center);

    if (length > circle.radius)
        return {.size = 0};
    else if (length < DISTANCE_EPSILON<T>)
        return {.size = 1, .data = {centeronline}};

    auto d = std::sqrt(circle.center * circle.center - length * length);

    return 
    {
        .size = 2,
        .data = { centeronline + d * line.dir, centeronline - d * line.dir }
    };
}

template <oxm::RealField T>
auto intersect(const Circle3D<T>& circle, const Line3D<T>& line) noexcept -> Array<Point3D<T>, 2>
{
    // Check if line and circle on the same plane
    if (absdistance(
                Plane<T> {.point = circle.center, .normal = oxm::cross(circle.xdir, circle.ydir)},
                line) > DISTANCE_EPSILON<T>)
        return {.size = 0};

    return intersect2Dlike(circle, line);

}

template <oxm::RealField T>
auto intersect(const Line3D<T>& line, const Plane<T>& plane) noexcept -> Intersection1<Point3D<T>>
{
    auto ndotl = oxm::dot(line.dir, plane.normal);
    if (std::abs(ndotl) < ANGLE_EPSILON<T>)
        return { InterType::EMPTY };
    return { InterType::POINT, line.point + line.dir * (oxm::dot(plane.point - line.point, plane.normal) / ndotl) };
}

template <oxm::RealField T>
inline auto fintersect(const Plane<T>& p1, const Plane<T>& p2) noexcept -> Line3D<T>
{
    auto dir = oxm::norm(oxm::cross(p1.normal, p2.normal));

    if (absdistance(p2, p1.point) > DISTANCE_EPSILON<T>)
        return 
        { 
            .pos = fintersect(Line3D<T>{.point = p1.point, .dir = oxm::cross(p1.normal, dir)}, p2), 
            .dir = dir 
        };
    else
        return { .pos = p1.point, .dir = dir };
}

template <oxm::RealField T>
auto intersect(const Plane<T>& p1, const Plane<T>& p2) noexcept -> Intersection1<Line3D<T>>
{
    Direction3D<T> dir;
    if(oxm::norm(oxm::cross(p1.normal, p2.normal), dir, ANGLE_EPSILON<T>))
        return {InterType::EMPTY};

    if (absdistance(p2, p1.point) > DISTANCE_EPSILON<T>)
        return 
        {
            .type = InterType::LINE,
            .data = 
            {
                .pos = fintersect(Line3D<T>{.point = p1.point, .dir = oxm::cross(p1.normal, dir)}, p2),
                .dir = dir
            }
        };
    else
        return 
        {
            .type = InterType::LINE,
            .data = { .pos = p1.point, .dir = dir }
        };
}

/*template <oxm::RealField T>
auto intersect(const Cylinder<T>& cylinder, const Line3D<T>& line) noexcept -> Array<Point3D<T>, 2>
{

}*/

// All directions should be normalized
// we depend on it.
template <oxm::RealField T>
auto intersect(const Plane<T>& plane, const Cylinder<T>& cylinder) noexcept -> 
    Intersection3<Array<Line3D<T>, 2>, Ellipse3D<T>, Circle3D<T>>
{
    auto ndotl = oxm::dot(plane.normal, cylinder.zdir);
    // plane is parallel to cylinder axis
    if (ndotl < ANGLE_EPSILON<T>)
    {
        Plane<T> baseplane = 
        { 
            .point = cylinder.center, 
            .normal = cylinder.zdir, 
            .xdir = cylinder.xdir, 
            .ydir = cylinder.ydir 
        };
        auto line =  fintersect(plane, baseplane);
        Circle3D<T> basecircle = 
        {
            .center = cylinder.center,
            .xdir = cylinder.xdir, 
            .ydir = cylinder.ydir,
            .radius = cylinder.radius
        };
        auto points = intersect2Dlike(basecircle, line);
        if (points.size == 0)
            return { .type = InterType::EMPTY };
        else if (points.size == 1)
            return 
            {
                .type = InterType::LINES,
                .data = { .data1 = { .size = points.size, .data = { 
                        Line3D<T>{points.data[0], cylinder.zdir}    
                }}}
            };
        else if (points.size == 2)
            return 
            {
                .type = InterType::LINES,
                .data = { .data1 = { .size = points.size, .data = { 
                        Line3D<T>{points.data[0], cylinder.zdir},
                        Line3D<T>{points.data[1], cylinder.zdir}
                }}}
            };
    }
    else if (std::abs(std::abs(ndotl) - static_cast<T>(1.0)) < ANGLE_EPSILON<T>)
    {
        return 
        {
            .type = InterType::CIRCLE,
            .data = { .data3 = 
                {
                    .center = project(plane, cylinder.center),
                    .xdir = cylinder.xdir,
                    .ydir = cylinder.ydir,
                    .radius = cylinder.radius
                }}
        };
    }
    else // ellipse
    {
        auto center = project(plane, cylinder.center);
        auto xaxis = oxm::norm(oxm::cross(plane.normal, cylinder.zdir));
        auto yaxis = oxm::cross(plane.normal, xaxis);
        auto yradius = std::abs(cylinder.radius / ndotl);
        return 
        {
            .type = InterType::ELLIPSE,
            .data = { .data2 = 
                {
                    .center = center,
                    .xradius = cylinder.radius * xaxis,
                    .yradius = yradius * yaxis,
                }}
        };
    }
}

template <oxm::RealField T>
auto intersect(const Plane<T>& plane, const Cone<T>& cone) noexcept -> 
    Intersection3<Array<Line3D<T>, 2>, Ellipse3D<T>, Circle3D<T>>
{

}

// Very specific function, usefull for Delaunay triangulation
// Checks if point pTest is inside circumcircle of triangle with pTri1, pTri2 and pTri3 vertices
// Vertices of triangle should be in counterclockwise order.
template <oxm::RealField T>
inline auto circumcircled(const Point2D<T>& pTri1, const Point2D<T>& pTri2, 
        const Point2D<T>& pTri3, const Point2D<T>& pTest) noexcept -> bool
{
    const auto pTestSq = pTest * pTest;
    return oxm::det(oxm::md3 {
                pTri1[0] - pTest[0], pTri2[0] - pTest[0], pTri3[0] - pTest[0],
                pTri1[1] - pTest[1], pTri2[1] - pTest[1], pTri3[1] - pTest[1],
                
                pTri1[0] * pTri1[0] - pTestSq[0] + pTri1[1] * pTri1[1] - pTestSq[1],
                pTri2[0] * pTri2[0] - pTestSq[0] + pTri2[1] * pTri2[1] - pTestSq[1],
                pTri3[0] * pTri3[0] - pTestSq[0] + pTri3[1] * pTri3[1] - pTestSq[1],
            }) > 0;
}

template <oxm::RealField T>
inline auto barycentrics(const Point2D<T>& pTri1, const Point2D<T>& pTri2, 
        const Point2D<T>& pTri3, const Point2D<T>& p) noexcept -> oxm::v3<T>
{
    const auto s1 = pTri2 - pTri1;
    const auto d = 1 / oxm::cross(s1, pTri3 - pTri1);
    const oxm::v3<T> result;
    result[0] = oxm::cross(s1, p - pTri1) * d;
    result[1] = oxm::cross(pTri3 - pTri1, p - pTri2) * d;
    result[2] = 1 - result[0] - result[1];
    return result;
}

// Degenerated cases not considered convex
template <oxm::RealField T>
inline auto is_convex(const Quadrangle2D<T>& q) noexcept -> bool
{
    const auto s1 = q.points[1] - q.points[0];
    const auto s2 = q.points[2] - q.points[1];
    const auto s3 = q.points[3] - q.points[2];
    const auto s4 = q.points[0] - q.points[3];
    const auto cr1 = oxm::cross(s1, s2);
    const auto cr2 = oxm::cross(s2, s3);
    const auto cr3 = oxm::cross(s3, s4);
    const auto cr4 = oxm::cross(s4, s1);

    return  (cr1 > 0 && cr2 > 0 && cr3 > 0 && cr4 > 0) || 
            (cr1 < 0 && cr2 < 0 && cr3 < 0 && cr4 < 0);
}

}
