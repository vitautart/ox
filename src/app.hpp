#include "camera.hpp"
#include "definitions.hpp"
#include <GLFW/glfw3.h>
#include <render/render.hpp>
#include <render/renderwindow.hpp>
#include <render/renderview3d.hpp>
#include <ui/ui.hpp>
#include <brep.hpp>
#include <vector>
#include <vulkan/vulkan_core.h>

namespace ox
{

class App;
class AppWindow;
class AppDocument;

class AppDocument
{
public:
    AppDocument() = delete;
    AppDocument(App* app);
    AppDocument(App* app, ox::BRep3D&& brep);
    CTORS_COPY_DELETE(AppDocument);
    CTORS_MOVE_DEFAULT(AppDocument);
    auto getMeshSolid() const -> const ox::BufferMesh3DSolid&
    {
        return mMeshSolid;
    }
    auto getMeshWire() const -> const ox::BufferMesh3DWire&
    {
        return mMeshWire;
    }
    auto setModel(ox::BRep3D&& brep) -> void { mModel = std::move(brep); mModelModified = true; }
    auto getBRep3D() const -> const ox::BRep3D& { return mModel; }
    auto modifyBRep3D(const std::function<bool(ox::BRep3D&)>& modifyFn) 
    {
        mModelModified = modifyFn(mModel);
    }
    auto load(VkCommandBuffer) -> bool;
private:
    App* mApp;
    ox::BRep3D mModel;
    // TODO: THIS TWO here is super dangerous
    // it is possible that we destroy document while rendering it through this
    // buffers, in this situation we can take hard times
    // probably make them shared_ptr and cache them neer rendering.
    ox::BufferMesh3DSolid mMeshSolid;
    ox::BufferMesh3DWire mMeshWire;
    bool mModelModified;
};

enum class AppInputState
{
    NONE,
    CAM_PAN,
    CAM_ROT,
    //UI_HOVER,
    UI_INPUT,
};

enum class AppUIHoverState
{
    NONE,
    UI_HOVER,
};

class AppWindow
{
public:
    AppWindow() = delete;
    AppWindow(App* app, uint32_t w, uint32_t h, bool& res);
    CTORS_COPY_DELETE(AppWindow);
    CTORS_MOVE_DELETE(AppWindow);
    ~AppWindow() noexcept;
    auto prepare() -> void;
    auto submit() -> void;
    auto isClosing() -> bool;
private:
    static auto fnTextInputExterior(GLFWwindow* window, unsigned int codepoint) -> void;
    static auto fnKeyPressExterior(GLFWwindow* window, int key, int scancode, int action, int mods) -> void;
    static auto fnMousePressExterior(GLFWwindow* window, int button, int action, int mods) -> void;
    static auto fnMouseMoveExterior(GLFWwindow* window, double x, double y) -> void;
    static auto fnScrollExterior(GLFWwindow* window, double xOffset, double yOffset) -> void;
    static auto fnResizeExterior(GLFWwindow* window, int width,int height) -> void;

    auto fnTextInputInterior(unsigned int codepoint) -> void;
    auto fnKeyPressInterior(int key, int scancode, int action, int mods) -> void;
    auto fnMousePressInterior(int button, int action, int mods) -> void;
    auto fnMouseMoveInterior(double x, double y) -> void;
    auto fnScrollInterior(double xOffset, double yOffset) -> void;
    auto fnResizeInterior(int width,int height) -> void;

private:
    App* mApp;
    GLFWwindow* mGLFWWindow;
    ox::RenderWindow mRenderWindow;
    ox::RenderView3D mRenderView3D;
    ox::RenderShape2D mRenderShape2D;
    ox::RenderText2D mRenderText2D;
    ox::EditorCamera3D mCamera;
    VkRect2D mView3DRect;
    ox::ui::VScrolCanvas mLeftPanel;
    AppInputState mInputState;
    AppUIHoverState mUIHoverState;
    std::pair<ox::ui::InputElement*, ox::ui::LinearCanvas*>  mCurrentFocus;
};

class App
{
public:
    explicit App(bool& res);
    ~App() noexcept;
    auto run () -> void;
    auto getRender() -> ox::Render& { return mRender; }
    auto getRender() const -> const ox::Render& { return mRender;}
    auto getDocuments() -> std::span<AppDocument> { return mDocuments; } 
    auto getDocuments() const -> std::span<const AppDocument> { return mDocuments; } 
    auto addDocument(BRep3D&& brep) -> void 
    {
        mDocuments.emplace_back(this, std::move(brep));
    }

private:
    // Render should be lastly destroyed
    ox::Render mRender;
    std::vector<std::unique_ptr<AppWindow>> mWindows;
    std::vector<AppDocument> mDocuments;
};

}
