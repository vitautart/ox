#include <cstdint>
#include <cassert>
#include <definitions.hpp>
#include <iterator>
#include <render/rendercommon.hpp>
#include <vulkan/vulkan_core.h>

#if defined(OX_LINUX_OS)
#define GLFW_EXPOSE_NATIVE_X11
#endif

#include <X11/Xlib.h>
#include <render/renderwindow.hpp> 
#include <GLFW/glfw3native.h>

#if defined(OX_LINUX_OS)
#include <vulkan/vulkan_xlib.h>
#endif

#include <algorithm>

auto ox::RenderWindow::create(const RenderWindowCreateInfo& iCreateInfo,
        const std::function<void(VkFormat, VkColorSpaceKHR)>& setColorFormatAndSpaceFn) -> bool
{
    if (mInitialized)
        return true;
    else
        mInitialized = true;

    mCreateInfo = iCreateInfo;

    CALL_CHECK_DESTROY_RETURN(createSurface());
    CALL_CHECK_DESTROY_RETURN(configureSwapchain());
    CALL_CHECK_DESTROY_RETURN(createSwapchain());
    CALL_CHECK_DESTROY_RETURN(createScreenTarget());
    CALL_CHECK_DESTROY_RETURN(createSyncData());
    CALL_CHECK_DESTROY_RETURN(createCommandPool());
    CALL_CHECK_DESTROY_RETURN(createStandardCommandBuffers());
    CALL_CHECK_DESTROY_RETURN(createTransitionalCommandBuffers());

    if (setColorFormatAndSpaceFn != nullptr)
        setColorFormatAndSpaceFn(mConfig.colorFormat, mConfig.colorSpace);

    return true;
}

auto ox::RenderWindow::destroy() -> void
{
    auto* r = mCreateInfo.render;
    if (!mInitialized)
        return;

    if (!mToColorCommands.empty())
        vkFreeCommandBuffers(r->getDevice(), mCommandPoolGraphic, 
                mToColorCommands.size(), mToColorCommands.data());
    mToColorCommands.clear();

    if (!mToPresentCommands.empty())
        vkFreeCommandBuffers(r->getDevice(), mCommandPoolGraphic, 
                mToPresentCommands.size(), mToPresentCommands.data());   
    mToPresentCommands.clear();

    if (!mStandardCommands.empty())
        vkFreeCommandBuffers(r->getDevice(), mCommandPoolGraphic, 
                mStandardCommands.size(), mStandardCommands.data());   
    mStandardCommands.clear();
     
    if (mCommandPoolGraphic)
        vkDestroyCommandPool(r->getDevice(), mCommandPoolGraphic, nullptr);

    for (auto semaphore : mSemaphoresCommandDone)
        vkDestroySemaphore(r->getDevice(), semaphore, nullptr);
    mSemaphoresCommandDone.clear();

    for (auto semaphore : mSemaphoresPresentDone)
        vkDestroySemaphore(r->getDevice(), semaphore, nullptr);
    mSemaphoresPresentDone.clear();

    for (auto fence : mFencesCommandDone)
        vkDestroyFence(r->getDevice(), fence, nullptr);
    mFencesCommandDone.clear();

    destroyScreenTarget();

    if (mSwapchain)
        vkDestroySwapchainKHR(r->getDevice(), mSwapchain, nullptr);
    mSwapchain = VK_NULL_HANDLE;

    if (mSurface)
        vkDestroySurfaceKHR(r->getInstance(), mSurface, nullptr);
    mSurface = VK_NULL_HANDLE;

    mInitialized = false;
}

auto ox::RenderWindow::createSurface() -> bool
{
    if constexpr (ox::IS_LINUX_OS)
    {
        auto display = glfwGetX11Display();
        auto window =  glfwGetX11Window(mCreateInfo.window);
        
        VkXlibSurfaceCreateInfoKHR surfaceInfo = 
        {
            .sType = VK_STRUCTURE_TYPE_XLIB_SURFACE_CREATE_INFO_KHR,
            .pNext = nullptr,
            .flags = 0,
            .dpy = display,
            .window = window
        }; 
        auto result = vkCreateXlibSurfaceKHR(mCreateInfo.render->getInstance(), &surfaceInfo, nullptr, &mSurface);
        CHECK_AND_RETURN(result, "[ERROR] Surface creation failed: %i\n");

        return true;
    }
    else
    {
        static_assert(IS_LINUX_OS, "Not supported OS.");
    }
}

// TODO: on linux with VK_PRESENT_MODE_FIFO_KHR there are visible slow-mo effect
// maybe we can investigate better this issue, or switch for linux to Immediate mode.
auto ox::RenderWindow::configureSwapchain() -> bool
{
    auto* r = mCreateInfo.render;

    if (r->hasTargetColorFormat() && r->hasTargetColorSpace())
    {
        mConfig.colorFormat = r->getTargetColorFormat();
        mConfig.colorSpace = r->getTargetColorSpace();
    }
    else
    {
        std::vector<VkSurfaceFormatKHR> formats;
        vkenum(vkGetPhysicalDeviceSurfaceFormatsKHR, formats, r->getPhysicalDevice(), mSurface);

        bool found = false;
        for(const auto& format : formats)
        {
            if (format.format == VK_FORMAT_B8G8R8A8_SRGB 
                && format.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR)
            {
                mConfig.colorFormat = format.format;
                mConfig.colorSpace = format.colorSpace;
                found = true;
                break;
            }
        }
        if (!found)
        {
            printf("[ERROR] Couldn't find needed surface format\n");
            return false;
        }
    }

    if (r->hasTargetDepthFormat())
    {
        mConfig.depthFormat = r->getTargetDepthFormat();
    }
    else
    {
        printf("[ERROR] Couldn't find needed depth format\n");
        return false;
    }

    std::vector<VkPresentModeKHR> modes;
    vkenum(vkGetPhysicalDeviceSurfacePresentModesKHR, modes, r->getPhysicalDevice(), mSurface);
    {
    
        auto found = false;
        for (const auto& mode : modes)
        {
            if (mode == VK_PRESENT_MODE_MAILBOX_KHR 
                || mode == VK_PRESENT_MODE_FIFO_KHR)
            //if(mode == VK_PRESENT_MODE_IMMEDIATE_KHR)
            {
                mConfig.mode = mode;
                found = true;
                break;
            }
        }
        if (!found)
        {
            printf("[ERROR] Couldn't find needed present mode\n");
            return false;
        }
    }

    return true;
}

auto ox::RenderWindow::createSwapchain() -> bool
{
    auto* r = mCreateInfo.render;
    int width = 0, height = 0;
    glfwGetFramebufferSize(mCreateInfo.window, &width, &height);
    if (width == 0 || height == 0)
    {
        printf("[ERROR] Couldn't get size of window\n");
        return false;
    }
    VkSurfaceCapabilitiesKHR capabilities;
    auto result = vkGetPhysicalDeviceSurfaceCapabilitiesKHR(r->getPhysicalDevice(), mSurface, &capabilities);

    CHECK_AND_RETURN(result, "[ERROR] Couldn't get data from device: %i\n");

    if (capabilities.currentExtent.width != UINT32_MAX)
        mExtent = capabilities.currentExtent;
    else
        mExtent = 
        { 
            .width = std::clamp(static_cast<uint32_t>(width), 
                    capabilities.minImageExtent.width, capabilities.maxImageExtent.width),
            .height = std::clamp(static_cast<uint32_t>(height), 
                    capabilities.minImageExtent.height, capabilities.maxImageExtent.height)
        };
    auto minImageCount = std::clamp(capabilities.minImageCount+1, 
            capabilities.minImageCount,
            capabilities.maxImageCount > 0 ? capabilities.maxImageCount : 256);

    bool graphicPresentSame = mCreateInfo.gfxCompQueueFamily == mCreateInfo.presentQueueFamily;
    uint32_t indices[2] = {mCreateInfo.gfxCompQueueFamily, mCreateInfo.presentQueueFamily};


    VkSwapchainCreateInfoKHR swapchainInfo = 
    {
        .sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
        .pNext = nullptr,
        .flags = 0,
        .surface = mSurface,
        .minImageCount = minImageCount,
        .imageFormat = mConfig.colorFormat,
        .imageColorSpace = mConfig.colorSpace,
        .imageExtent = mExtent,
        .imageArrayLayers = 1,
        .imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
        .imageSharingMode = graphicPresentSame ? VK_SHARING_MODE_EXCLUSIVE : VK_SHARING_MODE_CONCURRENT,
        .queueFamilyIndexCount = graphicPresentSame ? 0u : 2u,
        .pQueueFamilyIndices = graphicPresentSame ? nullptr : indices,
        .preTransform = capabilities.currentTransform,
        .compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
        .presentMode = mConfig.mode,
        .clipped = VK_TRUE,
        //.oldSwapchain = mSwapchain.swapchain // NOT SURE ABOUT THIS
        .oldSwapchain = VK_NULL_HANDLE
    };

    result = vkCreateSwapchainKHR(r->getDevice(), &swapchainInfo, nullptr, &mSwapchain);
    CHECK_AND_RETURN(result, "[ERROR] Swapchain creation failed: %i\n");

    return true;
}

auto ox::RenderWindow::createScreenTarget() -> bool
{
    auto* r = mCreateInfo.render;
    std::vector<VkImage> swapchainImages;
    ox::vkenum(vkGetSwapchainImagesKHR, swapchainImages, r->getDevice(), mSwapchain);

    VkImageCreateInfo imageInfo = 
    {
        .sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .imageType = VkImageType::VK_IMAGE_TYPE_2D,
        .extent = { mExtent.width, mExtent.height, 1}, 
        .mipLevels = 1,
        .arrayLayers = 1,
        .samples = r->getSamplesCount(),
        .tiling = VkImageTiling::VK_IMAGE_TILING_OPTIMAL,
        .sharingMode = VkSharingMode::VK_SHARING_MODE_EXCLUSIVE,
        .queueFamilyIndexCount = 0,
        .pQueueFamilyIndices = nullptr,
        .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED
    };

    VkImageViewCreateInfo viewInfo = 
    {
        .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .viewType = VK_IMAGE_VIEW_TYPE_2D,
        .components = RGBA_COMPONENT_MAPPING_STANDART,
        .subresourceRange = 
        {
            .baseMipLevel = 0,
            .levelCount = 1,
            .baseArrayLayer = 0,
            .layerCount = 1
        }
    };

    for (auto image : swapchainImages)
    {
        viewInfo.image = VK_NULL_HANDLE; // will be added inside create method
        viewInfo.format = mConfig.colorFormat;
        viewInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
 
        Image imageData; 
        auto result = imageData.create(image, ImageSource::SWAPCHAIN, viewInfo,
                r->getAllocator(), r->getDevice());
        if (result != VK_SUCCESS)
        {
            printf("[ERROR] Screen target swapchain image creation failed\n");
            return false;
        }

        if (r->getSamplesCount() == VkSampleCountFlagBits::VK_SAMPLE_COUNT_1_BIT)
            mScreenTarget.colorImages.push_back(imageData);
        else
            mScreenTarget.resolveImages.push_back(imageData);
    }

    for (size_t i = 0; i < swapchainImages.size(); i++)
    {
        if (r->getSamplesCount() == VkSampleCountFlagBits::VK_SAMPLE_COUNT_1_BIT)
            break;

        imageInfo.format = mConfig.colorFormat;
        imageInfo.usage = VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT | VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;

        viewInfo.image = VK_NULL_HANDLE; // will be added inside create method
        viewInfo.format = mConfig.colorFormat;
        viewInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
       
        Image imageData = {}; 
        auto result = imageData.create(imageInfo, viewInfo, 
                r->getAllocator(), r->getDevice());
        if (result != VK_SUCCESS)
        {
            printf("[ERROR] Screen target color image creation failed\n");
            return false;
        }

        mScreenTarget.colorImages.push_back(imageData);
    }

    for (size_t i = 0; i < swapchainImages.size(); i++)
    {
        imageInfo.format = mConfig.depthFormat;
        imageInfo.usage = VkImageUsageFlagBits::VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;

        viewInfo.image = VK_NULL_HANDLE; // will be added inside create method
        viewInfo.format = mConfig.depthFormat;
        viewInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;

        Image imageData = {}; 
        auto result = imageData.create(imageInfo, viewInfo, 
                r->getAllocator(), r->getDevice());
        if (result != VK_SUCCESS)
        {
            printf("[ERROR] Screen target depth image creation failed\n");
            return false;
        }
        
        mScreenTarget.depthImages.push_back(imageData);
    }

    mScreenTarget.extent = mExtent;
    mScreenTarget.samples = r->getSamplesCount();
    return true;
}

auto ox::RenderWindow::createSyncData() -> bool
{
    auto* r = mCreateInfo.render;
    VkSemaphoreCreateInfo infoSem = 
    {
        .sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0
    };
    VkFenceCreateInfo infoFen = 
    {
        .sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
        .pNext = nullptr,
        .flags = VK_FENCE_CREATE_SIGNALED_BIT
    };
    for (size_t i = 0; i < MAX_CONCURRENT_FRAMES; i++)
    {
        VkSemaphore item;
        auto result = vkCreateSemaphore(r->getDevice(), &infoSem, nullptr, &item);
        CHECK_AND_RETURN(result, "[ERROR] Semaphore creation failed: %i\n");
        mSemaphoresCommandDone.push_back(item);
    }

    for (size_t i = 0; i < MAX_CONCURRENT_FRAMES; i++)
    {
        VkSemaphore item;
        auto result = vkCreateSemaphore(r->getDevice(), &infoSem, nullptr, &item);
        CHECK_AND_RETURN(result, "[ERROR] Semaphore creation failed: %i\n");
        mSemaphoresPresentDone.push_back(item);
    }

    for (size_t i = 0; i < MAX_CONCURRENT_FRAMES; i++)
    {
        VkFence item;
        auto result = vkCreateFence(r->getDevice(), &infoFen, nullptr, &item);
        CHECK_AND_RETURN(result, "[ERROR] Fence creation failed: %i\n");
        mFencesCommandDone.push_back(item);
    }
    return true;
}

auto ox::RenderWindow::createCommandPool() -> bool
{    
    VkCommandPoolCreateInfo poolInfo = 
    {
        .sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
        .pNext = nullptr,
        .flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT,
        .queueFamilyIndex = mCreateInfo.gfxCompQueueFamily
    };
    auto result = vkCreateCommandPool(mCreateInfo.render->getDevice(), &poolInfo, nullptr, &mCommandPoolGraphic);
    CHECK_AND_RETURN(result, "[ERROR] Command buffer pool creation failed: %i\n");

    return true;
}

auto ox::RenderWindow::createStandardCommandBuffers() -> bool
{
    VkCommandBufferAllocateInfo info = 
    {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
        .pNext = nullptr,
        .commandPool = mCommandPoolGraphic,
        .level = VkCommandBufferLevel::VK_COMMAND_BUFFER_LEVEL_PRIMARY,
        .commandBufferCount = MAX_CONCURRENT_FRAMES
    };

    std::vector<VkCommandBuffer> commandBuffers; commandBuffers.resize(info.commandBufferCount);
    auto result = vkAllocateCommandBuffers(mCreateInfo.render->getDevice(), &info, commandBuffers.data());
    CHECK_AND_RETURN(result, "[ERROR] Command buffer allocation failed: %i\n");
    mStandardCommands = std::move(commandBuffers);

    return true;
}

auto ox::RenderWindow::createTransitionalCommandBuffers() -> bool
{
    VkCommandBufferAllocateInfo info = 
    {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
        .pNext = nullptr,
        .commandPool = mCommandPoolGraphic,
        .level = VkCommandBufferLevel::VK_COMMAND_BUFFER_LEVEL_PRIMARY,
        .commandBufferCount = (uint32_t)mScreenTarget.size()
    };
    {
        std::vector<VkCommandBuffer> commandBuffers; commandBuffers.resize(info.commandBufferCount);
        auto result = vkAllocateCommandBuffers(mCreateInfo.render->getDevice(), &info, commandBuffers.data());
        CHECK_AND_RETURN(result, "[ERROR] Command buffer allocation failed: %i\n");
        mToColorCommands = std::move(commandBuffers);
    }
    {
        std::vector<VkCommandBuffer> commandBuffers; commandBuffers.resize(info.commandBufferCount);
        auto result = vkAllocateCommandBuffers(mCreateInfo.render->getDevice(), &info, commandBuffers.data());
        CHECK_AND_RETURN(result, "[ERROR] Command buffer allocation failed: %i\n");
        mToPresentCommands = std::move(commandBuffers);
    }

    recordTransitionalCommandBuffers();
    return true;
}

auto ox::RenderWindow::recordTransitionalCommandBuffers() -> void
{
    VkCommandBufferBeginInfo cbBeginInfo = { .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO };

    {
        VkImageMemoryBarrier imageBarrier = 
        {
            .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
            .dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
            .oldLayout = VK_IMAGE_LAYOUT_UNDEFINED,
            .newLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
            .subresourceRange = 
            {
              .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
              .baseMipLevel = 0,
              .levelCount = 1,
              .baseArrayLayer = 0,
              .layerCount = 1,
            }
        };
        for (size_t i = 0; (i <  mToColorCommands.size()) && (i < mScreenTarget.colorImages.size()); i++)
        {
            auto cb = mToColorCommands[i];
            if (mScreenTarget.colorImages[i].source == ImageSource::SWAPCHAIN)
                imageBarrier.image = mScreenTarget.colorImages[i].image;
            else if (mScreenTarget.hasResolve() && 
                    (mScreenTarget.resolveImages[i].source == ImageSource::SWAPCHAIN))
                imageBarrier.image = mScreenTarget.resolveImages[i].image;
            else 
            {
                printf("[ERROR] Couldn't find swapchain images\n");
                assert(false);
            }

            vkResetCommandBuffer(cb, 0);

            vkBeginCommandBuffer(cb, &cbBeginInfo);
            vkCmdPipelineBarrier(cb, 
                    VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, 
                    VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
                    0, 0, nullptr, 0, nullptr, 1, &imageBarrier);
            vkEndCommandBuffer(cb);
        }
    }
    {
        VkImageMemoryBarrier imageBarrier = 
        {
            .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
            .srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
            .oldLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
            .newLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
            .subresourceRange = 
            {
              .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
              .baseMipLevel = 0,
              .levelCount = 1,
              .baseArrayLayer = 0,
              .layerCount = 1,
            }
        };

        for (size_t i = 0; (i <  mToPresentCommands.size()) && (i < mScreenTarget.colorImages.size()); i++)
        {
            auto cb = mToPresentCommands[i];
            if (mScreenTarget.colorImages[i].source == ImageSource::SWAPCHAIN)
                imageBarrier.image = mScreenTarget.colorImages[i].image;
            else if (mScreenTarget.hasResolve() && 
                    (mScreenTarget.resolveImages[i].source == ImageSource::SWAPCHAIN))
                imageBarrier.image = mScreenTarget.resolveImages[i].image;
            else 
            {
                printf("[ERROR] Couldn't find swapchain images\n");
                assert(false);
            }
            vkResetCommandBuffer(cb, 0);

            vkBeginCommandBuffer(cb, &cbBeginInfo);
            vkCmdPipelineBarrier(cb, 
                    VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
                    VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, 
                    0, 0, nullptr, 0, nullptr, 1, &imageBarrier);
            vkEndCommandBuffer(cb);
        }
    }
}
auto ox::RenderWindow::destroyScreenTarget() -> void
{
    auto* r = mCreateInfo.render;
    for (auto image : mScreenTarget.colorImages)
        image.destroy(r->getAllocator(), r->getDevice());
    for (auto image : mScreenTarget.depthImages)
        image.destroy(r->getAllocator(), r->getDevice());
    for (auto image : mScreenTarget.resolveImages)
        image.destroy(r->getAllocator(), r->getDevice());
    mScreenTarget = {};
}

auto ox::RenderWindow::acquire() -> RenderFrame
{
    auto* r = mCreateInfo.render;
    const auto frameid = mFrameIndexer.last();
    const auto presentSemaphore = mSemaphoresPresentDone[frameid];
    const auto commandFence = mFencesCommandDone[frameid];

    vkWaitForFences(r->getDevice(), 1, &commandFence, VK_TRUE, UINT64_MAX);
    
    auto result = vkAcquireNextImageKHR(r->getDevice(), mSwapchain, 
            UINT64_MAX, presentSemaphore, nullptr, &mImageid);

    mCachedCommandBuffers.clear();
    mCachedCommandBuffers.push_back(mToColorCommands[mImageid]);
    return 
    { 
        .id = frameid, 
        .image = mImageid, 
        .viewColorFinal = mScreenTarget.getFinalImage(mImageid).view,
        .viewDepthFinal = mScreenTarget.depthImages[mImageid].view,
        .viewColorMultisampled = mScreenTarget.colorImages[mImageid].view,
        .valid = (result == VK_SUBOPTIMAL_KHR) || (result == VK_SUCCESS)
    };
}

auto ox::RenderWindow::addCommandBuffer(VkCommandBuffer iCommandBuffer) -> void
{
    mCachedCommandBuffers.push_back(iCommandBuffer);
}

auto ox::RenderWindow::submit(VkQueue iGfxCompQueue, VkQueue iPresentQueue) -> bool
{
    const auto frameid = mFrameIndexer.last();
    const auto presentSemaphore = mSemaphoresPresentDone[frameid];
    const auto commandSemaphore = mSemaphoresCommandDone[frameid];
    const auto commandFence = mFencesCommandDone[frameid];

    //mCachedCommandBuffers.clear();
    //mCachedCommandBuffers.push_back(mToColorCommands[mImageid]);
    //std::copy(iCommandBuffers.begin(), iCommandBuffers.end(), std::back_inserter(mCachedCommandBuffers));
    mCachedCommandBuffers.push_back(mToPresentCommands[mImageid]);
    
    auto result = vkResetFences(mCreateInfo.render->getDevice(), 1, &commandFence);
    VkPipelineStageFlags waitStages[] = {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};
    VkSubmitInfo submitInfo =
    {
        .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
        .waitSemaphoreCount = 1,
        .pWaitSemaphores = &presentSemaphore,
        .pWaitDstStageMask = waitStages,
        .commandBufferCount = (uint32_t)mCachedCommandBuffers.size(),
        .pCommandBuffers = mCachedCommandBuffers.data(),
        .signalSemaphoreCount = 1,
        .pSignalSemaphores = &commandSemaphore
    };

    result = vkQueueSubmit(iGfxCompQueue, 1, &submitInfo, commandFence);

    VkPresentInfoKHR presentInfo = 
    {
        .sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
        .pNext = nullptr,
        .waitSemaphoreCount = 1,
        .pWaitSemaphores = &commandSemaphore,
        .swapchainCount = 1,
        .pSwapchains = &mSwapchain,
        .pImageIndices = &mImageid
    };

    result = vkQueuePresentKHR(iPresentQueue, &presentInfo);

    auto _ = mFrameIndexer.next();

    mCachedCommandBuffers.clear();

    return (result == VK_SUBOPTIMAL_KHR) || (result == VK_SUCCESS);
}

auto ox::RenderWindow::cleanupSizedData() -> void
{
    destroyScreenTarget();

    if (mSwapchain)
        vkDestroySwapchainKHR(mCreateInfo.render->getDevice(), mSwapchain, nullptr);
    mSwapchain = VK_NULL_HANDLE;   
}

auto ox::RenderWindow::resize() -> bool
{
    CALL_CHECK_DESTROY_RETURN(createSwapchain());
    CALL_CHECK_DESTROY_RETURN(createScreenTarget());
    recordTransitionalCommandBuffers();
    return true;
}
