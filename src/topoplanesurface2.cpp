// Copyright (C) 2023 Vitalii "vitautart" Tartachnyi
// See end of file for extended copyright information.

#include <topoplanesurface.hpp>
#include <graphentities.hpp>
#include <arena.hpp>
#include <forest.hpp>

#include <algorithm>
#include <limits>

namespace {

constexpr double EPS = 4 * oxm::DISTANCE_EPSILON<double>;

enum class PointType :uint8_t
{
    NONE,
    START,
    MERGE,
    SPLIT,
    LREGULAR,
    RREGULAR,
    END
};

struct SweeplineEdge
{
    // If noncollinear is false 'a' stands for middle x coord and 'b' for y coord
    double a, b; // => (y = kx + c) => (x = (y - c) / k) => (x = y / k - c / k) => (a = 1/k, b = - c /k)
    ox::VertexIdx helper;
    PointType helperType;
    bool noncollinear;
    inline auto getXFromY(double y) const noexcept -> double
    {
        return noncollinear ? y * a + b : a;
    }
};

struct SWHalfedge
{
    ox::HalfedgeIdx prev;
    ox::HalfedgeIdx next;
    ox::HalfedgeIdx twin;
    ox::VertexIdx start;
    bool visited;
};

struct SWVertex
{
    oxm::vd2 point;
    ox::HalfedgeIdx input;
    ox::VertexIdx prev;
    ox::VertexIdx next;
    uint32_t id;
};

struct SWDCEL
{
    auto operator[](ox::VertexIdx idx) noexcept -> SWVertex& { return verts[idx.idx]; }
    auto operator[](ox::VertexIdx idx) const noexcept -> const SWVertex& { return verts[idx.idx]; }
    auto operator[](ox::HalfedgeIdx idx) noexcept -> SWHalfedge& { return edges[idx.idx]; }
    auto operator[](ox::HalfedgeIdx idx) const noexcept -> const SWHalfedge& { return edges[idx.idx]; }
    auto clear() noexcept -> void { verts.clear(); edges.clear(); }
    auto reserve(size_t vertsCap, size_t edgesCap) noexcept -> void { verts.reserve(vertsCap); edges.reserve(edgesCap); }
    std::vector<SWVertex> verts;
    std::vector<SWHalfedge> edges;
};

struct CompareFunc
{
    CompareFunc(const SWVertex* data) : verts(data){}
    auto operator()(const ox::VertexIdx l, const ox::VertexIdx r) const noexcept -> bool
    {
            const auto& lp = verts[l.idx].point;
            const auto& rp = verts[r.idx].point;
            const auto d = lp[1] - rp[1];
            return (d > EPS) || ((d < EPS) && (d > -EPS)  && (lp[0] < rp[0]));
    };
    auto operator()(const oxm::vd2& lp, const oxm::vd2& rp) const noexcept -> bool
    {
            const auto d = lp[1] - rp[1];
            return (d > EPS) || ((d < EPS) && (d > -EPS)  && (lp[0] < rp[0]));
    };
private:
    const SWVertex* verts;
};

inline auto createSweeplineEdge(const ox::TopoPoint2D& p1, const ox::TopoPoint2D& p2) noexcept -> SweeplineEdge
{
    const auto x3x4 = p1[0] - p2[0];
    const auto y3y4 = p1[1] - p2[1];
    // if collinear to sweepline embed middle x coordinate to a and y coord to b 
    if (y3y4 < EPS && y3y4 > -EPS )
        return { .a = (p1[0] + p2[0]) * 0.5, .b = p1[1], .noncollinear = false };
    const auto y3y4r = 1 / y3y4;
    const auto y3x4x3y4 = p1[1] * p2[0] - p1[0] * p2[1];
    
    return 
    {
        .a = x3x4 * y3y4r,
        .b = y3x4x3y4 * y3y4r,
        .noncollinear = true,
    };
}

struct SweeplineTriangulation
{
public:
    SWDCEL mDCEL;
    std::vector<ox::VertexIdx> mSortedids;
    std::vector<ox::VertexIdx> mMonoStack;
    std::vector<std::pair<ox::HalfedgeIdx, uint32_t>> mPostsubdividedRoots;
    CompareFunc mCompFunc = CompareFunc(nullptr);

public:
    auto triangulate(const oxm::Plane<double>& plane, const std::vector<std::span<ox::TopoPoint>>& iLoops,
        const std::function<void(const ox::TopoPoint&, const ox::TopoDir&)>& cbAddPoint,
        const std::function<void(uint32_t, uint32_t, uint32_t)>& cbAddTriangle) -> void
    {
        if (!checkPrepare(plane, iLoops, cbAddPoint))
            return;
        subdivide();
        postsubdivide(cbAddTriangle);
        handleMonotonic(cbAddTriangle);
    }
private:
    auto checkPrepare(const oxm::Plane<double>& plane, 
            const std::vector<std::span<ox::TopoPoint>>& iLoops,
            const std::function<void(const ox::TopoPoint&, const ox::TopoDir&)>& cbAddPoint) -> bool
    {
        size_t pointCount = 0;
        for (const auto& loop : iLoops)
            pointCount += loop.size();

        if (pointCount < 3 || iLoops[0].empty())
            return false;

        // TODO: add more accurate reserve for edges
        mDCEL.reserve(pointCount, pointCount * 2);
        mSortedids.reserve(pointCount);

        uint32_t offset = 0;
        uint32_t id = 0;
        for (const auto& loop : iLoops)
        {
            const uint32_t loopSize = loop.size();
            for (uint32_t i = 0; i < loopSize; i++)
            {
                ox::HalfedgeIdx prev = {offset + (i + loopSize - 1) % loopSize};
                ox::HalfedgeIdx next = {offset + (i + 1) % loopSize};
                const auto p = plane.parametric(loop[i]);
                cbAddPoint(loop[i], plane.normal);
                mDCEL.verts.push_back(SWVertex
                {
                    .point = p,
                    .input = prev,
                    .prev = {prev.idx},
                    .next = {next.idx},
                    .id = id,
                });
                mDCEL.edges.push_back(SWHalfedge
                {
                    .prev = prev,
                    .next = next,
                    .twin = ox::IDX_INV_HALFEDGE,
                    .start = { offset + i },
                    .visited = false
                });
                mSortedids.push_back({uint32_t(mDCEL.verts.size() - 1)});
                id++;
            }
            offset += loopSize;
        }

        mCompFunc = CompareFunc(mDCEL.verts.data());
        std::sort(mSortedids.begin(), mSortedids.end(), mCompFunc);

        return true;
    }

    auto wire(ox::VertexIdx v1, bool updateV1, ox::VertexIdx v2, bool updateV2) -> void
    {
        auto inp1 = mDCEL[v1].input;
        auto out1 = mDCEL[inp1].next;

        auto inp2 = mDCEL[v2].input;
        auto out2 = mDCEL[inp2].next;

        auto new1 = ox::HalfedgeIdx{uint32_t(mDCEL.edges.size())};
        auto new2 = ox::HalfedgeIdx{uint32_t(mDCEL.edges.size() + 1)};

        // new1
        mDCEL.edges.push_back(SWHalfedge
        {
            .prev = inp1,
            .next = out2,
            .twin = new2,
            .start = v1,
            .visited = false
        });
        // new2
        mDCEL.edges.push_back(SWHalfedge
        {
            .prev = inp2,
            .next = out1,
            .twin = new1,
            .start = v2,
            .visited = false
        });

        mDCEL[inp1].next = new1;
        mDCEL[out1].prev = new2;

        mDCEL[inp2].next = new2;
        mDCEL[out2].prev = new1;

        if (updateV1) mDCEL[v1].input = new2;
        if (updateV2) mDCEL[v2].input = new1;
    }

    auto wireSplits(ox::VertexIdx higher, ox::VertexIdx lower) -> void
    {
        auto& p1 = mDCEL[higher];
        auto& p2 = mDCEL[lower];
        auto& next1 = mDCEL[p1.next];
        auto cross = oxm::cross(next1.point - p1.point, p2.point - p1.point);

        ox::HalfedgeIdx inp1 = p1.input;
        if (cross > 0)
        {
            auto twin = mDCEL[mDCEL[p1.input].next].twin;
            while (twin != ox::IDX_INV_HALFEDGE)
            {
                inp1 = twin;
                twin = mDCEL[mDCEL[twin].next].twin;
            };
        }
        
        auto out1 = mDCEL[inp1].next;

        auto inp2 = p2.input;
        auto out2 = mDCEL[inp2].next;

        auto new1 = ox::HalfedgeIdx{uint32_t(mDCEL.edges.size())};
        auto new2 = ox::HalfedgeIdx{uint32_t(mDCEL.edges.size() + 1)};

        // new1
        mDCEL.edges.push_back(SWHalfedge
        {
            .prev = inp1,
            .next = out2,
            .twin = new2,
            .start = higher,
            .visited = false
        });
        // new2
        mDCEL.edges.push_back(SWHalfedge
        {
            .prev = inp2,
            .next = out1,
            .twin = new1,
            .start = lower,
            .visited = false
        });

        mDCEL[inp1].next = new1;
        mDCEL[out1].prev = new2;

        mDCEL[inp2].next = new2;
        mDCEL[out2].prev = new1;
    }

    auto subdivide() -> void
    {
        // we are using Y-monotonic setup, so dir of the sweepline is along X
        oxm::Line2D<double> sweepline = {.point = {0, 0}, .dir = {1, 0}};

        // Lines is guaranteed to not intersecs in yPos
        // Because we add only one of edge at the ambigouties points like start or split
        auto comp = [&yPos = sweepline.point[1]](const SweeplineEdge& lhs, const SweeplineEdge& rhs) -> bool
        {
            return lhs.getXFromY(yPos) < rhs.getXFromY(yPos);
        };
        auto equal = [&yPos = sweepline.point[1]](const SweeplineEdge& lhs, const SweeplineEdge& rhs) -> bool
        {
            return std::abs(lhs.getXFromY(yPos) - rhs.getXFromY(yPos)) < EPS;
        };
        auto xgreater = [](const oxm::vd2& point, const SweeplineEdge& rhs) -> bool
        {
            return point[0] > rhs.getXFromY(point[1]);
        };

        using tree_t = ox::rbtree<
            SweeplineEdge, 
            ox::ArenaAllocator<ox::rbnode<SweeplineEdge>>, 
            decltype(comp), 
            decltype(equal)>;
        auto bst = tree_t(comp, equal);

        for (uint32_t i = 0; i < mSortedids.size(); i++)
        {
            const auto idx = mSortedids[i];
            const auto& p0 = mDCEL[idx];
            const auto& prev = mDCEL[p0.prev];
            const auto& next = mDCEL[p0.next];

            auto prevEdge = createSweeplineEdge(prev.point, p0.point);
            auto nextEdge = createSweeplineEdge(p0.point, next.point);
            
            sweepline.point[1] = p0.point[1];
            
            auto type = classifyPoint(p0.point, prev.point, next.point, sweepline.dir);
            
            switch(type)
            {
                case PointType::START: {
                        nextEdge.helper = idx;
                        nextEdge.helperType = type;
                        bst.insert(nextEdge);
                    } break;
                case PointType::END: {
                        auto itPrev = bst.search(prevEdge);
                        if (itPrev->data.helperType == PointType::MERGE)
                            wire(itPrev->data.helper, false, idx, false);
                        bst.remove(itPrev);
                    } break;
                case PointType::MERGE: {
                        auto itPrev = bst.search(prevEdge);
                        if (itPrev->data.helperType == PointType::MERGE)
                            wire(itPrev->data.helper, false, idx, true);
                        auto itHelper = bst.prev(itPrev);
                        if (itHelper->data.helperType == PointType::MERGE)
                            wire(itHelper->data.helper, false, idx, false);
                        itHelper->data.helper = idx;
                        itHelper->data.helperType = type;
                        bst.remove(itPrev);
                    } break;
                case PointType::SPLIT: { 
                        nextEdge.helper = idx;
                        nextEdge.helperType = type;
                        auto itNext = bst.insert(nextEdge);
                        auto itHelper = bst.prev(itNext);
                        if (itHelper->data.helperType != PointType::SPLIT)
                            wire(itHelper->data.helper, false, idx, false);
                        else
                            wireSplits(itHelper->data.helper, idx);
                        itHelper->data.helper = idx;
                        itHelper->data.helperType = type;
                    } break;
                case PointType::LREGULAR: { 
                        auto itPrev = bst.search(prevEdge);
                        if (itPrev->data.helperType == PointType::MERGE)
                            wire(itPrev->data.helper, false, idx, true);
                        nextEdge.helper = idx;
                        nextEdge.helperType = type;
                        bst.remove(itPrev);
                        bst.insert(nextEdge);
                    } break;
                case PointType::RREGULAR: { 
                        auto itHelper = ox::tree::lower(bst, p0.point, xgreater);
                        if (itHelper->data.helperType == PointType::MERGE)
                            wire(itHelper->data.helper, false, idx, false);
                        itHelper->data.helper = idx;
                        itHelper->data.helperType = type;
                    } break;
                default: break;
            }
        }
    }

    auto push3Verts(const std::function<void(uint32_t, uint32_t, uint32_t)>& cbAddTriangle, ox::HalfedgeIdx edge)
    {
        auto v1 = mDCEL[edge].start;
        auto v2 = mDCEL[mDCEL[edge].next].start;
        auto v3 = mDCEL[mDCEL[edge].prev].start;
        cbAddTriangle(mDCEL[v1].id, mDCEL[v2].id, mDCEL[v3].id);
    }

    auto postsubdivide(const std::function<void(uint32_t, uint32_t, uint32_t)>& cbAddTriangle) -> void
    {
        for (uint32_t i = 0; i < mDCEL.edges.size(); i++)
        {
            ox::HalfedgeIdx idx = {i};
            if(mDCEL[idx].visited)
                continue;

            ox::HalfedgeIdx idxmax;
            auto current = idx;
            uint32_t counter = 0;
            oxm::vd2 maxPoint = {0, std::numeric_limits<double>::lowest()};
            do
            {
                counter++;
                auto& edge = mDCEL[current]; 
                auto& p = mDCEL[edge.start]; 
                edge.visited = true;

                if (mCompFunc(p.point, maxPoint))
                {
                    maxPoint = p.point;
                    idxmax = current;
                }
                current = edge.next;
            } while(current != idx);

            if(counter > 3)
                mPostsubdividedRoots.push_back({idxmax, counter});
            else if (counter == 3)
                push3Verts(cbAddTriangle, idx);
        }
    }

    auto isEdgeConvex(ox::VertexIdx v1, ox::VertexIdx v2, ox::VertexIdx v3) noexcept -> bool
    {
        const auto e1 = mDCEL[v2].point - mDCEL[v1].point;
        const auto e2 = mDCEL[v3].point - mDCEL[v2].point;
        auto cross = oxm::cross(e1, e2);

        return cross > EPS;
    }

    auto handleMonotonic(const std::function<void(uint32_t, uint32_t, uint32_t)>& cbAddTriangle) -> void
    {
        for (const auto [edgeIdx, size] : mPostsubdividedRoots)
        {
            mMonoStack.clear();
            auto idx = mDCEL[edgeIdx].start;
            auto leftEdgeIdx = mDCEL[edgeIdx].next;
            auto leftIdx = mDCEL[leftEdgeIdx].start;
            auto rightEdgeIdx = mDCEL[edgeIdx].prev;
            auto rightIdx = mDCEL[rightEdgeIdx].start;

            bool leftSide = mCompFunc(leftIdx, rightIdx);
            auto nextIdx = leftSide ? leftIdx : rightIdx;
            mMonoStack.push_back(idx);
            mMonoStack.push_back(nextIdx);

            for (uint32_t p = 2; p < size; p++)
            {
                if (leftSide)
                {
                    leftEdgeIdx = mDCEL[leftEdgeIdx].next;
                    leftIdx = mDCEL[leftEdgeIdx].start;
                }
                else
                {
                    rightEdgeIdx = mDCEL[rightEdgeIdx].prev;
                    rightIdx = mDCEL[rightEdgeIdx].start;
                }
                bool lastPoint = leftIdx == rightIdx;

                bool currentLeftSide = !lastPoint ? mCompFunc(leftIdx, rightIdx) : !leftSide;
                nextIdx = currentLeftSide ? leftIdx : rightIdx;

                if(leftSide == currentLeftSide)
                {
                    while (mMonoStack.size() > 1)
                    {
                        auto v1 = mMonoStack.back();
                        auto v2 = currentLeftSide ? nextIdx : *std::next(mMonoStack.rbegin());
                        auto v3 = currentLeftSide ? *std::next(mMonoStack.rbegin()) : nextIdx;
                        if (!isEdgeConvex(v3, v1, v2))
                            break;
                        cbAddTriangle(mDCEL[v1].id, mDCEL[v2].id, mDCEL[v3].id);
                        mMonoStack.pop_back();
                    }
                    mMonoStack.push_back(nextIdx);
                }
                else
                {
                    auto v1 = nextIdx;
                    //for (size_t i = 1; i < mMonoStack.size() - (lastPoint ? 1 : 0); i++)
                    for (size_t i = 1; i < mMonoStack.size(); i++)
                    {
                        auto v2 = currentLeftSide ? mMonoStack[i] : mMonoStack[i - 1];
                        auto v3 = currentLeftSide ? mMonoStack[i - 1] : mMonoStack[i];
                        cbAddTriangle(mDCEL[v1].id, mDCEL[v2].id, mDCEL[v3].id);
                    }
                    auto top = mMonoStack.back();
                    mMonoStack.clear();
                    mMonoStack.push_back(top);
                    mMonoStack.push_back(nextIdx);                   
                }
                leftSide = currentLeftSide;
            }
        }
    }

    auto classifyPoint(const oxm::vd2& p0, const oxm::vd2& prev, const oxm::vd2& next, 
            const ox::TopoDir2D& sweeplineDir) const noexcept -> PointType
    {
        bool u1ascend = mCompFunc(p0, prev);
        bool u2ascend = mCompFunc(next, p0);

        if (u1ascend == u2ascend)
            return u1ascend ? PointType::RREGULAR : PointType::LREGULAR;

        const auto u1 = p0 - prev;
        const auto u2 = next - p0;
        if (oxm::cross(u1, u2) > 0)
            return u1ascend ? PointType::START : PointType::END;
        else
            return u1ascend ? PointType::SPLIT : PointType::MERGE;

        return PointType::NONE; 
    }
};

} // anon namespace

auto ox::TopoPlaneSurface::discretize(
        const std::vector<std::span<TopoPoint>>& iLoops,
        const std::function<void(const TopoPoint&, const TopoDir&)>& cbAddPoint,
        const std::function<void(uint32_t, uint32_t, uint32_t)>& cbAddTriangle) 
    const -> void
{
    if (iLoops.size() == 1 && iLoops[0].size() == 3)
    {
        cbAddPoint(iLoops[0][0], plane.normal);
        cbAddPoint(iLoops[0][1], plane.normal);
        cbAddPoint(iLoops[0][2], plane.normal);
        cbAddTriangle(0, 1, 2);
    }
    else if (iLoops.size() == 1 && iLoops[0].size() == 4)
    {
        const auto& v0 = iLoops[0][0];
        const auto& v1 = iLoops[0][1];
        const auto& v2 = iLoops[0][2];
        const auto& v3 = iLoops[0][3];
        
        cbAddPoint(v0, plane.normal);
        cbAddPoint(v1, plane.normal);
        cbAddPoint(v2, plane.normal);
        cbAddPoint(v3, plane.normal);

        const auto d0 = v1 - v0;
        const auto d1 = v2 - v1;
        const auto d2 = v3 - v2;
        const auto d3 = v0 - v3;
        const auto& n = plane.normal;
        if (oxm::dot(oxm::cross(d0, d1), n) < 0 || oxm::dot(oxm::cross(d2, d3), n) < 0)
        {
            cbAddTriangle(0, 1, 3);
            cbAddTriangle(1, 2, 3);
        }
        else
        {
            cbAddTriangle(1, 2, 0);
            cbAddTriangle(2, 3, 0);
        }
    }
    else
    {
        SweeplineTriangulation{}.triangulate(plane, iLoops, cbAddPoint, cbAddTriangle);
    }
}

// MIT License
// 
// Copyright (c) 2023 Vitalii Tartachnyi
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

