#include <render/rendercommon.hpp>
#include <render/renderview3d.hpp>
#include <render/pipelines.hpp>
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include <vulkan/vulkan_core.h>

auto ox::BufferMesh3D::load(VkCommandBuffer iCommandBuffer,
                std::span<RenderVertex3D> iVertices, std::span<uint32_t> iIndices) -> bool
{
    mVerticesCounts[mCurrentBucket] = uint32_t(iVertices.size());
    mIndicesCounts[mCurrentBucket] = uint32_t(iIndices.size());
    mCurrentBucket = mNextBucket;
    mNextBucket = (mNextBucket + 1) % MAX_CONCURRENT_FRAMES;

    if (mVerticesCounts[mCurrentBucket] < 1 || mIndicesCounts[mCurrentBucket] < 1)
        return false;

    {
        const auto bytes = iVertices.size() * sizeof(RenderVertex3D);
        const auto bit = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;
        auto alloc = mRender->getAllocator();
        auto& buff = mVertexBuffers[mCurrentBucket];

        auto res = buff.recreate(alloc, bit, bytes);
        CHECK_VK(res, "[ERROR] Vertex buffer creation failed: %i\n");
        
        buff.write(iVertices.data(), bytes, 0);

        VkBufferCopy region = { .srcOffset = 0, .dstOffset = 0, .size = buff.size() };
        vkCmdCopyBuffer(iCommandBuffer, buff.cpubuffer(), buff.gpubuffer(), 1, &region);
    }
    {
        const auto bytes = iIndices.size() * sizeof(uint32_t);
        const auto bit = VK_BUFFER_USAGE_INDEX_BUFFER_BIT;
        auto alloc = mRender->getAllocator();
        auto& buff = mIndexBuffers[mCurrentBucket];

        auto res = buff.recreate(alloc, bit, bytes);
        CHECK_VK(res, "[ERROR] Index buffer creation failed: %i\n");
        
        buff.write(iIndices.data(), bytes, 0);

        VkBufferCopy region = { .srcOffset = 0, .dstOffset = 0, .size = buff.size() };
        vkCmdCopyBuffer(iCommandBuffer, buff.cpubuffer(), buff.gpubuffer(), 1, &region);
    }

    return true;
}

auto ox::BufferMesh3D::destroy() noexcept -> void
{
    for (auto& b : mVertexBuffers) 
        b.destroy(mRender->getAllocator());
    
    for (auto& b : mIndexBuffers) 
        b.destroy(mRender->getAllocator());
}

ox::BufferMesh3D::~BufferMesh3D() noexcept 
{
    destroy();
}

auto ox::BufferMesh3D::indexbuffer() const noexcept -> const VkBuffer&
{
    return mIndexBuffers[mCurrentBucket].gpubuffer();
}

auto ox::BufferMesh3D::vertexbuffer() const noexcept -> const VkBuffer&
{
    return mVertexBuffers[mCurrentBucket].gpubuffer();
}

auto ox::BufferMesh3D::indexsize() const noexcept -> oxm::u32
{
    return mIndicesCounts[mCurrentBucket];
}

auto ox::BufferMesh3D::vertexsize() const noexcept -> oxm::u32
{
    return mVerticesCounts[mCurrentBucket];
}

namespace 
{
    // Shader reflected data
    struct UnifiedCamera
    {
        alignas(16) oxm::mf4 view_proj;
        alignas(16) oxm::mf4 view;
        alignas(16) oxm::mf4 proj;
        alignas(16) oxm::mf4 view_inv;
    };
}

auto ox::RenderView3D::create(const RenderViewCreateInfo& iCreateInfo) -> bool
{
    if (mInitialized)
        return true;
    else
        mInitialized = true;

    mCreateInfo = iCreateInfo;

    CALL_CHECK_DESTROY_RETURN(createLayouts());
    CALL_CHECK_DESTROY_RETURN(createPipelineForMesh());
    CALL_CHECK_DESTROY_RETURN(createPipelineForWireframe());
    CALL_CHECK_DESTROY_RETURN(createUniformBuffers()); // should be before descriptor sets
    CALL_CHECK_DESTROY_RETURN(createDescriptorSets());

    return true;
}

auto ox::RenderView3D::destroy() -> void
{  
    if (!mInitialized)
        return;

    if (mDescriptorPool)
        vkDestroyDescriptorPool(mCreateInfo.render->getDevice(), mDescriptorPool, nullptr);
    mDescriptorPool = VK_NULL_HANDLE;

    mModelTformNavCube.destroy(mCreateInfo.render->getAllocator());

    for (auto buffer : mViewProjNavCube)
        buffer.destroy(mCreateInfo.render->getAllocator());
    mViewProjNavCube.clear();

    for (auto buffer : mViewProjData)
        buffer.destroy(mCreateInfo.render->getAllocator());
    mViewProjData.clear();

    for (auto buffer : mModelTformData)
        buffer.destroy(mCreateInfo.render->getAllocator());
    mModelTformData.clear();

    if (mMeshPipeline)
        vkDestroyPipeline(mCreateInfo.render->getDevice(), mMeshPipeline, nullptr);
    mMeshPipeline = VK_NULL_HANDLE;

    if (mWirePipeline)
        vkDestroyPipeline(mCreateInfo.render->getDevice(), mWirePipeline, nullptr);
    mWirePipeline = VK_NULL_HANDLE;

    if (mDescriptorSetLayout)
        vkDestroyDescriptorSetLayout(mCreateInfo.render->getDevice(), mDescriptorSetLayout, nullptr);
    mDescriptorSetLayout = VK_NULL_HANDLE;

    if (mPipelineLayout)
        vkDestroyPipelineLayout(mCreateInfo.render->getDevice(), mPipelineLayout, nullptr);
    mPipelineLayout = VK_NULL_HANDLE;

    mInitialized = false;
}



auto ox::RenderView3D::createLayouts() -> bool
{
    std::array<VkDescriptorSetLayoutBinding, 2> bindings = 
    {
        VkDescriptorSetLayoutBinding{
            .binding = 0,
            .descriptorType = VkDescriptorType::VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
            .descriptorCount = 1,
            .stageFlags = VkShaderStageFlagBits::VK_SHADER_STAGE_VERTEX_BIT | 
                VkShaderStageFlagBits::VK_SHADER_STAGE_FRAGMENT_BIT,
            .pImmutableSamplers = nullptr
        },
        VkDescriptorSetLayoutBinding{
            .binding = 1,
            .descriptorType = VkDescriptorType::VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
            .descriptorCount = 1,
            .stageFlags = VkShaderStageFlagBits::VK_SHADER_STAGE_VERTEX_BIT,
            .pImmutableSamplers = nullptr
        },
    };

    VkDescriptorSetLayoutCreateInfo descriptorInfo = 
    {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO, .pNext = nullptr, .flags = 0,
        .bindingCount = bindings.size(),
        .pBindings = bindings.data()
    };
    auto result = vkCreateDescriptorSetLayout(mCreateInfo.render->getDevice(), &descriptorInfo, nullptr, &mDescriptorSetLayout);
    CHECK_AND_RETURN(result, "Descriptor layout creation failed: %i\n");

    VkPipelineLayoutCreateInfo layoutInfo = 
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .setLayoutCount = 1,
        .pSetLayouts = &mDescriptorSetLayout,
        .pushConstantRangeCount = 0,
        .pPushConstantRanges = nullptr
    };

    result = vkCreatePipelineLayout(mCreateInfo.render->getDevice(), &layoutInfo, nullptr, &mPipelineLayout);
    CHECK_AND_RETURN(result, "Pipeline layout creation failed: %i\n");

    return true;
}

auto ox::RenderView3D::createUniformBuffers() -> bool
{
    VkBufferCreateInfo createInfo = 
    {
        .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        .pNext = nullptr, .flags = 0,
        .size = sizeof(UnifiedCamera),
        .usage = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
        .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
        .queueFamilyIndexCount = 0,
        .pQueueFamilyIndices = 0
    };

    for (size_t i = 0; i < mCreateInfo.frameCount; i++)
    {
        GPUPreferMappedBuffer buffer = {};
        auto result = buffer.create(mCreateInfo.render->getAllocator(), createInfo);
        CHECK_AND_RETURN(result, "[ERROR] Uniform buffer creation failed: %i\n");
        mViewProjNavCube.push_back(buffer);
    }

    for (size_t i = 0; i < mCreateInfo.frameCount; i++)
    {
        GPUPreferMappedBuffer buffer = {};
        auto result = buffer.create(mCreateInfo.render->getAllocator(), createInfo);
        CHECK_AND_RETURN(result, "[ERROR] Uniform buffer creation failed: %i\n");
        mViewProjData.push_back(buffer);
    }

    createInfo.size = sizeof(oxm::mf4);

    for (size_t i = 0; i < mCreateInfo.frameCount; i++)
    {
        GPUPreferMappedBuffer buffer = {};
        auto result = buffer.create(mCreateInfo.render->getAllocator(), createInfo);
        CHECK_AND_RETURN(result, "[ERROR] Uniform buffer creation failed: %i\n");
        mModelTformData.push_back(buffer);
    }

    {
        GPUPreferMappedBuffer buffer = {};
        auto result = buffer.create(mCreateInfo.render->getAllocator(), createInfo);
        CHECK_AND_RETURN(result, "[ERROR] Uniform buffer creation failed: %i\n");
        mModelTformNavCube = buffer;
        auto idmatrix = oxm::mf4::id();
        mModelTformNavCube.write(&idmatrix, createInfo.size, 0);
    }

    return true;
}

auto ox::RenderView3D::createPipelineForMesh() -> bool
{
    auto result =  ox::pipelines::mesh3d_v1(mCreateInfo.render->getDevice(), mPipelineLayout, 
            mCreateInfo.render->get(ox::ShaderResource::DIFFUSE3D_VERT), 
            mCreateInfo.render->get(ox::ShaderResource::DIFFUSE3D_FRAG), 
            mCreateInfo.render->getTargetColorFormat(), 
            mCreateInfo.render->getTargetDepthFormat(),
            mCreateInfo.render->getSamplesCount(),
            &mMeshPipeline);
    CHECK_AND_RETURN(result, "[ERROR] Pipeline creation failed: %i\n");

    return true;
}

auto ox::RenderView3D::createPipelineForWireframe() -> bool
{
    auto result =  ox::pipelines::wireframe3d_v1(mCreateInfo.render->getDevice(), mPipelineLayout, 
            mCreateInfo.render->get(ox::ShaderResource::WIREFRAME3D_VERT), 
            mCreateInfo.render->get(ox::ShaderResource::SIMPLE3D_FRAG), 
            mCreateInfo.render->getTargetColorFormat(), 
            mCreateInfo.render->getTargetDepthFormat(),
            mCreateInfo.render->getSamplesCount(),
            &mWirePipeline);
    CHECK_AND_RETURN(result, "[ERROR] Pipeline creation failed: %i\n");

    return true;
}
auto ox::RenderView3D::createDescriptorSets() -> bool
{
    VkDescriptorPoolSize poolSize = 
    {
        .type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
        // 4 beacuse we use 2 descriptors for 3d view and 2 descriptors for navcube
        .descriptorCount = 4 * mCreateInfo.frameCount
    };
    VkDescriptorPoolCreateInfo poolInfo = 
    {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        // 2 beacuse we have sets for 3dview and nav cube
        .maxSets = mCreateInfo.frameCount * 2,
        .poolSizeCount = 1,
        .pPoolSizes = &poolSize
    };

    auto result = vkCreateDescriptorPool(mCreateInfo.render->getDevice(), &poolInfo, nullptr, &mDescriptorPool);
    CHECK_AND_RETURN(result, "[ERROR] Descriptor pool cration failed: %i\n");

    auto createDescSets = [pool = mDescriptorPool, descLayout = mDescriptorSetLayout, ci = mCreateInfo]
        (std::vector<VkDescriptorSet>& sets,
        const std::vector<GPUPreferMappedBuffer>& viewProjs, 
        const std::vector<GPUPreferMappedBuffer>& modelTforms) -> bool
    {

        std::vector<VkDescriptorSetLayout> layouts(ci.frameCount, descLayout);
        VkDescriptorSetAllocateInfo info = 
        {
            .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
            .pNext = nullptr,
            .descriptorPool = pool,
            .descriptorSetCount = (uint32_t)layouts.size(),
            .pSetLayouts = layouts.data()
        };
        sets.resize(info.descriptorSetCount);

        if (sets.size() != viewProjs.size() && sets.size() != modelTforms.size())
        {
            printf("[ERROR] Sizes of vectors for creating descriptors does not match\n");
            return false;
        }

        auto result = vkAllocateDescriptorSets(ci.render->getDevice(), &info, sets.data());
        CHECK_AND_RETURN(result, "[ERROR] Descriptor set allocation failed: %i\n");

        std::vector<VkDescriptorBufferInfo> bufferInfos(sets.size() * 2);
        std::vector<VkWriteDescriptorSet> writes;
        for (size_t i = 0; i < sets.size(); i++)
        {
            size_t viewProjId = i * sets.size() + 0;
            size_t modelTrfId = i * sets.size() + 1;
            bufferInfos[viewProjId] = 
            {
                .buffer = viewProjs[i].buffer(),
                .offset = 0,
                .range = viewProjs[i].size()
            };
            VkWriteDescriptorSet write = 
            {
                .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
                .pNext = nullptr,
                .dstSet = sets[i],
                .dstBinding = 0,
                .dstArrayElement = 0,
                .descriptorCount = 1,
                .descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                .pImageInfo = nullptr,
                .pBufferInfo = &bufferInfos[viewProjId],
                .pTexelBufferView = nullptr
            };
            writes.push_back(write);

            bufferInfos[modelTrfId] = 
            {
                .buffer = modelTforms[i].buffer(),
                .offset = 0,
                .range = modelTforms[i].size()
            };
            write.dstBinding = 1;
            write.pBufferInfo = &bufferInfos[modelTrfId],
            writes.push_back(write);
        }

        vkUpdateDescriptorSets(ci.render->getDevice(), writes.size(), writes.data(), 0, nullptr);
        return true;
    };

    bool res = createDescSets(mDescriptorSets, mViewProjData, mModelTformData);
    if (!res) return false;

    std::vector<GPUPreferMappedBuffer> modelTformsNavCube = {mModelTformNavCube, mModelTformNavCube}; 
    res = createDescSets(mDescriptorSetsNavCube, mViewProjNavCube, modelTformsNavCube);
    if (!res) return false;

    return true;
};

auto ox::RenderView3D::updateViewProj(const oxm::mf4& iView, const oxm::mf4& iProj, uint32_t iFrameid) -> void
{
    auto pos = iView[3].shrink<3>();
    auto view_rot_inv = oxm::transpose(iView.shrink<3, 3>());
    auto pos_inv = - (view_rot_inv * pos);
    const UnifiedCamera camera = 
    {
        .view_proj = iProj * iView,
        .view = iView,
        .proj = iProj,
        .view_inv =
        {
            oxm::grow(view_rot_inv[0], 0.0f),
            oxm::grow(view_rot_inv[1], 0.0f),
            oxm::grow(view_rot_inv[2], 0.0f),
            oxm::grow(pos_inv, 1.0f),
        }
    };
    mViewProjData[iFrameid].write(&camera, sizeof(UnifiedCamera), 0);
}

auto ox::RenderView3D::updateViewProjNavCube(const oxm::mf4& iView, const oxm::mf4& iProj, uint32_t iFrameid) -> void
{
    auto pos = iView[3].shrink<3>();
    auto view_rot_inv = oxm::transpose(iView.shrink<3, 3>());
    auto pos_inv = - (view_rot_inv * pos);
    const UnifiedCamera camera = 
    {
        .view_proj = iProj * iView,
        .view = iView,
        .proj = iProj,
        .view_inv =
        {
            oxm::grow(view_rot_inv[0], 0.0f),
            oxm::grow(view_rot_inv[1], 0.0f),
            oxm::grow(view_rot_inv[2], 0.0f),
            oxm::grow(pos_inv, 1.0f),
        }
    };
    mViewProjNavCube[iFrameid].write(&camera, sizeof(UnifiedCamera), 0);
}


auto ox::RenderView3D::updateModelTform(const oxm::mf4& iModelTform, uint32_t iFrameid) -> void
{
    mModelTformData[iFrameid].write(&iModelTform, sizeof(oxm::mf4), 0);
}

auto ox::RenderView3D::drawBegin(
        VkCommandBuffer iCommandBuffer, 
        const VkRect2D& iRect,
        VkImageView iColorView,
        VkImageView iDepthView,
        ox::maybe<VkImageView> iColorViewMS,
        size_t iFrameId
        ) -> void
{
    VkViewport viewport = 
    {
        .x = (float)iRect.offset.x,
        .y = (float)iRect.offset.y,
        .width = (float)iRect.extent.width,
        .height = (float)iRect.extent.height,
        .minDepth = 0.0f,
        .maxDepth = 1.0f
    };
    VkRenderingAttachmentInfo colorAttachment = 
    {
        .sType = VK_STRUCTURE_TYPE_RENDERING_ATTACHMENT_INFO,
        .pNext = nullptr,
        .imageView = iColorView,
        .imageLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
        .resolveMode = VkResolveModeFlagBits::VK_RESOLVE_MODE_NONE,
        .resolveImageView = VK_NULL_HANDLE, 
        .resolveImageLayout = VK_IMAGE_LAYOUT_UNDEFINED,
        .loadOp = VkAttachmentLoadOp::VK_ATTACHMENT_LOAD_OP_CLEAR,
        .storeOp = VkAttachmentStoreOp::VK_ATTACHMENT_STORE_OP_STORE,
        .clearValue = { .color = {0.1f, 0.2f, 0.2f, 1.0f} }
    };
    if (iColorViewMS.valid)
    {
        colorAttachment.resolveMode = VkResolveModeFlagBits::VK_RESOLVE_MODE_AVERAGE_BIT;
        colorAttachment.resolveImageView = iColorView;
        colorAttachment.resolveImageLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
        colorAttachment.imageView = iColorViewMS.data;
    }
    VkRenderingAttachmentInfo depthAttachment = 
    {
        .sType = VK_STRUCTURE_TYPE_RENDERING_ATTACHMENT_INFO,
        .pNext = nullptr,
        .imageView = iDepthView,
        .imageLayout = VK_IMAGE_LAYOUT_DEPTH_ATTACHMENT_OPTIMAL,
        .resolveMode = VkResolveModeFlagBits::VK_RESOLVE_MODE_NONE,
        .resolveImageView = VK_NULL_HANDLE, 
        .resolveImageLayout = VK_IMAGE_LAYOUT_UNDEFINED,
        .loadOp = VkAttachmentLoadOp::VK_ATTACHMENT_LOAD_OP_CLEAR,
        .storeOp = VkAttachmentStoreOp::VK_ATTACHMENT_STORE_OP_DONT_CARE,
        .clearValue = { .depthStencil = {.depth = 1.0f, .stencil = 0} }
    };

    VkRenderingInfo renderingInfo = 
    {
        .sType = VK_STRUCTURE_TYPE_RENDERING_INFO,
        .pNext = nullptr,
        .flags = 0, // TODO: maybe here can be something usefull
        .renderArea = iRect,
        .layerCount = 1, 
        .viewMask = 0,
        .colorAttachmentCount = 1,
        .pColorAttachments = &colorAttachment,
        .pDepthAttachment = &depthAttachment, 
        .pStencilAttachment = nullptr
    };

    vkCmdBeginRendering(iCommandBuffer, &renderingInfo);

    vkCmdBindDescriptorSets(iCommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, mPipelineLayout, 
            0, 1, &mDescriptorSets[iFrameId], 0, nullptr);
    vkCmdSetViewport(iCommandBuffer, 0, 1, &viewport);
    vkCmdSetScissor(iCommandBuffer, 0, 1, &iRect);
}

auto ox::RenderView3D::draw(VkCommandBuffer iCommandBuffer, const BufferMesh3DSolid& iMesh) -> void
{
    VkDeviceSize offsets[1] = {0};
    vkCmdBindPipeline(iCommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, mMeshPipeline);
    auto vertices = iMesh.vertexbuffer();
    auto indices = iMesh.indexbuffer();
    vkCmdBindVertexBuffers(iCommandBuffer, 0, 1, &vertices, offsets);
    vkCmdBindIndexBuffer(iCommandBuffer, indices, 0, VkIndexType::VK_INDEX_TYPE_UINT32);
    vkCmdDrawIndexed(iCommandBuffer, iMesh.indexsize(), 1, 0, 0, 0);
}

auto ox::RenderView3D::draw(VkCommandBuffer iCommandBuffer, const BufferMesh3DWire& iMesh) -> void
{
    VkDeviceSize offsets[1] = {0};
    vkCmdBindPipeline(iCommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, mWirePipeline);
    auto vertices = iMesh.vertexbuffer();
    auto indices = iMesh.indexbuffer();
    vkCmdBindVertexBuffers(iCommandBuffer, 0, 1, &vertices, offsets);
    vkCmdBindIndexBuffer(iCommandBuffer, indices, 0, VkIndexType::VK_INDEX_TYPE_UINT32);
    vkCmdDrawIndexed(iCommandBuffer, iMesh.indexsize(), 1, 0, 0, 0);
}

auto ox::RenderView3D::drawEnd(VkCommandBuffer iCommandBuffer) -> void
{
    vkCmdEndRendering(iCommandBuffer);
}

auto ox::RenderView3D::drawNavCube(VkCommandBuffer iCommandBuffer, const VkRect2D& iRect,
            VkImageView iColorView,
            VkImageView iDepthView,
            ox::maybe<VkImageView> iColorViewMS,
            size_t iFrameId
            ) -> void
{
    VkViewport viewport = 
    {
        .x = (float)iRect.offset.x,
        .y = (float)iRect.offset.y,
        .width = (float)iRect.extent.width,
        .height = (float)iRect.extent.height,
        .minDepth = 0.0f,
        .maxDepth = 1.0f
    };
    VkRenderingAttachmentInfo colorAttachment = 
    {
        .sType = VK_STRUCTURE_TYPE_RENDERING_ATTACHMENT_INFO,
        .pNext = nullptr,
        .imageView = iColorView,
        .imageLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
        .resolveMode = VkResolveModeFlagBits::VK_RESOLVE_MODE_NONE,
        .resolveImageView = VK_NULL_HANDLE, 
        .resolveImageLayout = VK_IMAGE_LAYOUT_UNDEFINED,
        .loadOp = VkAttachmentLoadOp::VK_ATTACHMENT_LOAD_OP_LOAD,
        .storeOp = VkAttachmentStoreOp::VK_ATTACHMENT_STORE_OP_STORE,
        .clearValue = { .color = {0.1f, 0.2f, 0.2f, 1.0f} }
    };
    if (iColorViewMS.valid)
    {
        colorAttachment.resolveMode = VkResolveModeFlagBits::VK_RESOLVE_MODE_AVERAGE_BIT;
        colorAttachment.resolveImageView = iColorView;
        colorAttachment.resolveImageLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
        colorAttachment.imageView = iColorViewMS.data;
    }
    VkRenderingAttachmentInfo depthAttachment = 
    {
        .sType = VK_STRUCTURE_TYPE_RENDERING_ATTACHMENT_INFO,
        .pNext = nullptr,
        .imageView = iDepthView,
        .imageLayout = VK_IMAGE_LAYOUT_DEPTH_ATTACHMENT_OPTIMAL,
        .resolveMode = VkResolveModeFlagBits::VK_RESOLVE_MODE_NONE,
        .resolveImageView = VK_NULL_HANDLE, 
        .resolveImageLayout = VK_IMAGE_LAYOUT_UNDEFINED,
        .loadOp = VkAttachmentLoadOp::VK_ATTACHMENT_LOAD_OP_CLEAR,
        .storeOp = VkAttachmentStoreOp::VK_ATTACHMENT_STORE_OP_DONT_CARE,
        .clearValue = { .depthStencil = {.depth = 1.0f, .stencil = 0} }
    };

    VkRenderingInfo renderingInfo = 
    {
        .sType = VK_STRUCTURE_TYPE_RENDERING_INFO,
        .pNext = nullptr,
        .flags = 0, // TODO: maybe here can be something usefull
        .renderArea = iRect,
        .layerCount = 1, 
        .viewMask = 0,
        .colorAttachmentCount = 1,
        .pColorAttachments = &colorAttachment,
        .pDepthAttachment = &depthAttachment, 
        .pStencilAttachment = nullptr
    };

    vkCmdBeginRendering(iCommandBuffer, &renderingInfo);

    vkCmdBindDescriptorSets(iCommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, mPipelineLayout, 
            0, 1, &mDescriptorSetsNavCube[iFrameId], 0, nullptr);
    vkCmdSetViewport(iCommandBuffer, 0, 1, &viewport);
    vkCmdSetScissor(iCommandBuffer, 0, 1, &iRect);

    VkDeviceSize offsets[1] = {0};
    vkCmdBindPipeline(iCommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, mMeshPipeline);
    auto mesh = mCreateInfo.render->getCubeMesh();
    vkCmdBindVertexBuffers(iCommandBuffer, 0, 1, &mesh->buffer.buffer(), offsets);
    vkCmdDraw(iCommandBuffer, mesh->vertcount, 1, 0, 0);

    vkCmdEndRendering(iCommandBuffer);
}

