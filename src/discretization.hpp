#pragma once

#include "oxmath.hpp"
#include <brep.hpp>
#include <render/rendervertex.hpp>

namespace ox
{
    class Discretization
    {
    public:
        Discretization(Discretization&& other) = default;
        auto operator= (Discretization&& other) -> Discretization& = default;
        static auto create(const BRep3D& iBrep) -> Discretization;

        auto getWireframePoints() const -> const std::vector<RenderVertex3D>&
        {
            return mWireframePoints;
        }
        auto getWireframeIndexes() const -> const std::vector<uint32_t>&
        {
            return mWireframeIndexes;
        }
        auto getSolidPoints() const -> const std::vector<RenderVertex3D>&
        {
            return mSolidPoints;
        }
        auto getSolidIndexes() const -> const std::vector<uint32_t>&
        {
            return mSolidIndexes;
        }
        auto moveWireframePointsTo(std::vector<RenderVertex3D>& points) -> void
        {
            points = std::move(mWireframePoints);
        }
        auto moveWireframeIndexesTo(std::vector<uint32_t>& indexes) -> void
        {
            indexes = std::move(mWireframeIndexes);
        }
        auto moveSolidPointsTo(std::vector<RenderVertex3D>& points) -> void
        {
            points = std::move(mSolidPoints);
        }
        auto moveSolidIndexesTo(std::vector<uint32_t>& indexes) -> void
        {
            indexes = std::move(mSolidIndexes);
        }
        auto move(std::vector<uint32_t>& indexes) && -> void
        {
            indexes = std::move(mSolidIndexes);
        }
    private:
        Discretization() = default;
        Discretization(const Discretization&) = delete;
        auto operator= (const Discretization& other) -> Discretization& = delete;
        auto createWireframe(const BRep3D& iBrep) -> void;
        auto createSolid(const BRep3D& iBrep) -> void;

    private:
        std::vector<RenderVertex3D> mWireframePoints;
        std::vector<uint32_t> mWireframeIndexes;
        std::vector<RenderVertex3D> mSolidPoints;
        std::vector<uint32_t> mSolidIndexes;
    };
}
