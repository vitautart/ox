#version 450
#extension GL_ARB_separate_shader_objects : enable

layout (location = 0) in vec3 in_color;

layout (location = 0) out vec4 out_color;

struct UnifiedCamera
{
    mat4x4 view_proj;
    mat4x4 view;
    mat4x4 proj;
    mat4x4 view_inv;
};

// unused, just in case here
layout (set = 0, binding = 0) uniform Camera { UnifiedCamera data; } camera;

void main()
{
  out_color = vec4(in_color, 1.0);
}
