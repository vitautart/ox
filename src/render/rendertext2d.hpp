#pragma once

#include "definitions.hpp"
#include "render/rendercommon.hpp"
#include <render/render.hpp>
#include <render/res/resbuffers.hpp>
#include <render/rendervertex.hpp>

#include <string>
#include <vector>
#include <vulkan/vulkan_core.h>

namespace ox
{
    class BufferText2D
    {
    public:
        CTORS_DEFAULT_ALL(BufferText2D);
        explicit BufferText2D(const ox::Render* iRender) : mRender(iRender) {};
        auto destroy() noexcept -> void;
        auto add(const std::u32string_view iText, const oxm::vf2& iPos, const oxm::vu4& iColor) -> void;
        auto clear() noexcept -> void;
        auto load(VkCommandBuffer iCommandBuffer) -> bool;
        auto buffer() const noexcept -> const VkBuffer&;
        auto size() const noexcept -> oxm::u32;
        // TODO: probably this function should be refactored somewhere else
        // when we will get different fonts
        auto font() const noexcept -> const BitmapFont*;
    private:
        std::vector<CharVertex> mVertices;
        CPUtoGPUBuffer mVertexBuffers[2] = {};
        bool mShouldLoad[2] = {false, false};
        oxm::u32 mVerticesCounts[2] = {};
        oxm::u8 mCurrentBucket = 0;
        oxm::u8 mNextBucket = 0;
    private: // non-owned objects
        const Render* mRender = nullptr;
    };

    class RenderText2D
    {
    public:
        auto create(const Render* iRender) -> bool;
        auto destroy() -> void;
        auto drawBegin(VkCommandBuffer iCommandBuffer, const VkRect2D& iRect, VkImageView iColorView) -> void;
        auto draw(VkCommandBuffer iCommandBuffer, const BufferText2D& iBuffer) -> void;
        auto drawEnd(VkCommandBuffer iCommandBuffer) -> void;
        
    private:
        auto createLayouts() -> bool;
        auto createPipeline() -> bool;
        auto createDescriptorSets() -> bool;

    private:
        VkPipeline mPipeline = VK_NULL_HANDLE;
        VkPipelineLayout mPipelineLayout = VK_NULL_HANDLE;
        VkDescriptorPool mDescriptorPool = VK_NULL_HANDLE;
        VkDescriptorSetLayout mDescriptorSetLayout = VK_NULL_HANDLE;
        VkDescriptorSet mDescriptorSet = VK_NULL_HANDLE;
        VkSampler mSampler = VK_NULL_HANDLE;
    private: // non-owned objects
        const Render* mRender = nullptr;
    };
}
