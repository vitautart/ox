#include "oxgeometry.hpp"
#include "topobase.hpp"
#include <memory>
#include <test.hpp>
#include <cmath>
#include <numbers>
#include <stdio.h>
#include <thread>
#include <chrono>
#include <GLFW/glfw3.h>
#include <render/render.hpp>
#include <render/renderwindow.hpp>
#include <render/renderview3d.hpp>
#include <render/rendertext2d.hpp>
#include <render/rendershape2d.hpp>
#include <camera.hpp>
#include <vulkan/vulkan_core.h>
#include <brep.hpp>
#include <topolinecurve.hpp>
#include <discretization.hpp>
#include <ui/ui.hpp>

namespace test2
{
#define WINDOW_WIDTH  640
#define WINDOW_HEIGHT  480

uint32_t view_width = WINDOW_WIDTH;
uint32_t view_height = WINDOW_HEIGHT;
int view_offset_x = 0;
int view_offset_y = 0;

bool panning = false;
bool rotate = false;
int resizeCounter = 0;

struct UserDataGLFW
{
    ox::EditorCamera3D* camera;
    ox::ui::VStretchCanvas* canvas;
};

//ox::RenderWindow rw1 = {};

auto printBREP(const ox::BRep3D& brep) -> void
{
    printf("-----------------------------------------\n");
    printf("- FACES: %zu\n", brep.getFaces().size());
    printf("- LOOPS: %zu\n", brep.getLoops().size());
    printf("- EDGES: %zu\n", brep.getEdges().size());
    printf("- HALFEDGES: %zu\n", brep.getHalfedges().size());
    printf("- VERTICES: %zu\n", brep.getVertices().size());

    printf("-----------------------------------------\n");
    auto fids = brep.getFaceIds();
    auto lids = brep.getLoopIds();
    auto eids = brep.getEdgeIds();
    auto heids = brep.getHalfedgeIds();
    auto vids = brep.getVertexIds();
    printf("- VALID FACES: %zu\n", fids.size());
    printf("- VALID LOOPS: %zu\n", lids.size());
    printf("- VALID EDGES: %zu\n", eids.size());
    printf("- VALID HALFEDGES: %zu\n", heids.size());
    printf("- VALID VERTEX: %zu\n", vids.size());
    printf("-----------------------------------------\n");
}

void generateBase1(std::vector<ox::TopoPoint>& vertices, std::vector<std::unique_ptr<ox::ITopoCurve>>& curves)
{
    vertices = std::vector<ox::TopoPoint>
    {
        {-2,  2, 0},
        {-2, -2, 0},
        { 3, -2, 0},
        { 3,  0, 0},
        { 1,  2, 0}
    };
    for (size_t i = 0; i < vertices.size(); i++)
    {
        auto dir = oxm::norm(vertices[(i + 1) % vertices.size()] - vertices[i]);
        curves.push_back(std::make_unique<ox::TopoLineCurve>(oxm::Line3D<double>{ vertices[i], dir }));
    }
}

void generateBase2(std::vector<ox::TopoPoint>& vertices, std::vector<std::unique_ptr<ox::ITopoCurve>>& curves)
{
    vertices = std::vector<ox::TopoPoint>
    {
        {-1,  1, 0},
        {-3,  1, 0},
        {-3, -3, 0},
        { 4, -3, 0},
        { 4,  1, 0},
        { 2,  1, 0},
        { 2, -1, 0},
        {-1, -1, 0},
    };
    for (size_t i = 0; i < vertices.size(); i++)
    {
        auto dir = oxm::norm(vertices[(i + 1) % vertices.size()] - vertices[i]);
        curves.push_back(std::make_unique<ox::TopoLineCurve>(oxm::Line3D<double>{ vertices[i], dir }));
    }
}

void generateBase3(std::vector<ox::TopoPoint>& vertices, std::vector<std::unique_ptr<ox::ITopoCurve>>& curves)
{
    vertices = std::vector<ox::TopoPoint>
    {
        { 0,  0,  0},
        { 1, -3,  0},
        { 3, -2,  0},
        { 3,  0,  0},
        { 2,  2,  0},
        { 1,  0,  0},
        { 0,  2,  0},
        {-1,  0,  0},
        {-2,  2,  0},
        {-3,  0,  0},
        {-3, -2,  0},
        {-1, -3,  0},
            
    };
    for (size_t i = 0; i < vertices.size(); i++)
    {
        auto dir = oxm::norm(vertices[(i + 1) % vertices.size()] - vertices[i]);
        curves.push_back(std::make_unique<ox::TopoLineCurve>(oxm::Line3D<double>{ vertices[i], dir }));
    }
}

void generateBase4(std::vector<ox::TopoPoint>& vertices, std::vector<std::unique_ptr<ox::ITopoCurve>>& curves)
{
    vertices = std::vector<ox::TopoPoint>
    {
        { 0,  0,  0},
        { 1, -1,  0},
        { 2,  0,  0},
        { 3, -1,  0},
        { 3,  1,  0},
        { 2,  2,  0},
        { 1,  1,  0},
        { 0,  2,  0},
        {-1,  1,  0},
        {-2,  2,  0},
        {-3,  1,  0},
        {-3, -1,  0},
        {-2,  0,  0},
        {-1, -1,  0},
            
    };
    for (size_t i = 0; i < vertices.size(); i++)
    {
        auto dir = oxm::norm(vertices[(i + 1) % vertices.size()] - vertices[i]);
        curves.push_back(std::make_unique<ox::TopoLineCurve>(oxm::Line3D<double>{ vertices[i], dir }));
    }
}

void generateBase5(std::vector<ox::TopoPoint>& vertices, std::vector<std::unique_ptr<ox::ITopoCurve>>& curves)
{
    vertices = std::vector<ox::TopoPoint>
    {
        { 0,  0,  0},
        { 0, -2,  0},
        { 3, -2,  0},
        { 3,  2,  0},
        {-2,  2,  0},
        {-2, -2,  0},
        {-1, -2,  0},
        {-1,  1,  0},
        { 2,  1,  0},
        { 2, -1,  0},
        { 1, -1,  0},
        { 1,  0,  0},
    };
    for (size_t i = 0; i < vertices.size(); i++)
    {
        auto dir = oxm::norm(vertices[(i + 1) % vertices.size()] - vertices[i]);
        curves.push_back(std::make_unique<ox::TopoLineCurve>(oxm::Line3D<double>{ vertices[i], dir }));
    }
}
void generateBase6(std::vector<std::vector<ox::TopoPoint>>& vertices, std::vector<std::vector<std::unique_ptr<ox::ITopoCurve>>>& curves)
{
    vertices.emplace_back();
    vertices.back() = std::vector<ox::TopoPoint>
    {
        {-2,  2, 0},
        {-2, -2, 0},
        { 3, -2, 0},
        { 3,  0, 0},
        { 1,  2, 0}
    };
    vertices.emplace_back();
    vertices.back() = std::vector<ox::TopoPoint>
    {
        {-1,  1, 0},
        { 1,  1, 0},
        { 1, -1, 0},
        {-1, -1, 0},
    };
    for(auto v : vertices)
    {
        curves.emplace_back();
        auto& c = curves.back();
        for (size_t i = 0; i < v.size(); i++)
        {
            auto dir = oxm::norm(v[(i + 1) % v.size()] - v[i]);
            c.push_back(std::make_unique<ox::TopoLineCurve>(oxm::Line3D<double>{ v[i], dir }));
        }
    }
}
void generateSolid(std::vector<ox::RenderVertex3D>& points, std::vector<uint32_t>& indexes, std::vector<ox::RenderVertex3D>& solidPoints, std::vector<uint32_t>& solidIndexes)
{
    std::vector<std::vector<ox::TopoPoint>> vertices;
    std::vector<std::vector<std::unique_ptr<ox::ITopoCurve>>> curves;
    generateBase6(vertices, curves);

    std::vector<std::span<ox::TopoPoint>> vertsInput;
    std::vector<std::span<std::unique_ptr<ox::ITopoCurve>>> curvesInput;   
    for (auto& v : vertices)
        vertsInput.push_back(v);
     for (auto& c : curves)
        curvesInput.push_back(c);   
    auto [brep, valid] = ox::BRep3D::createPlanarFace(vertsInput, curvesInput, oxm::Plane<double>{.normal = {0, 0, 1}, .xdir = {1, 0, 0}, .ydir = {0, 1, 0} });
    if (valid)
        printf("[TEST] BREP SUCCSESSFULLY CREATED:\n");
    else
        printf("[ERROR] BREP CREATION FAILED\n");

    printBREP(brep);   

    auto fids = brep.getFaceIds();
    fids = brep.getFaceIds();
    brep.extrudeFace(fids[0], 4);

    /*
    for (auto lidx : brep.getLoopIds())
        brep.print(lidx);
    for (auto vidx : brep.getVertexIds())
        brep.print(vidx);
    */
    printBREP(brep);

    auto disc = ox::Discretization::create(brep);
    
    disc.moveWireframePointsTo(points);
    disc.moveWireframeIndexesTo(indexes);
    disc.moveSolidPointsTo(solidPoints);
    disc.moveSolidIndexesTo(solidIndexes);
    printf("[TEST] WIREFRAME POINTS COUNT: %zu INDEX COUNT: %zu\n", points.size(), indexes.size());
    printf("[TEST] SOLID POINTS COUNT: %zu INDEX COUNT: %zu\n", solidPoints.size(), solidIndexes.size());
    printf("[TEST] GENERATION FINISHED\n\n");
}

void resizeCallback(GLFWwindow* window,int width,int height)
{
    resizeCounter = 50;
}


void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
    {
        glfwSetWindowShouldClose(window, true);
    }
    if (key == GLFW_KEY_R && action == GLFW_RELEASE)
    {
        glfwSetWindowSize(window, 500, 400);
    }
    if (key == GLFW_KEY_P && action == GLFW_RELEASE)
    {
        //auto camera = (ox::EditorCamera3D*) glfwGetWindowUserPointer(window);
        auto ud = (UserDataGLFW*) glfwGetWindowUserPointer(window);
        ud->camera->setPerspective(!ud->camera->mPerspective);
    }
}

void mouseCallback(GLFWwindow* window, int button, int action, int mods)
{
    auto ud = (UserDataGLFW*) glfwGetWindowUserPointer(window);

    if (action == GLFW_PRESS)
    {
        double x, y;
        glfwGetCursorPos(window, &x, &y);
        
        if (ud->canvas->getRect().inside({float(x), float(y)}))
            //ud->canvas->onClick(oxm::vf2{float(x), float(y)} - ud->canvas->getRect().pos);
            ud->canvas->onClick(oxm::vf2{float(x), float(y)});
    }

    if (button == GLFW_MOUSE_BUTTON_LEFT)
    {
        if (action == GLFW_RELEASE)
        {
            ud->camera->resetPan();
            panning = false;
        }
        else
            panning = true;
    }
    else if (button == GLFW_MOUSE_BUTTON_RIGHT)
    {
        if (action == GLFW_RELEASE)
        {
            ud->camera->resetArcball();
            rotate = false;
        }
        else
            rotate = true;
    }
}

void mouseMoveCallback(GLFWwindow* window, double x, double y)
{
    auto ud = (UserDataGLFW*) glfwGetWindowUserPointer(window);

    if (panning)
        ud->camera->pan({(float)x + view_offset_x, (float)y + view_offset_y});

    if (rotate)
        ud->camera->arcball({(float)x + view_offset_x, (float)y + view_offset_y});
    oxm::vf2 pos = {float(x), float(y)};
    
    if (ud->canvas->getRect().inside(pos))
        ud->canvas->onHover(pos);
    else
        ud->canvas->onHoverEnd();
}

void scrollCallback(GLFWwindow* window, double xOffset, double yOffset)
{
    //auto camera = (ox::EditorCamera3D*) glfwGetWindowUserPointer(window);
    auto ud = (UserDataGLFW*) glfwGetWindowUserPointer(window);
    ud->camera->zoom(yOffset * 0.1f);
}

int run()
{
    //generateSolid();
    oxm::vf3 color = {0, 0, 0.8f};
    oxm::vf3 colorWireframe = {0, 0, 0};
    std::vector<ox::RenderVertex3D> vertices;

    std::vector<uint32_t> indices;

    std::vector<ox::RenderVertex3D> verticesWireframe;


    std::vector<uint32_t> indicesWireframe;

    generateSolid(verticesWireframe, indicesWireframe, vertices, indices);

    if (!glfwInit())
        return -1;

    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    glfwWindowHint(GLFW_RESIZABLE, false);
    auto window1 = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, "OX CAD", NULL, NULL);
    //auto window2 = glfwCreateWindow(480, 480, "OX CAD", NULL, NULL);
    glfwSetKeyCallback(window1, keyCallback);
    glfwSetMouseButtonCallback(window1, mouseCallback);
    glfwSetCursorPosCallback(window1, mouseMoveCallback);
    glfwSetScrollCallback(window1, scrollCallback);
    glfwSetWindowSizeCallback(window1, resizeCallback);

    //glfwSetKeyCallback(window2, keyCallback);

    bool result = false;
    ox::Render render { {VkSampleCountFlagBits::VK_SAMPLE_COUNT_4_BIT}, result};

    ox::RenderWindow rw1 = {}/*, rw2 = {}*/;
    ox::RenderWindowCreateInfo info = 
    {
        .render = &render,
        .window = window1,
        .gfxCompQueueFamily = render.getGfxCompQueue().family,
        .presentQueueFamily = render.getPresentQueue().family,
    };
    rw1.create(info, [r = &render](VkFormat format, VkColorSpaceKHR space)
    {
        r->setTargetColorFormat(format);
        r->setTargetColorSpace(space);
    });
    //info.window = window2;
    //rw2.create(info);


    ox::RenderView3D view = {};
    ox::RenderViewCreateInfo viewInfo = 
    {
        .render = &render,
        .frameCount = render.getFrameCount(),
    };
    view.create(viewInfo);
    ox::BufferMesh3DSolid meshSolid {&render};
    ox::BufferMesh3DWire meshWire {&render};
    bool meshShouldUpdate = true;
    
    ox::RenderShape2D ui = {}; ui.create(&render);

    ox::RenderText2D text = {}; text.create(&render);

    ox::ui::Style style = 
    {
        .bgColorDefault = { 5, 10, 10, 255 },
        .bgColorHighlight = { 60, 60, 60, 255 },
        .bgColorDisabled = { 40, 40, 40, 255 },
        .bgColorInputField = { 0, 15, 15, 255 },
        .borderColorDefault = { 200, 200, 200, 255 },
        .borderColorHighlight = { 120, 255, 120, 255 },
        .borderColorDisabled = { 255, 255, 255, 255 },
        .textColorDefault = { 255, 255, 255, 255 },
        .textColorHighlight = { 255, 255, 255, 255 },
        .textColorDisabled = { 255, 255, 255, 255 },
        .textColorSelected = { 255, 255, 255, 255 },
        .textBgColorSelected = { 255, 255, 255, 255 },
    };
    ox::ui::VStretchCanvas canvas = {{15, 15, 200, 201 }, 8, &render, &style };
    auto button1 = std::make_unique<ox::ui::Button>(U"Button 1", &style);
    button1->setClickHandler([]{printf("PRESS BUTTON\n");});
    std::vector<std::unique_ptr<ox::ui::Element>> elems; 
    {
        elems.push_back(std::make_unique<ox::ui::Label>(U"Label 1", &style));
        elems.push_back(std::move(button1));
        elems.push_back(std::make_unique<ox::ui::Button>(U"Button 2", &style));
        elems.push_back(std::make_unique<ox::ui::Button>(U"Button 3", &style));
        elems.push_back(std::make_unique<ox::ui::SmallInput>(&style));
    };
    canvas.add(elems);


    auto camera = ox::EditorCamera3D({0, 0, 4}, {0.0f, 0.0f, 0.0f}, {0, 1, 0}, 
            view_width, view_height, false, std::numbers::pi / 4);

    auto userData = UserDataGLFW 
    {
        .camera = &camera,
        .canvas = &canvas
    };

    glfwSetWindowUserPointer(window1, &userData);

    while (!glfwWindowShouldClose(window1) /*&& !glfwWindowShouldClose(window2)*/)
    {
        if (resizeCounter > 0)
        {
            std::this_thread::sleep_for(std::chrono::milliseconds(1));
            resizeCounter--;
            if (resizeCounter == 0)
            {
                render.wait();

                int width, height;
                glfwGetFramebufferSize(window1, &width, &height);

                view_width = width, view_height = height;
                camera.setSize(view_width, view_height);
                
                rw1.cleanupSizedData();
                // -----
                rw1.resize();
            }
            glfwPollEvents();
        }
        else
        {
            auto frame = rw1.acquire();
            view.updateViewProj(camera.getView(), camera.getProj(), frame.id);
            view.updateModelTform(oxm::mf4::id(), frame.id);

            rw1.addCommandBuffer(frame,
            [&](VkCommandBuffer cb, const ox::RenderFrame& frame)
            {
                bool syncView = meshShouldUpdate;
                if (meshShouldUpdate)
                {
                    meshSolid.load(cb,vertices, indices);
                    meshWire.load(cb,verticesWireframe, indicesWireframe);
                    meshShouldUpdate = false;
                }

                bool syncUI = canvas.load(cb);
                if (syncView || syncUI)
                {
                    VkMemoryBarrier2 barrier =
                    {
                        .sType = VK_STRUCTURE_TYPE_MEMORY_BARRIER_2,
                        .pNext = nullptr,
                        .srcStageMask = VK_PIPELINE_STAGE_2_COPY_BIT,
                        .srcAccessMask = VK_ACCESS_2_TRANSFER_WRITE_BIT,
                        .dstStageMask = VK_PIPELINE_STAGE_2_INDEX_INPUT_BIT | VK_PIPELINE_STAGE_2_VERTEX_ATTRIBUTE_INPUT_BIT,
                        .dstAccessMask = VK_ACCESS_2_INDEX_READ_BIT | VK_ACCESS_2_VERTEX_ATTRIBUTE_READ_BIT
                    };

                    VkDependencyInfo deps =
                    {
                        .sType = VK_STRUCTURE_TYPE_DEPENDENCY_INFO,
                        .pNext = nullptr,
                        .dependencyFlags = 0,
                        .memoryBarrierCount = 1,
                        .pMemoryBarriers = &barrier,
                        .bufferMemoryBarrierCount = 0,
                        .pBufferMemoryBarriers = nullptr,
                        .imageMemoryBarrierCount = 0,
                        .pImageMemoryBarriers = nullptr
                    };
                    vkCmdPipelineBarrier2(cb, &deps);
                }

                auto windowRect = VkRect2D
                { 
                    .offset ={view_offset_x, view_offset_y}, 
                    .extent = {view_width, view_height}
                };

                view.drawBegin(cb, windowRect, frame.viewColorFinal, frame.viewDepthFinal, frame.viewColorMultisampled, frame.id);
                view.draw(cb, meshSolid);
                view.draw(cb, meshWire);
                view.drawEnd(cb);
                
                auto rect = canvas.getRect();
                VkRect2D vkrect = 
                {
                    {(int32_t)rect.pos[0], (int32_t)rect.pos[1]},
                    {(uint32_t)rect.size[0], (uint32_t)rect.size[1]}
                };
                const auto& canvasSolidBuf = canvas.getBufferSolid();
                const auto& canvasWireBuf = canvas.getBufferWire();
                const auto& canvasTextBuf = canvas.getBufferText();
                ui.drawBegin(cb, vkrect, frame.viewColorFinal);
                ui.draw(cb, canvasSolidBuf);
                ui.draw(cb, canvasWireBuf);
                ui.drawEnd(cb);

                text.drawBegin(cb, vkrect, frame.viewColorFinal);
                text.draw(cb, canvasTextBuf);
                text.drawEnd(cb);
            });

            auto res = rw1.submit(render.getGfxCompQueue().queue, render.getPresentQueue().queue);
            glfwWaitEvents();
        }
    }

    render.wait();

    text.destroy();
    ui.destroy();
    view.destroy();
    rw1.destroy();
    //rw2.destroy();
    glfwTerminate();
    return 0;
}
}
